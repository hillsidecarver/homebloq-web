import React, { Component } from "react"
import { PropTypes } from "prop-types"
import Login from "../../auth/Login"
import Logout from "../../auth/Logout"
import { loginUser } from "../../../actions"
import AppBar from "material-ui/AppBar"
import "../../../styles/Main.css"

export default class Navbar extends Component {
  render() {
    const { profile, dispatch, isAuthenticated, errorMessage } = this.props

    let title = "Welcome " + profile.item.firstname + "..."
    return (
      <div>
        {!isAuthenticated &&
          <Login
            errorMessage={errorMessage}
            onLoginClick={creds => dispatch(loginUser(creds))}
          />}

        {isAuthenticated &&
          <div className="transparentbg">
            <span style={{ float: "left", margin: "15px" }}>
              {title}
            </span>
            <span style={{ float: "right" }}>
              <Logout dispatch={dispatch} />
            </span>
          </div>}
      </div>
    )
  }
}

Navbar.propTypes = {
  dispatch: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  errorMessage: PropTypes.string,
  profile: PropTypes.object.isRequired
}
