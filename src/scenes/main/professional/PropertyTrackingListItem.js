import React, { Component } from "react"
import { PropTypes } from "prop-types"

import IconButton from "material-ui/IconButton"
import "../../../styles/Main.css"

import Badge from "material-ui/Badge"

import TextInput from "../../../components/TextInput"
import Avatar from "material-ui/Avatar"

import {
  Card,
  CardActions,
  CardHeader,
  CardText,
  CardTitle
} from "material-ui/Card"

import CustomerAffordability from "./CustomerAffordability"
import NumberFormat from "react-number-format"

class PropertyTrackingListItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      expanded: false
    }
  }

  handleExpandChange = expanded => {
    this.setState({ expanded: expanded })
  }

  handleToggle = (event, toggle) => {
    this.setState({ expanded: toggle })
  }

  handleExpand = () => {
    this.setState({ expanded: true })
  }

  handleReduce = () => {
    this.setState({ expanded: false })
  }

  render() {
    const { property, customer } = this.props

    return (
      <Card
        className="property-tracking-grid-griditem"
        expanded={this.state.expanded}
        onExpandChange={this.handleExpandChange}
      >
        <CardHeader
          title={
            <span>
              <span className="fieldheader">Affordability</span>
              <br />
              <NumberFormat
                value={property.affordabilityMax}
                displayType={"text"}
                thousandSeparator={true}
                decimalPrecision={2}
                prefix={"$"}
              />/mo
            </span>
          }
          subtitle={
            <span>
              <span className="fieldheader">Price</span>
              <br />
              <NumberFormat
                value={property.price}
                displayType={"text"}
                thousandSeparator={true}
                decimalPrecision={2}
                prefix={"$"}
              />
            </span>
          }
          actAsExpander={true}
          showExpandableButton={true}
        />
        <CardTitle expandable={true}>
          <div>
            BR: {property.BR} BA: {property.BA}
            <br />
            {property.address}
            <br />
            {property.city} {property.zip}
          </div>
        </CardTitle>
        <CardText expandable={true}>
          <CustomerAffordability property={property} customer={customer} />
        </CardText>
        <CardActions>
          <Badge
            badgeContent={10}
            primary={true}
            className="tracking-grid-griditem-button-position"
          >
            <IconButton iconClassName="material-icons" tooltip="comments">
              question_answer
            </IconButton>
          </Badge>
          <Badge
            badgeContent={10}
            primary={true}
            className="tracking-grid-griditem-button-position"
          >
            <IconButton iconClassName="material-icons" tooltip="pictures">
              collections
            </IconButton>
          </Badge>
        </CardActions>
      </Card>
    )
  }
}

PropertyTrackingListItem.propTypes = {
  property: PropTypes.object.isRequired,
  customer: PropTypes.object.isRequired
}

export default PropertyTrackingListItem
