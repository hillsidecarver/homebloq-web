import React, { Component } from "react"
import { PropTypes } from "prop-types"
import { Tabs, Tab } from "material-ui/Tabs"
import FontIcon from "material-ui/FontIcon"
import MapsPersonPin from "material-ui/svg-icons/maps/person-pin"
import Profile from "./Profile"
import TrackingListFilterSort from "./TrackingListFilterSort"

export default class MainNavBar extends Component {
  render() {
    const { dispatch } = this.props

    return (
      <Tabs>
        <Tab
          icon={<FontIcon className="material-icons">home</FontIcon>}
          label="HOME"
        />
        <Tab
          icon={<FontIcon className="material-icons">gps_fixed</FontIcon>}
          label="TRACKING"
        >
          <TrackingListFilterSort dispatch={dispatch} />
        </Tab>
        <Tab icon={<MapsPersonPin />} label="PROFILE">
          <Profile dispatch={dispatch} />
        </Tab>
      </Tabs>
    )
  }
}

MainNavBar.propTypes = {
  dispatch: PropTypes.func.isRequired
}
