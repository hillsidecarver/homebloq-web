import React, { Component } from "react";
import { PropTypes } from "prop-types";

import TrackingList from "./TrackingList";
import IconButton from "material-ui/IconButton";
import "../../../styles/Main.css";

import {
  BottomNavigation,
  BottomNavigationItem
} from "material-ui/BottomNavigation";
import Paper from "material-ui/Paper";
import FontIcon from "material-ui/FontIcon";
import { connect } from "react-redux";

class Tracking extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: -1
    };
  }

  select = index => this.setState({ selectedIndex: index });

  render() {
    const { dispatch, tracking } = this.props;

    return (
      <div className="tracking-grid-container">
        <TrackingList dispatch={dispatch} tracking={tracking} />

        <Paper zDepth={2}>
          <BottomNavigation selectedIndex={this.state.selectedIndex}>
            <BottomNavigationItem
              label="Add"
              icon={<FontIcon className="material-icons">add_circle</FontIcon>}
              onTouchTap={() => this.select(0)}
              onClick={event => this.handleSubmit(event)}
            />
            <BottomNavigationItem
              label="Search"
              icon={<FontIcon className="material-icons">search</FontIcon>}
              onTouchTap={() => this.select(1)}
              onClick={event => this.handleSubmit(event)}
            />
          </BottomNavigation>
        </Paper>
      </div>
    );
  }
}

Tracking.propTypes = {
  dispatch: PropTypes.func.isRequired,
  tracking: PropTypes.array.isRequired
};

function mapStateToProps(state) {
  const { tracking } = state;

  return {
    tracking
  };
}

export default connect(mapStateToProps)(Tracking);
