import React, { Component } from "react"
import { PropTypes } from "prop-types"

import IconButton from "material-ui/IconButton"
import "../../../styles/Main.css"

import FontIcon from "material-ui/FontIcon"
import Chip from "material-ui/Chip"
import Avatar from "material-ui/Avatar"
import { purple200 } from "material-ui/styles/colors"
import Badge from "material-ui/Badge"
import FloatingActionButton from "material-ui/FloatingActionButton"

import { Card, CardActions, CardHeader, CardText } from "material-ui/Card"

import moment from "moment"

import NumberFormat from "react-number-format"

class TrackingListItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      autoScrollBodyContent: true
    }

    this.handleUpdateState = this.handleUpdateState.bind(this)
  }

  renderStatus(item) {
    if (item.state === "SENT")
      return <FontIcon className="material-icons"> mail</FontIcon>
    else if (item.state === "TRACK")
      return <FontIcon className="material-icons"> gps_fixed </FontIcon>
    else if (item.state === "DENY")
      return <FontIcon className="material-icons">pan_tool</FontIcon>
    else return <FontIcon className="material-icons">timer</FontIcon>
  }

  renderWants(item) {
    return "[" + item.map(item => " " + item) + "]"
  }

  renderNeeds(item) {
    return (
      "min $" +
      item.priceMin +
      " max $" +
      item.priceMax +
      " BA " +
      item.BA +
      " BR " +
      item.BR +
      " [" +
      item.needs.map(item => " " + item) +
      " ] "
    )
  }

  renderChip(data) {
    return (
      <div className="tracking-grid-griditem-status">
        <Chip key={data.key} style={{ margin: 0 }} backgroundColor={purple200}>
          {data.state && <Avatar icon={this.renderStatus(data)} />}
          <span className="tracking-grid-griditem-smallfont">
            {moment(data.updateDate).format("llll")}
          </span>
        </Chip>
      </div>
    )
  }

  handleUpdateState(item, newState) {
    this.props.onUpdateState(item, newState)
  }

  handlePropertyTracking(item, dialog) {
    this.props.onPropertyTracking(item, dialog)
  }

  handlePropertyTrackingClose = () =>
    this.setState({
      open: false
    })

  render() {
    const { item } = this.props

    return (
      <Card expanded={true} className="tracking-grid-griditem">
        <CardHeader
          title={`${item.customer.firstname} ${item.customer.lastname}`}
          subtitle={item.customer.status}
        >
          {this.renderChip(item.customer)}
        </CardHeader>
        <CardText style={{ height: 50 }}>
          <div className="tracking-grid-griditem-text">
            {item.customer.need &&
              <span>
                &nbsp;
                <span className="lightFont">Needs: </span>{" "}
                {this.renderNeeds(item.customer.need)}
              </span>}
            {item.customer.wants &&
              <span>
                &nbsp;
                <span className="lightFont">Wants:</span>{" "}
                {this.renderWants(item.customer.wants)}
              </span>}
            {item.customer.targetedZipCodes &&
              <span>
                &nbsp;
                <span className="lightFont">Target ZipCodes:</span>{" "}
                {"[" +
                  item.customer.targetedZipCodes.map(item => " " + item) +
                  " ]"}
              </span>}
          </div>
        </CardText>
        <CardActions>
          {item.customer.state === "TRACK" &&
            <Badge
              badgeContent={item.customer.propertyList.length}
              primary={true}
              className="tracking-grid-griditem-button-position"
            >
              <IconButton iconClassName="material-icons" tooltip="comments">
                question_answer
              </IconButton>
            </Badge>}
          {item.customer.state === "TRACK" &&
            <Badge
              badgeContent={item.customer.propertyList.length}
              primary={true}
              className="tracking-grid-griditem-button-position"
            >
              <IconButton
                iconClassName="material-icons"
                tooltip="properties"
                onClick={event =>
                  this.handlePropertyTracking(item.customer, {
                    title: "Tracking",
                    open: true,
                    modal: true,
                    autoScrollBodyContent: true
                  })}
              >
                home
              </IconButton>
            </Badge>}
          {item.customer.state === "TRACK" &&
            <FloatingActionButton
              backgroundColor="#ff4081"
              label="DENY"
              className="tracking-grid-griditem-button-right"
              onClick={event => this.handleUpdateState(item, "DENY")}
            >
              <FontIcon className="material-icons">thumb_down</FontIcon>
            </FloatingActionButton>}
          {item.customer.state === "CONFIRM" &&
            item.customer.state !== "SENT" &&
            <div>
              <FloatingActionButton
                backgroundColor="#a4c639"
                label="CONFIRM"
                className="tracking-grid-griditem-button-right"
                onClick={event => this.handleUpdateState(item, "TRACK")}
              >
                <FontIcon className="material-icons">thumb_up</FontIcon>
              </FloatingActionButton>
              <FloatingActionButton
                backgroundColor="#ff4081"
                label="DENY"
                onClick={event => this.handleUpdateState(item, "DENY")}
              >
                <FontIcon className="material-icons">thumb_down</FontIcon>
              </FloatingActionButton>
            </div>}
        </CardActions>
      </Card>
    )
  }
}

TrackingListItem.propTypes = {
  item: PropTypes.object.isRequired,
  onUpdateState: PropTypes.func.isRequired
}

export default TrackingListItem
