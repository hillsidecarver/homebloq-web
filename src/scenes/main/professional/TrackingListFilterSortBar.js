import React from "react"
import { PropTypes } from "prop-types"
import Chip from "material-ui/Chip"
import Avatar from "material-ui/Avatar"
import { blue300 } from "material-ui/styles/colors"
import FontIcon from "material-ui/FontIcon"
import Popover from "material-ui/Popover"
import Badge from "material-ui/Badge"
import TextInput from "../../../components/TextInput"
import FloatingActionButton from "material-ui/FloatingActionButton"
import RaisedButton from "material-ui/RaisedButton"
import SelectField from "material-ui/SelectField"
import MenuItem from "material-ui/MenuItem"
import "../../../styles/Main.css"

class TrackingListFilterSortBar extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      trackStatus: true,
      trackState: true,
      comboFiliter: "",
      openPopUpAdd: false,
      openPopUpSearch: false,
      timeLine: "",
      searchAdd: ""
    }

    this.handleTouchTap = this.handleTouchTap.bind(this)
    this.handleRequestClose = this.handleRequestClose.bind(this)
    this.handleComboSearch = this.handleComboSearch.bind(this)

    this.openPopUpAddOpen = this.openPopUpAddOpen.bind(this)
    this.openPopUpAddClose = this.openPopUpAddClose.bind(this)

    this.openPopUpSearchOpen = this.openPopUpSearchOpen.bind(this)
    this.openPopUpSearchClose = this.openPopUpSearchClose.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleInput = this.handleInput.bind(this)
  }

  handleFilter = key => {
    setTimeout(this.props.onFilterTracking(key), 1500)
  }

  handleInvite = () => {
    const { email, firstName, lastName } = this.state
    this.openPopUpAddClose(false)
  }

  handleSearchAdd = () => {
    const { searchAdd } = this.state
  }

  handleChange = (event, index, timeLine) => {
    this.setState({ timeLine })
  }

  openPopUpAddOpen(event) {
    event.preventDefault()
    this.setState({
      openPopUpAdd: true,
      anchorEl: event.currentTarget
    })
  }

  openPopUpSearchOpen(event) {
    event.preventDefault()
    this.setState({
      openPopUpSearch: true,
      anchorEl: event.currentTarget
    })
  }

  openPopUpAddClose(state) {
    this.setState({ openPopUpAdd: false })
  }

  openPopUpSearchClose(state) {
    this.setState({ openPopUpSearch: false })
  }

  handleTouchTap = event => {
    // This prevents ghost click.
    event.preventDefault()

    this.setState({
      openPopUp: true,
      anchorEl: event.currentTarget
    })
  }

  handleRequestClose = () => {
    this.setState({
      openPopUp: false
    })
  }
  handleComboSearch(e) {
    const target = e.target
    const value = target.value
    const name = target.name

    this.setState(
      {
        [name]: value
      },
      () => console.log([name], value)
    )

    this.props.onComboSearch(value)
  }

  handleInput(e) {
    const target = e.target
    const value = target.value
    const name = target.name

    this.setState(
      {
        [name]: value
      },
      () => console.log([name], value)
    )
  }

  switchBackground = isfilter => (isfilter ? blue300 : null)

  renderStatus(item) {
    if (item.state === "SENT")
      return <FontIcon className="material-icons"> mail</FontIcon>
    else if (item.state === "TRACK")
      return <FontIcon className="material-icons"> gps_fixed </FontIcon>
    else if (item.state === "DENY")
      return <FontIcon className="material-icons">pan_tool</FontIcon>
    else if (item.state === "CONFIRM")
      return <FontIcon className="material-icons">thumb_up</FontIcon>
    else return <FontIcon className="material-icons" />
  }

  renderChip(data) {
    return (
      <Chip
        key={data.key}
        onTouchTap={() => this.handleFilter(data.key)}
        backgroundColor={this.switchBackground(data.filter)}
      >
        {data.state && <Avatar icon={this.renderStatus(data)} />}
        {data.label}
      </Chip>
    )
  }

  render() {
    const { filterTrackList } = this.props
    return (
      <div>
        <Badge
          primary={false}
          className="tracking-grid-grid-button-position"
          badgeContent={filterTrackList().length}
        >
          <FloatingActionButton
            backgroundColor="#ff4081"
            label="Search"
            onTouchTap={this.handleTouchTap}
          >
            <FontIcon className="material-icons">search</FontIcon>
          </FloatingActionButton>
        </Badge>
        <Popover
          open={this.state.openPopUp}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{ horizontal: "left", vertical: "top" }}
          targetOrigin={{ horizontal: "right", vertical: "top" }}
          onRequestClose={this.handleRequestClose}
          className="login-container"
        >
          <TextInput
            uniqueName="comboFiliter"
            text="Smith SENT  &quot;Actively Looking&quot;"
            content={this.state.comboFiliter}
            onChange={this.handleComboSearch}
          />

          {this.props.stateData.map(this.renderChip, this)}
        </Popover>
        <FloatingActionButton
          backgroundColor="#ff4081"
          label="Add"
          onTouchTap={event => this.openPopUpAddOpen(event)}
        >
          <FontIcon className="material-icons">add_circle</FontIcon>
        </FloatingActionButton>
        <Popover
          open={this.state.openPopUpAdd}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{ horizontal: "left", vertical: "top" }}
          targetOrigin={{ horizontal: "right", vertical: "top" }}
          onRequestClose={this.openPopUpAddClose}
          className="tracklist-popover-container"
        >
          <TextInput
            uniqueName="email"
            text="bb@homebloq.com"
            content={this.state.email}
            onChange={this.handleInput}
          />
          <TextInput
            uniqueName="firstName"
            text="First Name"
            content={this.state.firstName}
            onChange={this.handleInput}
          />
          <TextInput
            uniqueName="lastName"
            text="Last Name"
            content={this.state.lastName}
            onChange={this.handleInput}
          />

          <RaisedButton
            label="Invite"
            fullWidth={true}
            onClick={event => this.handleInvite(event)}
          />
        </Popover>
        &nbsp;&nbsp;
        <FloatingActionButton
          backgroundColor="#ff4081"
          label="Add"
          onTouchTap={event => this.openPopUpSearchOpen(event)}
        >
          <FontIcon className="material-icons">search</FontIcon>
        </FloatingActionButton>
        <Popover
          open={this.state.openPopUpSearch}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{ horizontal: "left", vertical: "top" }}
          targetOrigin={{ horizontal: "right", vertical: "top" }}
          onRequestClose={this.openPopUpSearchClose}
          className="tracklist-popover-container"
        >
          <TextInput
            uniqueName="emailAddress"
            text="Email Address"
            content={this.state.emailAddress}
            onChange={this.handleInput}
          />
          <TextInput
            uniqueName="zipCode"
            text="Zip Code"
            content={this.state.zipCode}
            onChange={this.handleInput}
          />

          <TextInput
            uniqueName="affordabilityMin"
            text="Affordability Min"
            content={this.state.affordabilityMin}
            onChange={this.handleInput}
          />

          <TextInput
            uniqueName="affordabilityMax"
            text="Affordability Max"
            content={this.state.affordabilityMax}
            onChange={this.handleInput}
          />

          <SelectField
            floatingLabelText="Timeline"
            value={this.state.timeLine}
            onChange={this.handleChange}
          >
            <MenuItem
              value={"Just Starting Out"}
              primaryText="Just Starting Out"
            />
            <MenuItem
              value={"Actively Looking"}
              primaryText="Actively Looking"
            />
            <MenuItem
              value={"Actively Looking w/ Agent"}
              primaryText="Actively Looking w/ Agent"
            />
            <MenuItem
              value={"Looking without Agent"}
              primaryText="Looking without Agent"
            />
          </SelectField>

          <RaisedButton
            label="Search"
            fullWidth={true}
            onClick={event => this.handleInvite(event)}
          />
        </Popover>
      </div>
    )
  }
}

TrackingListFilterSortBar.propTypes = {
  stateData: PropTypes.array.isRequired,
  onFilterTracking: PropTypes.func.isRequired,
  filterTrackList: PropTypes.func.isRequired,
  onComboSearch: PropTypes.func.isRequired
}

export default TrackingListFilterSortBar
