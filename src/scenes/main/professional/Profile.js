import React, { Component } from "react"
import { PropTypes } from "prop-types"
import { connect } from "react-redux"
import { saveProfile } from "../../../actions"
import TextInput from "../../../components/TextInput"
import "../../../styles/Main.css"
import FontIcon from "material-ui/FontIcon"

import {
  BottomNavigation,
  BottomNavigationItem
} from "material-ui/BottomNavigation"
import Paper from "material-ui/Paper"
import { Card, CardHeader, CardText } from "material-ui/Card"
import Checkbox from "material-ui/Checkbox"

class Profile extends Component {
  constructor(props) {
    super(props)

    this.state = Object.assign({}, this.props.profile.item, this.state)

    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.validateEmail = this.validateEmail.bind(this)
    this.isEnabled = this.isEnabled.bind(this)
  }

  handleSubmit(e) {
    e.preventDefault()

    const { dispatch } = this.props

    dispatch(saveProfile(this.state))
  }

  handleChange(e) {
    const target = e.target
    const value = target.type === "checkbox" ? target.checked : target.value
    const name = target.name

    this.setState(
      {
        [name]: value
      },
      () => console.log([name], value)
    )
  }

  select = index => this.setState({ selectedIndex: index })

  validateEmail(value) {
    return value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
      ? "Invalid email address"
      : undefined
  }

  isEnabled() {
    const {
      firstname,
      lastname,
      company,
      realEstateExperience,
      licenseNumber,
      email,
      professionalSummary,
      targetedZipcodes
    } = this.state

    return (
      email &&
      email.trim().length > 5 &&
      (firstname && firstname.trim().length > 1) &&
      (lastname && lastname.trim().length > 1) &&
      (company && company.trim().length > 5) &&
      (realEstateExperience && realEstateExperience > 0) &&
      (professionalSummary && professionalSummary.trim().length > 50) &&
      (licenseNumber && licenseNumber.trim().length > 5) &&
      (targetedZipcodes && targetedZipcodes.trim().length > 4)
    )
  }

  render() {
    const {
      errorMessage,
      firstname,
      lastname,
      company,
      website,
      realEstateExperience,
      licenseNumber,
      email,
      professionalSummary,
      targetedZipcodes,
      emailNotificationTracking
    } = this.state

    let isEnabled = this.isEnabled()
    if (isEnabled) this.select(0)

    return (
      <div className="container">
        <Paper zDepth={2}>
          <Card>
            <CardHeader>Profile</CardHeader>
            <CardText>
              {/*
               <TextInput
                uniqueName="linkedinprofile"
                text="Linkedin Profile"
                minCharacters={6}
                onChange={this.handleChange}
                content={linkedinprofile}
                errorMessage="LinkedIn is invalid"
                emptyMessage="LinkedIn is required" />

            <br/>
            */}
              <TextInput
                uniqueName="company"
                text="Company"
                minCharacters={6}
                required={true}
                onChange={this.handleChange}
                content={company}
                errorMessage="Company is invalid"
                emptyMessage="Company is required"
              />

              <br />
              <TextInput
                uniqueName="email"
                text="Email"
                minCharacters={6}
                onChange={this.handleChange}
                content={email}
                validate={this.validateEmail}
                required={true}
                errorMessage="Email is invalid"
                emptyMessage="Email is required"
              />
              <br />
              <TextInput
                uniqueName="website"
                text="Website"
                minCharacters={6}
                onChange={this.handleChange}
                content={website}
                errorMessage="Website is invalid"
                emptyMessage="Website is required"
              />
              <br />

              <TextInput
                uniqueName="firstname"
                text="First Name"
                minCharacters={2}
                required={true}
                onChange={this.handleChange}
                content={firstname}
                errorMessage="First is invalid"
                emptyMessage="First is required"
              />

              <br />

              <TextInput
                uniqueName="lastname"
                text="Last Name"
                minCharacters={2}
                required={true}
                onChange={this.handleChange}
                content={lastname}
                errorMessage="Last Name is invalid"
                emptyMessage="Last Name is required"
              />

              <br />
              <TextInput
                uniqueName="licenseNumber"
                text="License Number"
                minCharacters={6}
                required={true}
                onChange={this.handleChange}
                content={licenseNumber}
                errorMessage="License Number is invalid"
                emptyMessage="License Number  is required"
              />
              <br />
              <TextInput
                uniqueName="targetedZipcodes"
                text="Targeted Zipcodes (60622 60625 60614)"
                minCharacters={5}
                required={true}
                onChange={this.handleChange}
                content={targetedZipcodes}
                errorMessage="Targeted Zipcodes is invalid"
                emptyMessage="Targeted Zipcodes is required"
              />
              <br />

              <TextInput
                uniqueName="realEstateExperience"
                text="Real Estate Experience (years)"
                minCharacters={1}
                required={true}
                onChange={this.handleChange}
                content={realEstateExperience}
                errorMessage="Real Estate Experience is invalid"
                emptyMessage="Real Estate Experience is required"
              />

              <br />

              <TextInput
                uniqueName="professionalSummary"
                text="Professional Summary (min 50 char)"
                minCharacters={150}
                required={true}
                onChange={this.handleChange}
                content={professionalSummary}
                multiLine={true}
                rows={2}
                errorMessage="Professional Summary is invalid"
                emptyMessage="Professional Summary is required"
              />
            </CardText>
          </Card>
        </Paper>
        <br />
        <Paper zDepth={2}>
          <Card>
            <CardHeader>Settings</CardHeader>
            <CardText>
              <Checkbox
                name="emailNotificationTracking"
                label="Receive email copy incoming tracking request"
                onClick={this.handleChange}
                checked={emailNotificationTracking}
              />
            </CardText>
          </Card>
        </Paper>
        <br />
        {errorMessage &&
          <p style={{ color: "red" }}>
            {errorMessage}
          </p>}
        <br />
        <Paper zDepth={2}>
          <BottomNavigation selectedIndex={this.state.selectedIndex}>
            <BottomNavigationItem
              label="Save Profile"
              icon={<FontIcon className="material-icons">save</FontIcon>}
              onTouchTap={() => this.select(0)}
              onClick={event => this.handleSubmit(event)}
              disabled={!isEnabled}
            />
          </BottomNavigation>
        </Paper>
      </div>
    )
  }
}

Profile.propTypes = {
  dispatch: PropTypes.func.isRequired,
  errorMessage: PropTypes.string,
  profile: PropTypes.object.isRequired
}

function mapStateToProps(state) {
  const { profile } = state

  return {
    profile
  }
}

export default connect(mapStateToProps)(Profile)
