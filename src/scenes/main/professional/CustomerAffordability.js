import React, { Component } from "react"
import { PropTypes } from "prop-types"

import IconButton from "material-ui/IconButton"
import "../../../styles/Main.css"

import TextInput from "../../../components/TextInput"

import CurrencyInput from "react-currency-input"
import NumberFormat from "react-number-format"

class CustomerAffordability extends Component {
  constructor(props) {
    super(props)
    this.state = {
      PITI: "",
      PnI: "",
      monthlyInsurance: "",
      monthlyTaxes: "",
      PMI: "",
      HOA: "",
      downPayment: "",
      mortgage: "",
      totalCashToClose: "",
      cashToClose: "",
      rainyDayFund: ""
    }

    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(e) {
    const target = e.target
    const value = target.type === "checkbox" ? target.checked : target.value
    const name = target.name

    this.setState(
      {
        [name]: value
      },
      () => console.log([name], value)
    )
  }

  render() {
    const { property, customer } = this.props
    const {
      PITI,
      PnI,
      monthlyInsurance,
      monthlyTaxes,
      PMI,
      HOA,
      downPayment,
      mortgage,
      closingCosts,
      totalCashToClose,
      rainyDayFund
    } = this.state

    return (
      <div>
        <span className="fieldheader">Payment Estimates</span>
        <br />
        <NumberFormat
          customInput={TextInput}
          value={property.PITI}
          displayType={"text"}
          thousandSeparator={true}
          decimalPrecision={2}
          prefix={"$"}
        />
        <br />
        <span className="fieldheader">Total Housing Payment</span>
        <br />
        <NumberFormat
          customInput={TextInput}
          value={property.PnI}
          displayType={"text"}
          thousandSeparator={true}
          decimalPrecision={2}
          prefix={"$"}
        />
        <br />
        <span className="fieldheader">Monthly Insurance</span>
        <br />
        <NumberFormat
          customInput={TextInput}
          value={property.monthlyInsurance}
          displayType={"text"}
          thousandSeparator={true}
          decimalPrecision={2}
          prefix={"$"}
        />
        <br />
        <span className="fieldheader">Monthly Taxes</span>
        <br />
        <NumberFormat
          customInput={TextInput}
          value={property.monthlyTaxes}
          displayType={"text"}
          thousandSeparator={true}
          decimalPrecision={2}
          prefix={"$"}
        />
        <br />
        <span className="fieldheader">PMI</span>
        <br />
        <NumberFormat
          customInput={TextInput}
          value={property.monthlyPMI}
          displayType={"text"}
          thousandSeparator={true}
          decimalPrecision={2}
          prefix={"$"}
        />
        <br />
        <span className="fieldheader">Monthly HOA</span>
        <br />
        <NumberFormat
          customInput={TextInput}
          value={property.monthlyHOA}
          displayType={"text"}
          thousandSeparator={true}
          decimalPrecision={2}
          prefix={"$"}
        />
        <br />
        <span className="fieldheader">Closing Costs</span>
        <br />
        <NumberFormat
          customInput={TextInput}
          value={property.closingCosts}
          displayType={"text"}
          thousandSeparator={true}
          decimalPrecision={2}
          prefix={"$"}
        />
        <br />
        <span className="fieldheader">Down Payment</span>
        <br />
        <NumberFormat
          customInput={TextInput}
          value={customer.downPayment}
          displayType={"text"}
          thousandSeparator={true}
          decimalPrecision={2}
          prefix={"$"}
        />
        <br />
        <span className="fieldheader">Mortgage</span>
        <br />
        <NumberFormat
          customInput={TextInput}
          value={property.mortgage}
          displayType={"text"}
          thousandSeparator={true}
          decimalPrecision={2}
          prefix={"$"}
        />
        <br />
        <span className="fieldheader">Total Cash To Close</span>
        <br />
        <NumberFormat
          customInput={TextInput}
          value={customer.totalCashToClose}
          displayType={"text"}
          thousandSeparator={true}
          decimalPrecision={2}
          prefix={"$"}
        />
        <br />
        <span className="fieldheader">Rainy Day Fund</span>
        <br />
        <NumberFormat
          customInput={TextInput}
          value={customer.emergencyFund}
          displayType={"text"}
          thousandSeparator={true}
          decimalPrecision={2}
          prefix={"$"}
        />
      </div>
    )
  }
}

CustomerAffordability.propTypes = {
  property: PropTypes.object.isRequired,
  customer: PropTypes.object.isRequired
}

export default CustomerAffordability
