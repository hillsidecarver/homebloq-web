import React, { Component } from "react"
import { PropTypes } from "prop-types"

import "../../../styles/Main.css"

import FontIcon from "material-ui/FontIcon"
import Chip from "material-ui/Chip"
import Avatar from "material-ui/Avatar"
import { purple200 } from "material-ui/styles/colors"

import moment from "moment"
import DialogScrollable from "../../../lib/DialogScrollable"
import PropertyTrackingListFilterSort from "./PropertyTrackingListFilterSort"
import TrackingListItem from "./TrackingListItem"

class TrackingList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      title: "",
      open: false,
      modal: false,
      autoScrollBodyContent: false,
      customer: {}
    }

    this.handleUpdateState = this.handleUpdateState.bind(this)
    this.handlePropertyTracking = this.handlePropertyTracking.bind(this)
  }

  renderStatus(item) {
    if (item.state === "SENT")
      return <FontIcon className="material-icons"> mail</FontIcon>
    else if (item.state === "TRACK")
      return <FontIcon className="material-icons"> gps_fixed </FontIcon>
    else if (item.state === "DENY")
      return <FontIcon className="material-icons">pan_tool</FontIcon>
    else return <FontIcon className="material-icons">timer</FontIcon>
  }

  renderWants(item) {
    return "[" + item.map(item => " " + item) + "]"
  }

  renderNeeds(item) {
    return (
      "min$ " +
      item.priceMin +
      " max$ " +
      item.priceMax +
      " BA " +
      item.BA +
      " BR " +
      item.BR +
      " [" +
      item.needs.map(item => " " + item) +
      " ] "
    )
  }

  renderChip(data) {
    return (
      <div className="tracking-grid-griditem-status">
        <Chip key={data.key} style={{ margin: 0 }} backgroundColor={purple200}>
          {data.state && <Avatar icon={this.renderStatus(data)} />}
          <span className="tracking-grid-griditem-smallfont">
            {moment(data.updateDate).format("llll")}
          </span>
        </Chip>
      </div>
    )
  }

  handleUpdateState(item, newState) {
    this.props.onUpdateState(item, newState)
  }

  handlePropertyTracking(item, dialog) {
    this.setState({
      title: dialog.title,
      modal: dialog.modal,
      open: dialog.open,
      autoScrollBodyContent: false,
      customer: item
    })
  }

  handlePropertyTrackingClose = () =>
    this.setState({
      open: false
    })

  render() {
    const { filterTrackList } = this.props
    const { title, open, modal, autoScrollBodyContent, customer } = this.state

    return (
      <div className="tracking-grid-gridlist">
        <ul>
          {filterTrackList().map(item =>
            <li key={item.Id}>
              <TrackingListItem
                item={item}
                onUpdateState={this.handleUpdateState}
                onPropertyTracking={this.handlePropertyTracking}
              />
            </li>
          )}
        </ul>
        <DialogScrollable
          title={title}
          modal={modal}
          open={open}
          autoScrollBodyContent={autoScrollBodyContent}
          onHandleClose={this.handlePropertyTrackingClose}
          className={{ width: "100%", maxWidth: "none" }}
        >
          <PropertyTrackingListFilterSort customer={customer} />
        </DialogScrollable>
      </div>
    )
  }
}

TrackingList.propTypes = {
  filterTrackList: PropTypes.func.isRequired,
  onUpdateState: PropTypes.func.isRequired
}

export default TrackingList
