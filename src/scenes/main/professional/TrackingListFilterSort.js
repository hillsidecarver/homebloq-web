import React from "react"
import { connect } from "react-redux"
import { PropTypes } from "prop-types"
import { updateTrackState } from "../../../actions"
import TrackingList from "./TrackingList"
import FontIcon from "material-ui/FontIcon"
import Drawer from "material-ui/Drawer"
import FloatingActionButton from "material-ui/FloatingActionButton"
import {
  sortBy,
  filter,
  find,
  map,
  mapKeys,
  words,
  some,
  every,
  values,
  intersection,
  flatten,
  lowerCase,
  toLower,
  flattenDeep,
  isObject
} from "lodash"
import moment from "moment"

import {
  BottomNavigation,
  BottomNavigationItem
} from "material-ui/BottomNavigation"

import TrackingListFilterSortBar from "./TrackingListFilterSortBar"
import Popover from "material-ui/Popover"

class TrackingListFilterSort extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      openHelp: false,
      selectedIndex: -1,

      sortList: ["customer.state", "customer.status"],
      filterTrackList: sortBy(this.props.tracking.items, this.sortList),
      stateData: this.createFilterItems(),
      comboFilterList: []
    }

    this.createFilterItems = this.createFilterItems.bind(this)
    this.handleFilterTrackingList = this.handleFilterTrackingList.bind(this)
    this.handleFilterTracking = this.handleFilterTracking.bind(this)
    this.handleUpdateState = this.handleUpdateState.bind(this)
    this.handleComboSearch = this.handleComboSearch.bind(this)
    this.openPopUpAddOpen = this.openPopUpAddOpen.bind(this)
    this.openPopUpAddClose = this.openPopUpAddClose.bind(this)
    this.openPopUpSearch = this.openPopUpSearch.bind(this)
  }
  select = index => this.setState({ selectedIndex: index })

  openPopUpAddOpen(event) {
    event.preventDefault()
    this.setState({
      openPopUpAdd: true,
      anchorEl: event.currentTarget
    })
  }

  openPopUpAddClose(state) {
    this.setState({ openPopUpAdd: false })
  }

  openPopUpSearch(event) {
    event.preventDefault()
    this.setState({
      openPopUpSearch: true,
      anchorEl: event.currentTarget
    })
  }

  createFilterItems(filterItems) {
    let stateDataList = []

    let tracking = this.props.tracking.items

    let stateTally = {}
    let statusTally = {}
    map(tracking, function(item) {
      stateTally[item.customer.state] = !stateTally[item.customer.state]
        ? 1
        : stateTally[item.customer.state] + 1
      statusTally[item.customer.status] = !statusTally[item.customer.status]
        ? 1
        : statusTally[item.customer.status] + 1
    })

    let idx = 0
    mapKeys(stateTally, function(value, key) {
      let origStateList =
        filterItems && filterItems.find(item => item.label === key)
      stateDataList.push({
        key: idx++,
        state: key,
        label: key,
        count: value,
        filter: origStateList ? origStateList.filter : true
      })
    })

    mapKeys(statusTally, function(value, key) {
      let origStateList =
        filterItems && filterItems.find(item => item.label === key)

      stateDataList.push({
        key: idx++,
        status: key,
        label: key,
        count: value,
        filter: origStateList ? origStateList.filter : true
      })
    })

    return stateDataList
  }

  updateFilterSortBar(key) {
    let stateData = this.state.stateData
    const chipToToggle = stateData.find(item => item.key === key)

    chipToToggle.filter = !chipToToggle.filter

    this.setState({ stateData: stateData })
  }

  handleFilterTracking(key) {
    this.updateFilterSortBar(key)
  }

  handleUpdateState(item, newState) {
    const { dispatch } = this.props
    let list = this.props.tracking.items
    let newItem = list.find(olditem => olditem.customer.Id === item.customer.Id)

    newItem.customer.state = newState
    newItem.updateDate = moment().format("llll")

    this.setState({ stateData: this.createFilterItems(this.state.stateData) })

    dispatch(updateTrackState(item, list))
  }

  handleComboSearch(filterString) {
    this.setState({
      comboFilterList: words(filterString, /\w+|"[^"]+"/g).map(item =>
        item.replace(/\"/g, "")
      )
    })
  }

  handleFilterTrackingList() {
    let tracking = this.props.tracking.items

    const { stateData, comboFilterList } = this.state

    let results = filter(tracking, function(track) {
      return (
        find(stateData, function(field) {
          return (
            track.customer.state &&
            track.customer.state === field.state &&
            field.filter
          )
        }) &&
        find(stateData, function(field) {
          return (
            track.customer.status &&
            track.customer.status === field.status &&
            field.filter
          )
        }) &&
        (comboFilterList.length === 0 ||
          (comboFilterList.length > 0 &&
            intersection(
              comboFilterList.map(item => toLower(item)),
              flattenDeep(
                values(track.customer).map(item => {
                  return isObject(item)
                    ? values(item).map(item => {
                        return isObject(item)
                          ? values(item).map(item => {
                              return isObject(item)
                                ? values(item)
                                : toLower(item)
                            })
                          : toLower(item)
                      })
                    : toLower(item)
                })
              )
            ).length >= comboFilterList.length))
      )
    })

    return sortBy(results, this.state.sortList)
  }

  handleToggle = () => this.setState({ openHelp: !this.state.openHelp })

  render() {
    return (
      <div>
        <Drawer open={this.state.openHelp}>
          <div>DENY, CONFIRM, TRACK, etc</div>
        </Drawer>

        <table>
          <tbody>
            <tr>
              <td>
                <div>
                  <span style={{ float: "left" }}>
                    <TrackingListFilterSortBar
                      stateData={this.state.stateData}
                      onFilterTracking={this.handleFilterTracking}
                      filterTrackList={this.handleFilterTrackingList}
                      onComboSearch={this.handleComboSearch}
                    />
                  </span>
                  <span style={{ float: "right" }}>
                    <FloatingActionButton
                      backgroundColor="#a4c639"
                      label="HELP"
                      onTouchTap={this.handleToggle}
                    >
                      <FontIcon className="material-icons">help</FontIcon>
                    </FloatingActionButton>
                  </span>
                </div>
              </td>
            </tr>
            <tr>
              <td>
                <div className="tracking-grid-gridlist">
                  <TrackingList
                    filterTrackList={this.handleFilterTrackingList}
                    onUpdateState={this.handleUpdateState}
                  />
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  }
}

TrackingListFilterSort.propTypes = {
  dispatch: PropTypes.func.isRequired
}

function mapStateToProps(state) {
  const { tracking } = state

  return {
    tracking
  }
}

export default connect(mapStateToProps)(TrackingListFilterSort)
