import React from "react"
import { PropTypes } from "prop-types"

import PropertyTrackingListFilterSortBar from "./PropertyTrackingListFilterSortBar"
import PropertyTrackingList from "./PropertyTrackingList"

import "../../../styles/Main.css"
import { find } from "lodash"

import { affordAbilityMax } from "../../../lib/affordabilitymax"
import { map } from "lodash"

class PropertyTrackingListFilterSort extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      stateData: [],
      filterTrackList: [],
      filter: {}
    }

    this.handleFilterTrackingList = this.handleFilterTrackingList.bind(this)
    this.handleSetFilter = this.handleSetFilter.bind(this)

    this.updateFilterTrackingList = this.updateFilterTrackingList.bind(this)
  }

  handleSetFilter(filter) {
    this.setState({ filter: filter })
  }

  updateFilterTrackingList(customer) {
    let aam = affordAbilityMax(
      customer.monthlyGross,
      customer.expenses,
      customer.debt,
      customer.homeType,
      customer.homeUse,
      customer.creditScoreRange,
      customer.mortgageType,
      customer.totalCashToClose
    )

    let updateditems = map(customer.propertyList, function(item) {
      item.PITI = aam.PITI
      item.PnI = aam.PnI
      item.mortgage = aam.Mortgage
      item.affordabilityMax = aam.MaxHome - item.price
    })

    return customer
  }

  handleFilterTrackingList() {
    let tracking = this.updateFilterTrackingList(this.props.customer)
    let filter = this.state.filter

    let results = tracking.propertyList.filter(function(item) {
      return (
        ((!filter.priceMin || item.price >= filter.priceMin) &&
          (!filter.priceMax ||
            (item.price <= filter.priceMax &&
              (!filter.affordabilityMin ||
                item.affordability >= filter.affordabilityMin) &&
              !filter.affordabilityMax))) ||
        item.affordability <= filter.affordabilityMax
      )
    })

    return Object.assign({}, tracking, { propertyList: results })
  }

  render() {
    return (
      <table>
        <tbody>
          <tr>
            <td>
              <div>
                <PropertyTrackingListFilterSortBar
                  filterTrackList={this.handleFilterTrackingList}
                  onSetFilter={this.handleSetFilter}
                />
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div className="tracking-grid-gridlist">
                <PropertyTrackingList
                  filterTrackList={this.handleFilterTrackingList()}
                />
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    )
  }
}

PropertyTrackingListFilterSort.propTypes = {
  customer: PropTypes.object.isRequired
}

export default PropertyTrackingListFilterSort
