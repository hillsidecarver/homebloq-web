import React from "react"
import { PropTypes } from "prop-types"
import TextInput from "../../../components/TextInput"
import Popover from "material-ui/Popover"
import Badge from "material-ui/Badge"
import "../../../styles/Main.css"
import FloatingActionButton from "material-ui/FloatingActionButton"
import FontIcon from "material-ui/FontIcon"

class PropertyTrackingListFilterSortBar extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      priceMin: "",
      priceMax: "",
      affordabilityMin: "",
      affordabilityMax: "",
      minBR: "",
      maxBR: "",
      minBA: "",
      maxBA: "",
      openPopUp: false
    }

    this.handleFilter = this.handleFilter.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleTouchTap = this.handleTouchTap.bind(this)
    this.handleRequestClose = this.handleRequestClose.bind(this)
  }

  handleFilter = () => {
    setTimeout(this.props.onSetFilter(this.state), 1500)
  }

  handleChange(e) {
    const target = e.target
    const value = target.type === "checkbox" ? target.checked : target.value
    const name = target.name

    this.setState(
      {
        [name]: value
      },
      () => console.log([name], value)
    )

    this.handleFilter()
  }

  handleTouchTap = event => {
    // This prevents ghost click.
    event.preventDefault()

    this.setState({
      openPopUp: true,
      anchorEl: event.currentTarget
    })
  }

  handleRequestClose = () => {
    this.setState({
      openPopUp: false
    })
  }

  render() {
    const {
      priceMin,
      priceMax,
      affordabilityMin,
      affordabilityMax
    } = this.state

    const { filterTrackList } = this.props

    return (
      <div>
        <Badge
          badgeContent={filterTrackList().propertyList.length}
          primary={false}
          className="tracking-grid-grid-button-position"
        >
          <FloatingActionButton
            backgroundColor="#ff4081"
            hoverColor="#cc3367"
            label="Search"
            onTouchTap={this.handleTouchTap}
          >
            <FontIcon className="material-icons">search</FontIcon>
          </FloatingActionButton>
        </Badge>
        <Popover
          open={this.state.openPopUp}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{ horizontal: "left", vertical: "bottom" }}
          targetOrigin={{ horizontal: "left", vertical: "top" }}
          onRequestClose={this.handleRequestClose}
          className="login-container"
        >
          <TextInput
            uniqueName="priceMin"
            text="priceMin"
            onChange={this.handleChange}
            content={priceMin}
          />
          <TextInput
            uniqueName="priceMax"
            text="priceMax"
            onChange={this.handleChange}
            content={priceMax}
          />
          <TextInput
            uniqueName="affordabilityMin"
            text="affordabilityMin"
            onChange={this.handleChange}
            content={affordabilityMin}
          />
          <TextInput
            uniqueName="affordabilityMax"
            text="affordabilityMax"
            onChange={this.handleChange}
            content={affordabilityMax}
          />
        </Popover>
      </div>
    )
  }
}

PropertyTrackingListFilterSortBar.propTypes = {
  filterTrackList: PropTypes.func.isRequired,
  onSetFilter: PropTypes.func.isRequired
}

export default PropertyTrackingListFilterSortBar
