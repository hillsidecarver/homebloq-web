import React, { Component } from "react"
import { PropTypes } from "prop-types"
import PropertyTrackingListItem from "./PropertyTrackingListItem"

import "../../../styles/Main.css"

class PropertyTrackingList extends Component {
  render() {
    const { filterTrackList } = this.props

    return (
      <div className="tracking-grid-gridlist">
        <ul>
          {filterTrackList.propertyList.map(item =>
            <li className="property-tracking-grid-griditem">
              <PropertyTrackingListItem
                property={item}
                customer={filterTrackList}
              />
            </li>
          )}
        </ul>
      </div>
    )
  }
}

PropertyTrackingList.propTypes = {
  filterTrackList: PropTypes.object.isRequired
}

export default PropertyTrackingList
