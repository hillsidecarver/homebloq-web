import React, { Component } from "react"
import { connect } from "react-redux"
import { PropTypes } from "prop-types"
import RaisedButton from "material-ui/RaisedButton"
import "../../styles/Main.css"
import TextInput from "../../components/TextInput"
import CircularProgress from "material-ui/CircularProgress"

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: null,
      password: null
    }

    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.isEnabled = this.isEnabled.bind(this)
    this.validateEmail = this.validateEmail.bind(this)
  }

  handleSubmit(e) {
    e.preventDefault()
    const username = this.state.username
    const password = this.state.password
    const creds = { username: username.trim(), password: password.trim() }
    this.props.onLoginClick(creds)
  }

  handleChange(e) {
    const target = e.target
    const value = target.type === "checkbox" ? target.checked : target.value
    const name = target.name

    this.setState(
      {
        [name]: value
      },
      () => console.log([name], value)
    )
  }

  isEnabled() {
    const { username, password } = this.state
    return (
      username &&
      username.trim().length > 5 &&
      (password && password.trim().length > 5)
    )
  }

  validateEmail(value) {
    return value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
      ? "Invalid email address"
      : undefined
  }

  render() {
    const { errorMessage, auth } = this.props
    let isEnabled = this.isEnabled()

    return (
      <div className="container">
        <div>
          <div className="logo-form">
            <div className="main-logo">
              <div className="logo-text">Home</div>
              <div className="logo-text-bloq">Bloq</div>
            </div>
            <br />
            <div className="login-container">
              <div className="login-form">
                <TextInput
                  uniqueName="username"
                  text="Email Address"
                  required={true}
                  minCharacters={6}
                  validate={this.validateEmail}
                  onChange={this.handleChange}
                  content={this.state.username}
                  errorMessage="Email is invalid"
                  emptyMessage="Email is required"
                />

                <TextInput
                  uniqueName="password"
                  text="Password"
                  required={true}
                  minCharacters={6}
                  onChange={this.handleChange}
                  content={this.state.password}
                  errorMessage="Password is invalid"
                  emptyMessage="Password is required"
                />
                <RaisedButton
                  label="Login"
                  disabled={!isEnabled}
                  fullWidth={true}
                  onClick={event => this.handleSubmit(event)}
                >
                  {auth.isFetching && <CircularProgress size={18} />}
                </RaisedButton>
                {errorMessage &&
                  <p style={{ color: "red" }}>
                    {errorMessage}
                  </p>}
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Login.propTypes = {
  onLoginClick: PropTypes.func.isRequired,
  errorMessage: PropTypes.string
}

function mapStateToProps(state) {
  const { auth } = state

  return {
    auth
  }
}

export default connect(mapStateToProps)(Login)
