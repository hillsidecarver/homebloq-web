import React, { Component } from "react";
import { PropTypes } from "prop-types";
import IconButton from "material-ui/IconButton";
import { logoutUser } from "../../actions";

export default class Logout extends Component {
  render() {
    const { dispatch } = this.props;

    return (
      <div>
        <IconButton
          iconClassName="material-icons"
          tooltip="Logout"
          onClick={() => dispatch(logoutUser())}
        >
          exit_to_app
        </IconButton>
      </div>
    );
  }
}

Logout.propTypes = {
  dispatch: PropTypes.func
};
