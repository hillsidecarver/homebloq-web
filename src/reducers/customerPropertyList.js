import {
  REQUEST_CUSTOMER_PROPERTY_TRACKING,
  RECEIVE_CUSTOMER_PROPERTY_TRACKING
} from "../actions"

export const customerPropertyList = (
  state = {
    isFetching: false,
    didInvalidate: false,
    items: []
  },
  action
) => {
  switch (action.type) {
    case REQUEST_CUSTOMER_PROPERTY_TRACKING:
      return {
        ...state,
        isFetching: true,
        didInvalidate: false
      }
    case RECEIVE_CUSTOMER_PROPERTY_TRACKING:
      return {
        ...state,
        isFetching: false,
        items: action.list,
        lastUpdated: action.receivedAt
      }
    default:
      return state
  }
}
