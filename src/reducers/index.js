import thunkMiddleware from "redux-thunk";
import { createStore, applyMiddleware } from "redux";

import { REQUEST_POSTS, RECEIVE_POSTS } from "../actions";
import { combineReducers } from "redux";

const posts = (
  state = {
    isFetching: false,
    didInvalidate: false,
    items: []
  },
  action
) => {
  switch (action.type) {
    case REQUEST_POSTS:
      return {
        ...state,
        isFetching: true,
        didInvalidate: false
      };
    case RECEIVE_POSTS:
      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        items: action.posts,
        lastUpdated: action.receivedAt
      };
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  posts
});

let store = createStore(
  rootReducer,
  applyMiddleware(
    thunkMiddleware // lets us dispatch() functions
  )
);

export default store;
