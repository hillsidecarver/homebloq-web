import { SAVE_PROFILE_REQUEST, RECEIVE_PROFILE_REQUEST } from "../actions";

export const profile = (
  state = {
    isFetching: false,
    item: {}
  },
  action
) => {
  switch (action.type) {
    case SAVE_PROFILE_REQUEST:
      return {
        ...state,
        isFetching: true,
        item: action.profile
      };
    case RECEIVE_PROFILE_REQUEST:
      return {
        ...state,
        isFetching: false,
        items: action.posts,
        lastUpdated: action.receivedAt,
        item: action.profile
      };
    default:
      return state;
  }
};
