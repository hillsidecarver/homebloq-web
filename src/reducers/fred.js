import { FRED_REQUEST, FRED_FAILURE } from "../actions"

export const fred = (
  state = {
    isFetching: false,
    item: {}
  },
  action
) => {
  switch (action.type) {
    case FRED_REQUEST:
      return {
        ...state,
        isFetching: true,
        item: action.data
      }
    case FRED_FAILURE:
      return {
        ...state,
        isFetching: false,
        items: action.error
      }
    default:
      return state
  }
}
