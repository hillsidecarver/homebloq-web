import {
  REQUEST_TRACKING,
  RECEIVE_TRACKING,
  UPDATE_TRACKING_REQUEST,
  UPDATE_TRACKING_RECEIVE
} from "../actions"

export const tracking = (
  state = {
    isFetching: false,
    didInvalidate: false,
    items: []
  },
  action
) => {
  switch (action.type) {
    case REQUEST_TRACKING:
      return {
        ...state,
        isFetching: true,
        didInvalidate: false
      }
    case RECEIVE_TRACKING:
      return {
        ...state,
        isFetching: false,
        items: action.tracking,
        lastUpdated: action.receivedAt
      }
    case UPDATE_TRACKING_REQUEST:
      return {
        ...state,
        isFetching: true,
        item: action.item
      }
    case UPDATE_TRACKING_RECEIVE:
      return {
        ...state,
        isFetching: false,
        items: action.tracking,
        lastUpdated: action.receivedAt
      }
    default:
      return state
  }
}
