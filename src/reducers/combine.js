import thunkMiddleware from "redux-thunk"
import { compose, createStore, applyMiddleware } from "redux"
import { combineReducers } from "redux"
import { post } from "./post"
import { auth } from "./auth"
import { profile } from "./profile"
import { tracking } from "./tracking"
import { fred } from "./fred"
import { customerPropertyList } from "./customerPropertyList"

import { autoRehydrate } from "redux-persist"

const rootReducer = combineReducers({
  post,
  auth,
  profile,
  tracking,
  fred,
  customerPropertyList
})

let store = createStore(
  rootReducer,
  compose(
    applyMiddleware(
      thunkMiddleware // lets us dispatch() functions
    ),
    autoRehydrate()
  )
)

export default store
