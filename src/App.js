import React, { Component } from "react"
import { PropTypes } from "prop-types"
import "./App.css"
import { connect } from "react-redux"
import {
  loginUser,
  logoutUser,
  retrieveTracking,
  retrieveCustomersProperties,
  loadFred
} from "./actions"
import Login from "./scenes/auth/Login"
import Logout from "./scenes/auth/Logout"
import Navbar from "./scenes/main/professional/Navbar"
import MainNavBar from "./scenes/main/professional/MainNavBar"

class App extends Component {
  constructor(props) {
    super(props)

    this.onLoginClick = this.onLoginClick.bind(this)
  }

  onLoginClick(creds) {
    const { dispatch, auth } = this.props

    dispatch(loginUser(creds))
    dispatch(retrieveTracking(auth.username))
    dispatch(retrieveCustomersProperties(auth.username))
    dispatch(loadFred())
  }

  render() {
    const {
      profile,
      dispatch,
      auth,
      tracking,
      customerPropertyList
    } = this.props

    return (
      <div className="App">
        {!auth.isAuthenticated &&
          <Login
            errorMessage={auth.errorMessage}
            onLoginClick={creds => this.onLoginClick(creds)}
          />}

        {auth.isAuthenticated &&
          <div>
            <Navbar
              isAuthenticated={auth.isAuthenticated}
              errorMessage={auth.errorMessage}
              dispatch={dispatch}
              profile={profile}
            >
              <Logout onLogoutClick={() => dispatch(logoutUser())} />
            </Navbar>
            <MainNavBar
              dispatch={dispatch}
              tracking={tracking}
              customerPropertyList={customerPropertyList}
            />
          </div>}
      </div>
    )
  }
}

App.propTypes = {
  dispatch: PropTypes.func.isRequired

  /*,
  tracking: PropTypes.object.isRequired,
  isAuthenticated: PropTypes.bool,
  errorMessage: PropTypes.string,
  profile: PropTypes.object.isRequired*/
}

function mapStateToProps(state) {
  const { profile, tracking, auth, customerPropertyList } = state

  return {
    profile,
    tracking,
    auth,
    customerPropertyList
  }
}

export default connect(mapStateToProps)(App)
