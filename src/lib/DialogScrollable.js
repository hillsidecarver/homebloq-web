import React, { Component } from "react"
import { PropTypes } from "prop-types"
import Dialog from "material-ui/Dialog"
import FlatButton from "material-ui/FlatButton"

class DialogScrollable extends Component {
  state = {
    open: false
  }

  handleOpen = isopen => {
    this.setState({ open: isopen })
  }

  handleClose = () => {
    this.setState({ open: false })
  }

  render() {
    const actions = [
      <FlatButton
        label="Close"
        primary={true}
        onTouchTap={this.props.onHandleClose}
      />
    ]

    return (
      <div>
        <Dialog
          title={this.props.title}
          actions={actions}
          modal={this.props.modal}
          open={this.props.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={this.props.autoScrollBodyContent}
          contentStyle={this.props.className}
        >
          {this.props.children}
        </Dialog>
      </div>
    )
  }
}

DialogScrollable.propTypes = {
  title: PropTypes.string.isRequired,
  modal: PropTypes.bool.isRequired,
  open: PropTypes.bool.isRequired,
  autoScrollBodyContent: PropTypes.bool.isRequired,
  onHandleClose: PropTypes.func.isRequired,
  className: PropTypes.object.isRequired
}

export default DialogScrollable
