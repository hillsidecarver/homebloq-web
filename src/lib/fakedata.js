export const tracking = {
  "Id": "5a152bd6ea34470cf5839301",
  "professionalId": "5a152bd6fbfa2dc5f83c95d1",
  "customers": [
    {
      "Id": "5a152bd64eb3ccc444f90ac4",
      "lastname": "Murray",
      "firstname": "Slater",
      "status": "Actively Looking",
      "state": "CONFIRM",
      "email": "slatermurray@zillactic.com",
      "monthlyGross": 2511.48,
      "debt": 28090.78,
      "expenses": 1000,
      "targetedZipCodes": [
        60644,
        60682,
        60649,
        60617,
        60649,
        60602
      ],
      "totalCash": 7554.29,
      "downPayment": 6402.29,
      "mortgageType": "30Yr",
      "totalCashToClose": 5978.97,
      "emergencyFund": 1096.51,
      "creditScoreRange": "<640",
      "trackingHistory": [
        {
          "Id": "5a152bd6853157d85e6b4f95",
          "status": "SENT",
          "professionalId": "5a152bd68eeaac72edaa820b",
          "createDate": "2016-12-05T07:25:01"
        },
        {
          "Id": "5a152bd634d070b137801d4b",
          "status": "DENY",
          "professionalId": "5a152bd64e19bc4997c6271a",
          "createDate": "2014-12-17T03:44:19"
        },
        {
          "Id": "5a152bd6ac03ba670d9ec5a1",
          "status": "CONFIRM",
          "professionalId": "5a152bd68669119ae97631af",
          "createDate": "2015-05-30T12:31:37"
        },
        {
          "Id": "5a152bd6b9c936b218503419",
          "status": "TRACK",
          "professionalId": "5a152bd6f75e8b5793b2abb3",
          "createDate": "2014-11-22T07:13:39"
        }
      ],
      "need": {
        "priceMin": 467643.52,
        "priceMax": 454735.22,
        "BA": 1,
        "BR": 2,
        "needs": [
          "Close to L",
          "Balcony",
          "Deck",
          "Deck"
        ]
      },
      "wants": [
        "Wired for Sound"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd614c216d1dced4317",
            "5a152bd6f4d191afef39d8b5"
          ],
          "price": 761256.12,
          "address": "644 Baycliff Terrace",
          "city": "Berlin",
          "zip": 60649,
          "BR": 1,
          "BA": 2,
          "monthlyInsurance": 243.79,
          "monthlyTaxes": 222.02,
          "monthlyPMI": 503.8,
          "monthlyHOA": 829.87,
          "closingCosts": 1420.88,
          "createDate": "2016-08-01T11:54:27",
          "updateDate": "2015-12-10T12:19:02",
          "from": "MLS",
          "createdBy": "slatermurray@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6a195efd8f4e74e6d",
            "5a152bd62e0bbe2bb75939b7",
            "5a152bd6b6c8a4fe92317b5d",
            "5a152bd6bf70db7133b16259",
            "5a152bd6834085ffdd45c37c",
            "5a152bd6585bfcfd96ebc8d4",
            "5a152bd670ff8e3b37dfaffb",
            "5a152bd6dcd045f060d8c739"
          ],
          "price": 387103.65,
          "address": "456 Robert Street",
          "city": "Mammoth",
          "zip": 60668,
          "BR": 3,
          "BA": 2,
          "monthlyInsurance": 629.59,
          "monthlyTaxes": 219.49,
          "monthlyPMI": 472.6,
          "monthlyHOA": 560.03,
          "closingCosts": 1868.44,
          "createDate": "2017-10-31T01:59:02",
          "updateDate": "2014-09-26T08:44:48",
          "from": "Redfin",
          "createdBy": "slatermurray@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6cb448dfec85a0bc5"
          ],
          "price": 901556.18,
          "address": "620 Liberty Avenue",
          "city": "Bangor",
          "zip": 60631,
          "BR": 4,
          "BA": 4,
          "monthlyInsurance": 380.7,
          "monthlyTaxes": 254.65,
          "monthlyPMI": 407.13,
          "monthlyHOA": 397.76,
          "closingCosts": 1449.33,
          "createDate": "2014-04-01T02:47:33",
          "updateDate": "2016-05-22T03:14:41",
          "from": "Redfin",
          "createdBy": "slatermurray@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd61dda67081341ac1c",
            "5a152bd6e11dd76623be72be",
            "5a152bd658d5e4f496f5ba9c",
            "5a152bd63a98703e35dcd21d",
            "5a152bd6a20475c6835f4d8a"
          ],
          "price": 459528.4,
          "address": "250 Lloyd Street",
          "city": "Bakersville",
          "zip": 60659,
          "BR": 4,
          "BA": 3,
          "monthlyInsurance": 551.89,
          "monthlyTaxes": 316.51,
          "monthlyPMI": 451.03,
          "monthlyHOA": 530.92,
          "closingCosts": 1229.78,
          "createDate": "2015-02-12T11:33:50",
          "updateDate": "2015-04-29T02:29:31",
          "from": "Redfin",
          "createdBy": "slatermurray@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd66a98b45e58102f82",
            "5a152bd660edf1dbf70eedea",
            "5a152bd6e604308fa91b85f2",
            "5a152bd6bd6a5dd59f30ad43",
            "5a152bd6a7771e4db66690ae"
          ],
          "price": 854964.03,
          "address": "329 Calyer Street",
          "city": "Goldfield",
          "zip": 60630,
          "BR": 2,
          "BA": 4,
          "monthlyInsurance": 222.03,
          "monthlyTaxes": 685.37,
          "monthlyPMI": 570.71,
          "monthlyHOA": 598.64,
          "closingCosts": 1539.67,
          "createDate": "2016-02-12T12:06:47",
          "updateDate": "2014-10-17T12:40:12",
          "from": "Zillow",
          "createdBy": "slatermurray@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd626a47dfdf80d574d",
            "5a152bd667f184281fd4f478",
            "5a152bd649e7ebca2b4f5007",
            "5a152bd674695c9476adc9cd",
            "5a152bd6b09e1191dfb6143d"
          ],
          "price": 619508.08,
          "address": "791 Landis Court",
          "city": "Edgar",
          "zip": 60640,
          "BR": 2,
          "BA": 3,
          "monthlyInsurance": 394.13,
          "monthlyTaxes": 268.12,
          "monthlyPMI": 701.18,
          "monthlyHOA": 592.61,
          "closingCosts": 1693.04,
          "createDate": "2014-07-04T07:26:20",
          "updateDate": "2016-04-08T02:44:33",
          "from": "HomeBloq",
          "createdBy": "slatermurray@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6c691a7438d100671",
            "5a152bd697aacc1607a8a417",
            "5a152bd6c504efa5b14a059f"
          ],
          "price": 953754.14,
          "address": "171 Fillmore Place",
          "city": "Rew",
          "zip": 60614,
          "BR": 3,
          "BA": 2,
          "monthlyInsurance": 316.2,
          "monthlyTaxes": 736.13,
          "monthlyPMI": 513.11,
          "monthlyHOA": 727.36,
          "closingCosts": 1953.79,
          "createDate": "2014-10-09T12:33:45",
          "updateDate": "2015-10-20T08:15:14",
          "from": "MLS",
          "createdBy": "slatermurray@zillactic.com"
        }
      ],
      "createDate": "2014-11-12T04:04:57",
      "updateDate": "2015-05-12T11:02:15",
      "createdBy": "slatermurray@zillactic.com"
    },
    {
      "Id": "5a152bd63fda5c9f9295ee22",
      "lastname": "Frye",
      "firstname": "Kelly",
      "status": "Just Starting Out",
      "state": "DENY",
      "email": "kellyfrye@zillactic.com",
      "monthlyGross": 3888.31,
      "debt": 33682.02,
      "expenses": 1000,
      "targetedZipCodes": [
        60643,
        60619,
        60680,
        60689,
        60607,
        60602
      ],
      "totalCash": 5218.57,
      "downPayment": 4562.22,
      "mortgageType": "15yr",
      "totalCashToClose": 7478.89,
      "emergencyFund": 1083.42,
      "creditScoreRange": "680-99",
      "trackingHistory": [
        {
          "Id": "5a152bd6776ff31c6aad0478",
          "status": "SENT",
          "professionalId": "5a152bd6a7814a4bde64f166",
          "createDate": "2014-06-20T12:27:37"
        },
        {
          "Id": "5a152bd67c5c838a77c5cbe3",
          "status": "DENY",
          "professionalId": "5a152bd67d75a60715d896fb",
          "createDate": "2015-10-20T10:43:06"
        },
        {
          "Id": "5a152bd6a407fc9715426ee4",
          "status": "CONFIRM",
          "professionalId": "5a152bd615c922a86d834996",
          "createDate": "2016-03-14T12:26:19"
        },
        {
          "Id": "5a152bd61f7621376973bd00",
          "status": "TRACK",
          "professionalId": "5a152bd6a96766a7186986a3",
          "createDate": "2017-11-10T05:33:02"
        }
      ],
      "need": {
        "priceMin": 320153.56,
        "priceMax": 924882.53,
        "BA": 1,
        "BR": 2,
        "needs": [
          "Granite Counter Tops",
          "Deck",
          "Close to L",
          "Close to L",
          "Close to L",
          "Granite Counter Tops",
          "Garage"
        ]
      },
      "wants": [
        "Pool"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd6786ca016dc01d94d",
            "5a152bd60e4ebcb39f453e67",
            "5a152bd6410e3247f6b8403e"
          ],
          "price": 967616.12,
          "address": "157 Colonial Road",
          "city": "Kylertown",
          "zip": 60606,
          "BR": 4,
          "BA": 2,
          "monthlyInsurance": 250.11,
          "monthlyTaxes": 448.57,
          "monthlyPMI": 872.51,
          "monthlyHOA": 770.38,
          "closingCosts": 1513.59,
          "createDate": "2017-10-03T05:49:02",
          "updateDate": "2016-09-02T04:42:24",
          "from": "Zillow",
          "createdBy": "kellyfrye@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6f2e9d344f0d01fc3",
            "5a152bd630dbc7cae7bc4677",
            "5a152bd61d7fac67994ae5e7",
            "5a152bd672024ddda012ea32",
            "5a152bd6991af5655d160f2c"
          ],
          "price": 642818.43,
          "address": "161 Kaufman Place",
          "city": "Gorham",
          "zip": 60615,
          "BR": 3,
          "BA": 1,
          "monthlyInsurance": 949.31,
          "monthlyTaxes": 840.87,
          "monthlyPMI": 295.36,
          "monthlyHOA": 986.48,
          "closingCosts": 1216.85,
          "createDate": "2017-09-03T09:58:50",
          "updateDate": "2014-12-30T06:12:23",
          "from": "Zillow",
          "createdBy": "kellyfrye@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6b048956bdf881c56",
            "5a152bd6c1547560f65c6ae3",
            "5a152bd616933df8e700b93c",
            "5a152bd63786bed76fec9aea",
            "5a152bd60e513a5fdf443e61",
            "5a152bd6925e822159b3bce7",
            "5a152bd677d1a2d673b6b252",
            "5a152bd6262ab99d43002327"
          ],
          "price": 984825.34,
          "address": "317 Neptune Court",
          "city": "Corriganville",
          "zip": 60608,
          "BR": 3,
          "BA": 4,
          "monthlyInsurance": 748.88,
          "monthlyTaxes": 791.58,
          "monthlyPMI": 481.75,
          "monthlyHOA": 910.71,
          "closingCosts": 1002.91,
          "createDate": "2017-10-18T11:53:13",
          "updateDate": "2016-01-23T02:00:58",
          "from": "Redfin",
          "createdBy": "kellyfrye@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd606b453eec8945a63",
            "5a152bd61d6d35e4d8935cf4",
            "5a152bd6871c2f3e3eba27c6",
            "5a152bd6e48475b070612ad0",
            "5a152bd6b0e67725dc5a55fe",
            "5a152bd6a8a357b6910c4b75",
            "5a152bd6dfe148e3d35bb0d6",
            "5a152bd659f3aecad4157ba5"
          ],
          "price": 458072.68,
          "address": "256 Woodbine Street",
          "city": "Emison",
          "zip": 60617,
          "BR": 3,
          "BA": 1,
          "monthlyInsurance": 417.16,
          "monthlyTaxes": 403.73,
          "monthlyPMI": 782.96,
          "monthlyHOA": 923.7,
          "closingCosts": 1893.5,
          "createDate": "2017-06-07T08:17:58",
          "updateDate": "2016-05-27T05:43:08",
          "from": "HomeBloq",
          "createdBy": "kellyfrye@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6fdf25037cc556ae1",
            "5a152bd6dd3a648355888b44",
            "5a152bd6bad12e6a8bafacb5"
          ],
          "price": 798842.83,
          "address": "702 Elton Street",
          "city": "Grazierville",
          "zip": 60651,
          "BR": 2,
          "BA": 2,
          "monthlyInsurance": 686.45,
          "monthlyTaxes": 438.49,
          "monthlyPMI": 969.21,
          "monthlyHOA": 810.95,
          "closingCosts": 1412.44,
          "createDate": "2015-07-28T10:11:40",
          "updateDate": "2014-07-15T02:48:39",
          "from": "HomeBloq",
          "createdBy": "kellyfrye@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6f763bfdc9ca5cb1b",
            "5a152bd6e69d1763d9f53b57",
            "5a152bd614d64d3eb587a730",
            "5a152bd656a47b642287dbb6",
            "5a152bd6da16575395fe3150",
            "5a152bd6b0b0d941ad2cac87",
            "5a152bd66b9b7d94db22f241"
          ],
          "price": 80383.07,
          "address": "538 Colin Place",
          "city": "Ebro",
          "zip": 60676,
          "BR": 3,
          "BA": 2,
          "monthlyInsurance": 535.43,
          "monthlyTaxes": 770.96,
          "monthlyPMI": 290.59,
          "monthlyHOA": 257.66,
          "closingCosts": 1923.57,
          "createDate": "2016-10-09T11:17:45",
          "updateDate": "2015-06-16T07:06:54",
          "from": "MLS",
          "createdBy": "kellyfrye@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd631f376c7cfd78919",
            "5a152bd692f703a1b3e6993e"
          ],
          "price": 302811.01,
          "address": "132 Beard Street",
          "city": "Norwood",
          "zip": 60657,
          "BR": 2,
          "BA": 2,
          "monthlyInsurance": 491.75,
          "monthlyTaxes": 401.98,
          "monthlyPMI": 465.26,
          "monthlyHOA": 576.68,
          "closingCosts": 1529.57,
          "createDate": "2017-04-04T10:02:16",
          "updateDate": "2015-09-26T04:34:12",
          "from": "Redfin",
          "createdBy": "kellyfrye@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6928973ad7999cfd8",
            "5a152bd6a6a2afb492e5cc51",
            "5a152bd6bb9f7ee552a225f4",
            "5a152bd6266f4d1a784e0196"
          ],
          "price": 834771.87,
          "address": "769 Albany Avenue",
          "city": "Trexlertown",
          "zip": 60654,
          "BR": 4,
          "BA": 3,
          "monthlyInsurance": 980.94,
          "monthlyTaxes": 665.64,
          "monthlyPMI": 489.21,
          "monthlyHOA": 978.02,
          "closingCosts": 1071.3,
          "createDate": "2017-05-06T09:18:56",
          "updateDate": "2016-10-10T12:05:34",
          "from": "Redfin",
          "createdBy": "kellyfrye@zillactic.com"
        }
      ],
      "createDate": "2014-07-18T03:18:15",
      "updateDate": "2014-06-20T10:59:27",
      "createdBy": "kellyfrye@zillactic.com"
    },
    {
      "Id": "5a152bd6ff4823c90e70c4e8",
      "lastname": "Barker",
      "firstname": "Hooper",
      "status": "Actively Looking",
      "state": "TRACK",
      "email": "hooperbarker@zillactic.com",
      "monthlyGross": 3464.26,
      "debt": 29668.25,
      "expenses": 1000,
      "targetedZipCodes": [
        60654,
        60684,
        60686,
        60641,
        60605,
        60606,
        60681
      ],
      "totalCash": 5864.75,
      "downPayment": 6520.02,
      "mortgageType": "30Yr",
      "totalCashToClose": 5831.63,
      "emergencyFund": 2316.06,
      "creditScoreRange": "<640",
      "trackingHistory": [
        {
          "Id": "5a152bd65d8559b9a0a59c7c",
          "status": "SENT",
          "professionalId": "5a152bd6b6038a518e146ceb",
          "createDate": "2015-06-03T07:44:55"
        },
        {
          "Id": "5a152bd63ec8b26856817b10",
          "status": "DENY",
          "professionalId": "5a152bd689371dc1f22c850d",
          "createDate": "2014-03-01T10:30:20"
        },
        {
          "Id": "5a152bd6d65f686a837bac18",
          "status": "CONFIRM",
          "professionalId": "5a152bd6c276810f01205d52",
          "createDate": "2014-09-01T06:13:38"
        },
        {
          "Id": "5a152bd6c14897841e6ca431",
          "status": "TRACK",
          "professionalId": "5a152bd6fdd90f999b506cf3",
          "createDate": "2014-08-15T05:22:12"
        }
      ],
      "need": {
        "priceMin": 147259.33,
        "priceMax": 292509.84,
        "BA": 2,
        "BR": 1,
        "needs": [
          "Garage",
          "Granite Counter Tops",
          "Close to L",
          "Deck",
          "Close to L",
          "Balcony",
          "Deck"
        ]
      },
      "wants": [
        "Pool",
        "Pool",
        "Rooftop Balcony"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd66465abc00249316e",
            "5a152bd68a0d20ccd21c6b6a",
            "5a152bd63599f165661479e4"
          ],
          "price": 263216.64,
          "address": "877 Wythe Avenue",
          "city": "Ada",
          "zip": 60675,
          "BR": 3,
          "BA": 1,
          "monthlyInsurance": 935.77,
          "monthlyTaxes": 532.4,
          "monthlyPMI": 321.8,
          "monthlyHOA": 844.1,
          "closingCosts": 1170.99,
          "createDate": "2017-08-11T11:33:52",
          "updateDate": "2014-01-20T07:45:35",
          "from": "Zillow",
          "createdBy": "hooperbarker@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd627eb3b1756c4fa17",
            "5a152bd692ccc2175566252d",
            "5a152bd63de437bbaa37b383",
            "5a152bd6acc9de151c3a7ede"
          ],
          "price": 986175.5,
          "address": "957 Waldorf Court",
          "city": "Chemung",
          "zip": 60661,
          "BR": 3,
          "BA": 4,
          "monthlyInsurance": 546.57,
          "monthlyTaxes": 745.82,
          "monthlyPMI": 447.02,
          "monthlyHOA": 302.55,
          "closingCosts": 1380.68,
          "createDate": "2017-07-15T03:02:57",
          "updateDate": "2016-02-24T06:22:51",
          "from": "Redfin",
          "createdBy": "hooperbarker@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6b9b6a878575d4e03",
            "5a152bd60b21db5914f9f6c4",
            "5a152bd6a1e9da950251987a"
          ],
          "price": 631996.45,
          "address": "349 Preston Court",
          "city": "Hasty",
          "zip": 60684,
          "BR": 2,
          "BA": 1,
          "monthlyInsurance": 681.74,
          "monthlyTaxes": 761.63,
          "monthlyPMI": 978.02,
          "monthlyHOA": 397.91,
          "closingCosts": 1755.16,
          "createDate": "2015-07-29T01:20:46",
          "updateDate": "2017-03-09T11:30:46",
          "from": "HomeBloq",
          "createdBy": "hooperbarker@zillactic.com"
        }
      ],
      "createDate": "2015-08-25T01:22:00",
      "updateDate": "2015-09-03T02:39:56",
      "createdBy": "hooperbarker@zillactic.com"
    },
    {
      "Id": "5a152bd662bf60142db9b0af",
      "lastname": "Holder",
      "firstname": "Langley",
      "status": "Actively Looking",
      "state": "TRACK",
      "email": "langleyholder@zillactic.com",
      "monthlyGross": 3661.13,
      "debt": 36469.53,
      "expenses": 1000,
      "targetedZipCodes": [
        60688,
        60629,
        60613
      ],
      "totalCash": 7090.11,
      "downPayment": 4080.23,
      "mortgageType": "5Yr",
      "totalCashToClose": 8754.44,
      "emergencyFund": 2373.42,
      "creditScoreRange": "<640",
      "trackingHistory": [
        {
          "Id": "5a152bd6cdd8043531ca68b7",
          "status": "SENT",
          "professionalId": "5a152bd601afb27ef1ad11da",
          "createDate": "2017-05-05T12:33:10"
        },
        {
          "Id": "5a152bd6f91cc346c516cc93",
          "status": "DENY",
          "professionalId": "5a152bd676c7ede8292257c2",
          "createDate": "2017-09-10T07:52:00"
        },
        {
          "Id": "5a152bd6325ea23fdd50f720",
          "status": "CONFIRM",
          "professionalId": "5a152bd66e5d0a389940e2ca",
          "createDate": "2016-11-14T01:32:17"
        },
        {
          "Id": "5a152bd6f16ac35b69ae043e",
          "status": "TRACK",
          "professionalId": "5a152bd6521238f0159a6c34",
          "createDate": "2015-02-13T11:20:50"
        }
      ],
      "need": {
        "priceMin": 211009.43,
        "priceMax": 335279.78,
        "BA": 3,
        "BR": 3,
        "needs": [
          "Close to L",
          "Close to L"
        ]
      },
      "wants": [
        "Rooftop Balcony"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd60a40423e263beb57",
            "5a152bd671e48763918462b9",
            "5a152bd640e5978c80a7c968",
            "5a152bd6ed8cf9d53dcef72c"
          ],
          "price": 861727.8,
          "address": "365 Vista Place",
          "city": "Frierson",
          "zip": 60621,
          "BR": 4,
          "BA": 2,
          "monthlyInsurance": 491.82,
          "monthlyTaxes": 772.6,
          "monthlyPMI": 290.18,
          "monthlyHOA": 239.7,
          "closingCosts": 1626.94,
          "createDate": "2016-07-25T03:40:05",
          "updateDate": "2017-01-03T01:27:38",
          "from": "HomeBloq",
          "createdBy": "langleyholder@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6dd9b84233521fe7c",
            "5a152bd661b34fb1feb3110b",
            "5a152bd6b3bb63a31ceac2fc",
            "5a152bd60d5034701b23d869",
            "5a152bd6ee762ec0c310921c",
            "5a152bd61fffbde0b2397c84"
          ],
          "price": 821165.71,
          "address": "262 Lancaster Avenue",
          "city": "Gorst",
          "zip": 60662,
          "BR": 2,
          "BA": 2,
          "monthlyInsurance": 407.73,
          "monthlyTaxes": 333.56,
          "monthlyPMI": 236.98,
          "monthlyHOA": 807.71,
          "closingCosts": 1528.06,
          "createDate": "2016-12-16T06:08:11",
          "updateDate": "2015-07-08T10:08:12",
          "from": "HomeBloq",
          "createdBy": "langleyholder@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd66efc18ff37e7effe",
            "5a152bd6423b951a8c1d93a1",
            "5a152bd63a13147e8fdce3bd",
            "5a152bd69860f35d60480db7",
            "5a152bd685712ccd0b15a181",
            "5a152bd64eab0a89868f5ad1"
          ],
          "price": 787092.29,
          "address": "170 Stockton Street",
          "city": "Nord",
          "zip": 60632,
          "BR": 1,
          "BA": 2,
          "monthlyInsurance": 372.36,
          "monthlyTaxes": 222.76,
          "monthlyPMI": 342.98,
          "monthlyHOA": 419.06,
          "closingCosts": 1420.48,
          "createDate": "2014-06-02T04:18:10",
          "updateDate": "2017-06-29T01:18:20",
          "from": "MLS",
          "createdBy": "langleyholder@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd68d55be436100678c",
            "5a152bd6615b01bebf2ea532"
          ],
          "price": 407643.28,
          "address": "431 Autumn Avenue",
          "city": "Crayne",
          "zip": 60644,
          "BR": 4,
          "BA": 4,
          "monthlyInsurance": 409.17,
          "monthlyTaxes": 634.56,
          "monthlyPMI": 671.08,
          "monthlyHOA": 547.67,
          "closingCosts": 1231.54,
          "createDate": "2016-07-09T09:44:30",
          "updateDate": "2017-07-04T09:12:45",
          "from": "MLS",
          "createdBy": "langleyholder@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6cb9e0dc5f95119c8",
            "5a152bd65af3d8316e703f92"
          ],
          "price": 760600.56,
          "address": "443 Frost Street",
          "city": "Echo",
          "zip": 60617,
          "BR": 4,
          "BA": 4,
          "monthlyInsurance": 313.51,
          "monthlyTaxes": 669.06,
          "monthlyPMI": 318.93,
          "monthlyHOA": 468.83,
          "closingCosts": 1250.47,
          "createDate": "2017-04-15T05:49:22",
          "updateDate": "2017-08-13T03:30:00",
          "from": "Redfin",
          "createdBy": "langleyholder@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6b76a4a7889a546e4",
            "5a152bd617d1c87e1fd459e7"
          ],
          "price": 668378.81,
          "address": "182 Schenck Avenue",
          "city": "Charco",
          "zip": 60618,
          "BR": 2,
          "BA": 2,
          "monthlyInsurance": 544.86,
          "monthlyTaxes": 387.22,
          "monthlyPMI": 397.14,
          "monthlyHOA": 644.92,
          "closingCosts": 1764.53,
          "createDate": "2017-03-07T12:34:34",
          "updateDate": "2015-11-24T08:36:23",
          "from": "MLS",
          "createdBy": "langleyholder@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6be2e55c6210704e3",
            "5a152bd6e281ad9963ba1524",
            "5a152bd6c4d4470f91a93531",
            "5a152bd611a0499d2f1c93a0",
            "5a152bd67c9b3a976bedadc8"
          ],
          "price": 625732.35,
          "address": "145 Bedell Lane",
          "city": "Mappsville",
          "zip": 60677,
          "BR": 1,
          "BA": 3,
          "monthlyInsurance": 534.11,
          "monthlyTaxes": 290.31,
          "monthlyPMI": 302.34,
          "monthlyHOA": 584.85,
          "closingCosts": 1129.04,
          "createDate": "2017-03-08T01:33:49",
          "updateDate": "2014-06-16T01:55:50",
          "from": "Zillow",
          "createdBy": "langleyholder@zillactic.com"
        }
      ],
      "createDate": "2017-04-10T03:19:52",
      "updateDate": "2015-08-05T02:28:48",
      "createdBy": "langleyholder@zillactic.com"
    },
    {
      "Id": "5a152bd69a64a0c1ad1510aa",
      "lastname": "Harrington",
      "firstname": "Doyle",
      "status": "Looking without Agent",
      "state": "CONFIRM",
      "email": "doyleharrington@zillactic.com",
      "monthlyGross": 3102.9,
      "debt": 14459.15,
      "expenses": 1000,
      "targetedZipCodes": [
        60650,
        60626
      ],
      "totalCash": 7594.24,
      "downPayment": 4979.16,
      "mortgageType": "30Yr",
      "totalCashToClose": 7065.1,
      "emergencyFund": 3726.77,
      "creditScoreRange": "<640",
      "trackingHistory": [
        {
          "Id": "5a152bd6564e7d7c8bd63ab1",
          "status": "SENT",
          "professionalId": "5a152bd617c51320417ba902",
          "createDate": "2016-07-07T06:41:41"
        },
        {
          "Id": "5a152bd6d557123843c4bfb1",
          "status": "DENY",
          "professionalId": "5a152bd6c042ffa653a8c880",
          "createDate": "2015-04-09T05:03:27"
        },
        {
          "Id": "5a152bd6bf6f50132c2a842e",
          "status": "CONFIRM",
          "professionalId": "5a152bd65e492f66d38b1daa",
          "createDate": "2015-12-15T11:20:31"
        },
        {
          "Id": "5a152bd6b8633e7e27ef4580",
          "status": "TRACK",
          "professionalId": "5a152bd68ba110922d580588",
          "createDate": "2015-05-22T04:04:56"
        }
      ],
      "need": {
        "priceMin": 696492.1,
        "priceMax": 945361.69,
        "BA": 1,
        "BR": 2,
        "needs": [
          "Granite Counter Tops"
        ]
      },
      "wants": [
        "Pool"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd62219a350c47ebe63"
          ],
          "price": 355692.24,
          "address": "799 Taylor Street",
          "city": "Detroit",
          "zip": 60629,
          "BR": 3,
          "BA": 4,
          "monthlyInsurance": 253.83,
          "monthlyTaxes": 996.16,
          "monthlyPMI": 338.67,
          "monthlyHOA": 860.22,
          "closingCosts": 1460.99,
          "createDate": "2017-08-27T07:24:00",
          "updateDate": "2014-08-10T02:55:45",
          "from": "MLS",
          "createdBy": "doyleharrington@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd61198fa019251000a",
            "5a152bd67d24a41e04c181ad",
            "5a152bd6f556fb61fe38bd28",
            "5a152bd6bbcc2322e9ddc644",
            "5a152bd6845edee0dfc25313",
            "5a152bd6a085a17cf383fff7",
            "5a152bd65fd12912fbddd721",
            "5a152bd645c87f97e23afb00"
          ],
          "price": 964168.2,
          "address": "200 Richmond Street",
          "city": "Wilmington",
          "zip": 60664,
          "BR": 2,
          "BA": 3,
          "monthlyInsurance": 279.88,
          "monthlyTaxes": 886.29,
          "monthlyPMI": 726.43,
          "monthlyHOA": 332.75,
          "closingCosts": 1386.61,
          "createDate": "2014-05-13T05:46:52",
          "updateDate": "2015-10-28T10:21:49",
          "from": "Zillow",
          "createdBy": "doyleharrington@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd60e796a985a3cbd03"
          ],
          "price": 202237.18,
          "address": "310 Vernon Avenue",
          "city": "Barstow",
          "zip": 60615,
          "BR": 3,
          "BA": 2,
          "monthlyInsurance": 269.01,
          "monthlyTaxes": 588.16,
          "monthlyPMI": 323.78,
          "monthlyHOA": 580.45,
          "closingCosts": 1764.34,
          "createDate": "2016-02-29T09:59:38",
          "updateDate": "2017-08-17T11:38:21",
          "from": "HomeBloq",
          "createdBy": "doyleharrington@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6312b682ff2f80dfa",
            "5a152bd63305861bd3e97f68",
            "5a152bd6e988931cb81d23f3",
            "5a152bd6b7095b56293e3307",
            "5a152bd6a033a72842f79f87",
            "5a152bd64d2d085eb61ee77f"
          ],
          "price": 958136.19,
          "address": "387 Elm Place",
          "city": "Englevale",
          "zip": 60675,
          "BR": 3,
          "BA": 1,
          "monthlyInsurance": 595.19,
          "monthlyTaxes": 658.32,
          "monthlyPMI": 533.03,
          "monthlyHOA": 879.68,
          "closingCosts": 1995.27,
          "createDate": "2014-09-07T06:23:05",
          "updateDate": "2017-07-13T10:43:12",
          "from": "Redfin",
          "createdBy": "doyleharrington@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd60796dd420534be04",
            "5a152bd66dd95ccfada91a86",
            "5a152bd64a4f77a4b2297bf8",
            "5a152bd6594d50829fb355f0"
          ],
          "price": 166898.64,
          "address": "487 Harwood Place",
          "city": "Tyro",
          "zip": 60689,
          "BR": 3,
          "BA": 2,
          "monthlyInsurance": 926.78,
          "monthlyTaxes": 215.31,
          "monthlyPMI": 823.47,
          "monthlyHOA": 226.82,
          "closingCosts": 1438.45,
          "createDate": "2017-06-17T10:01:51",
          "updateDate": "2017-07-13T12:22:34",
          "from": "HomeBloq",
          "createdBy": "doyleharrington@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd614a211657d26deb9"
          ],
          "price": 756449.77,
          "address": "525 Abbey Court",
          "city": "Shaft",
          "zip": 60662,
          "BR": 2,
          "BA": 3,
          "monthlyInsurance": 628.46,
          "monthlyTaxes": 653,
          "monthlyPMI": 324.73,
          "monthlyHOA": 761.17,
          "closingCosts": 1167.9,
          "createDate": "2016-02-21T09:19:02",
          "updateDate": "2015-01-05T11:11:56",
          "from": "MLS",
          "createdBy": "doyleharrington@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6429a547aac107fc3",
            "5a152bd6a1fdb1ee9cd4f36f",
            "5a152bd605ba11225524d1e8",
            "5a152bd6b4cd1c53f90cca6e",
            "5a152bd6ba3cf6fbdc5db23d"
          ],
          "price": 140563.1,
          "address": "802 Doscher Street",
          "city": "Blanco",
          "zip": 60610,
          "BR": 3,
          "BA": 4,
          "monthlyInsurance": 572.4,
          "monthlyTaxes": 887.14,
          "monthlyPMI": 899,
          "monthlyHOA": 322.81,
          "closingCosts": 1496.21,
          "createDate": "2015-01-18T08:52:12",
          "updateDate": "2016-04-22T10:10:32",
          "from": "HomeBloq",
          "createdBy": "doyleharrington@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6de626b294b9ed568",
            "5a152bd6451721c60b83e5e2",
            "5a152bd6dfea6d687969d978",
            "5a152bd602d3d0f7f6a00b50"
          ],
          "price": 405383.36,
          "address": "734 Coleman Street",
          "city": "Canterwood",
          "zip": 60634,
          "BR": 1,
          "BA": 1,
          "monthlyInsurance": 881.51,
          "monthlyTaxes": 856.68,
          "monthlyPMI": 696.42,
          "monthlyHOA": 786.01,
          "closingCosts": 1303.04,
          "createDate": "2016-07-18T04:47:42",
          "updateDate": "2015-10-24T05:03:49",
          "from": "HomeBloq",
          "createdBy": "doyleharrington@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd662e4a983a5884b5f"
          ],
          "price": 712547.62,
          "address": "584 Franklin Avenue",
          "city": "Brownlee",
          "zip": 60617,
          "BR": 3,
          "BA": 4,
          "monthlyInsurance": 341.08,
          "monthlyTaxes": 819.32,
          "monthlyPMI": 717.4,
          "monthlyHOA": 787.96,
          "closingCosts": 1065.9,
          "createDate": "2016-01-17T04:25:54",
          "updateDate": "2016-08-22T06:37:49",
          "from": "MLS",
          "createdBy": "doyleharrington@zillactic.com"
        }
      ],
      "createDate": "2016-08-20T04:32:11",
      "updateDate": "2017-08-10T10:05:50",
      "createdBy": "doyleharrington@zillactic.com"
    },
    {
      "Id": "5a152bd645812ca3f3c45411",
      "lastname": "Mcclure",
      "firstname": "Lawrence",
      "status": "Actively Looking",
      "state": "TRACK",
      "email": "lawrencemcclure@zillactic.com",
      "monthlyGross": 3110.99,
      "debt": 70481.43,
      "expenses": 1000,
      "targetedZipCodes": [
        60648,
        60640,
        60621,
        60601,
        60658,
        60655,
        60609
      ],
      "totalCash": 8378.66,
      "downPayment": 5407.29,
      "mortgageType": "15yr",
      "totalCashToClose": 4058.65,
      "emergencyFund": 3519.13,
      "creditScoreRange": "720-739",
      "trackingHistory": [
        {
          "Id": "5a152bd60173e1302c05d688",
          "status": "SENT",
          "professionalId": "5a152bd698d807472c76327d",
          "createDate": "2014-11-17T09:54:36"
        },
        {
          "Id": "5a152bd6b58a8ffbf3f27f29",
          "status": "DENY",
          "professionalId": "5a152bd6d3ad14f38cfdad21",
          "createDate": "2017-04-20T12:03:50"
        },
        {
          "Id": "5a152bd628c7eb06a8cfeb3b",
          "status": "CONFIRM",
          "professionalId": "5a152bd6ea0c2e547416357d",
          "createDate": "2016-04-08T07:39:25"
        },
        {
          "Id": "5a152bd656596141035d867b",
          "status": "TRACK",
          "professionalId": "5a152bd66235cbce3b564a35",
          "createDate": "2016-06-28T11:23:18"
        }
      ],
      "need": {
        "priceMin": 653345.99,
        "priceMax": 719146.89,
        "BA": 3,
        "BR": 3,
        "needs": [
          "Balcony",
          "Garage"
        ]
      },
      "wants": [
        "Rooftop Balcony",
        "Wired for Sound"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd654cf6a57f96943da",
            "5a152bd67638251dc28ad76f",
            "5a152bd6878b9255da67e5a3",
            "5a152bd68b0a21281754397a",
            "5a152bd6f55ada2f15fc4aa2"
          ],
          "price": 599019.33,
          "address": "261 Stockholm Street",
          "city": "Lorraine",
          "zip": 60677,
          "BR": 3,
          "BA": 4,
          "monthlyInsurance": 791.23,
          "monthlyTaxes": 331.38,
          "monthlyPMI": 971.87,
          "monthlyHOA": 647.38,
          "closingCosts": 1797.27,
          "createDate": "2017-08-07T03:22:03",
          "updateDate": "2016-07-01T08:47:12",
          "from": "Zillow",
          "createdBy": "lawrencemcclure@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6e655f2b330a25cdf",
            "5a152bd6bf25eba8b159d69b",
            "5a152bd69a8459d9df69e613",
            "5a152bd6ffd75037b2c77719",
            "5a152bd6b4edab5cd7d56ff5",
            "5a152bd630c6879518d719c5",
            "5a152bd61c53027671efcc33"
          ],
          "price": 370609.65,
          "address": "510 Willow Place",
          "city": "Shasta",
          "zip": 60605,
          "BR": 4,
          "BA": 3,
          "monthlyInsurance": 910.28,
          "monthlyTaxes": 436.48,
          "monthlyPMI": 592.98,
          "monthlyHOA": 610.38,
          "closingCosts": 1172.05,
          "createDate": "2014-12-02T02:54:34",
          "updateDate": "2017-09-18T09:05:38",
          "from": "MLS",
          "createdBy": "lawrencemcclure@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd63ae1fb66040a591b",
            "5a152bd6c8585d8d05e0fcf8",
            "5a152bd6538cd49150ffa155",
            "5a152bd60a5d8adbeff34899",
            "5a152bd648a1a221c9fe572b"
          ],
          "price": 275720.87,
          "address": "573 Grattan Street",
          "city": "Lodoga",
          "zip": 60650,
          "BR": 1,
          "BA": 2,
          "monthlyInsurance": 212.2,
          "monthlyTaxes": 541.28,
          "monthlyPMI": 456.32,
          "monthlyHOA": 857.05,
          "closingCosts": 1186.18,
          "createDate": "2016-03-09T06:13:42",
          "updateDate": "2014-11-26T07:08:19",
          "from": "MLS",
          "createdBy": "lawrencemcclure@zillactic.com"
        }
      ],
      "createDate": "2017-04-11T09:33:06",
      "updateDate": "2016-05-28T09:25:14",
      "createdBy": "lawrencemcclure@zillactic.com"
    },
    {
      "Id": "5a152bd64cceaa55706641a8",
      "lastname": "Mann",
      "firstname": "Carolina",
      "status": "Looking without Agent",
      "state": "DENY",
      "email": "carolinamann@zillactic.com",
      "monthlyGross": 3661.91,
      "debt": 15722.89,
      "expenses": 1000,
      "targetedZipCodes": [
        60686,
        60610
      ],
      "totalCash": 8728.37,
      "downPayment": 5544.82,
      "mortgageType": "15yr",
      "totalCashToClose": 9730.6,
      "emergencyFund": 2526.01,
      "creditScoreRange": "720-739",
      "trackingHistory": [
        {
          "Id": "5a152bd6afb5b6eec7b77495",
          "status": "SENT",
          "professionalId": "5a152bd617bebb37c5745c3b",
          "createDate": "2017-05-22T12:18:28"
        },
        {
          "Id": "5a152bd63f9c41771099e689",
          "status": "DENY",
          "professionalId": "5a152bd60b81a5d6da6928f7",
          "createDate": "2016-02-05T07:01:27"
        },
        {
          "Id": "5a152bd63b0e9dc80b01d50d",
          "status": "CONFIRM",
          "professionalId": "5a152bd651e532f955a97135",
          "createDate": "2017-04-08T11:52:40"
        },
        {
          "Id": "5a152bd6c9e5909da7f62012",
          "status": "TRACK",
          "professionalId": "5a152bd627fe72753bcd865b",
          "createDate": "2014-05-03T09:06:49"
        }
      ],
      "need": {
        "priceMin": 328289.68,
        "priceMax": 780298,
        "BA": 1,
        "BR": 2,
        "needs": [
          "Deck"
        ]
      },
      "wants": [
        "Rooftop Balcony",
        "Rooftop Balcony",
        "Pool"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd67d9720fd27707e6f",
            "5a152bd68ffa28aa7c1286b1",
            "5a152bd6cb01fdf57ffc1659"
          ],
          "price": 293634.45,
          "address": "684 Marconi Place",
          "city": "Beechmont",
          "zip": 60618,
          "BR": 2,
          "BA": 2,
          "monthlyInsurance": 513.96,
          "monthlyTaxes": 799.34,
          "monthlyPMI": 974.5,
          "monthlyHOA": 599.1,
          "closingCosts": 1472.72,
          "createDate": "2014-10-14T12:49:33",
          "updateDate": "2014-06-09T05:19:19",
          "from": "MLS",
          "createdBy": "carolinamann@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6847c5ab4e7612b31",
            "5a152bd6be3c5b9e7e80e604",
            "5a152bd61b71c987bd3a2376",
            "5a152bd6823e90a116732390",
            "5a152bd6267789c6e0b8bd97",
            "5a152bd62197088e754bf74d"
          ],
          "price": 291797.62,
          "address": "790 Folsom Place",
          "city": "Lydia",
          "zip": 60605,
          "BR": 3,
          "BA": 2,
          "monthlyInsurance": 627.83,
          "monthlyTaxes": 752.04,
          "monthlyPMI": 356.82,
          "monthlyHOA": 523.13,
          "closingCosts": 1460.17,
          "createDate": "2016-01-29T03:31:24",
          "updateDate": "2016-10-15T01:00:27",
          "from": "HomeBloq",
          "createdBy": "carolinamann@zillactic.com"
        }
      ],
      "createDate": "2015-09-21T10:53:40",
      "updateDate": "2016-09-12T05:05:20",
      "createdBy": "carolinamann@zillactic.com"
    },
    {
      "Id": "5a152bd6eeb78ca48fbd10fc",
      "lastname": "Sparks",
      "firstname": "Madeline",
      "status": "Just Starting Out",
      "state": "SENT",
      "email": "madelinesparks@zillactic.com",
      "monthlyGross": 3145.28,
      "debt": 53152.8,
      "expenses": 1000,
      "targetedZipCodes": [
        60637,
        60681,
        60630,
        60675,
        60689,
        60646,
        60634
      ],
      "totalCash": 7450.14,
      "downPayment": 9245.33,
      "mortgageType": "30Yr",
      "totalCashToClose": 6419.17,
      "emergencyFund": 2872.38,
      "creditScoreRange": "680-99",
      "trackingHistory": [
        {
          "Id": "5a152bd6fbb24c2945676846",
          "status": "SENT",
          "professionalId": "5a152bd6eb2e1b1dbccfd2fd",
          "createDate": "2016-05-03T04:02:24"
        },
        {
          "Id": "5a152bd6e3eb2579f693a4b0",
          "status": "DENY",
          "professionalId": "5a152bd6d094f1fe80750167",
          "createDate": "2015-09-22T08:31:50"
        },
        {
          "Id": "5a152bd66128b896e05b9883",
          "status": "CONFIRM",
          "professionalId": "5a152bd6c4fadb174560837b",
          "createDate": "2017-03-26T05:06:20"
        },
        {
          "Id": "5a152bd6d3e2fa8a4a663a36",
          "status": "TRACK",
          "professionalId": "5a152bd6e6f3b3f3a6257164",
          "createDate": "2015-06-12T02:28:24"
        }
      ],
      "need": {
        "priceMin": 581445.02,
        "priceMax": 74123.25,
        "BA": 2,
        "BR": 4,
        "needs": [
          "Close to L",
          "Balcony",
          "Garage",
          "Close to L",
          "Garage"
        ]
      },
      "wants": [
        "Pool",
        "Wired for Sound"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd6c615161a26d22790",
            "5a152bd627bdca4b967ced79",
            "5a152bd6d970b5e6a67027c0",
            "5a152bd6bd3b0492e0f0769b",
            "5a152bd621d06fe98d2fee9e",
            "5a152bd6e274ec8b4039572e",
            "5a152bd6d101fc55ac3f4097",
            "5a152bd6c64c6153eeff2e2f"
          ],
          "price": 749163.72,
          "address": "580 Seacoast Terrace",
          "city": "Sunnyside",
          "zip": 60658,
          "BR": 4,
          "BA": 4,
          "monthlyInsurance": 550.57,
          "monthlyTaxes": 544.91,
          "monthlyPMI": 794.94,
          "monthlyHOA": 469.93,
          "closingCosts": 1336.95,
          "createDate": "2016-02-02T10:46:41",
          "updateDate": "2017-08-20T07:53:33",
          "from": "MLS",
          "createdBy": "madelinesparks@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd66566ea5e4e7c59c3",
            "5a152bd6e4ca81f0254194d9",
            "5a152bd60266d3685dc02d5f",
            "5a152bd60da856fb32eaaa1c",
            "5a152bd63870d4c1cbcb3024",
            "5a152bd635d67ac81a55272f",
            "5a152bd6c86f1c33c8eb9b9d",
            "5a152bd6285ad7406408b35b"
          ],
          "price": 739648.39,
          "address": "596 Overbaugh Place",
          "city": "Felt",
          "zip": 60609,
          "BR": 4,
          "BA": 3,
          "monthlyInsurance": 676.76,
          "monthlyTaxes": 287.11,
          "monthlyPMI": 951.13,
          "monthlyHOA": 533.77,
          "closingCosts": 1982.69,
          "createDate": "2016-10-31T02:00:51",
          "updateDate": "2017-06-18T02:11:07",
          "from": "Zillow",
          "createdBy": "madelinesparks@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6cfd08d31880c86fa",
            "5a152bd6d7488ec6d225e4df",
            "5a152bd680cd78c6b8089bf1",
            "5a152bd6eb48f1da21d79047",
            "5a152bd66a8acc28e2a88db3",
            "5a152bd6b24c21b5a6f691c4",
            "5a152bd6575097d68425ea1f",
            "5a152bd6784fa643443d9180"
          ],
          "price": 693554.01,
          "address": "662 Hicks Street",
          "city": "Montura",
          "zip": 60614,
          "BR": 3,
          "BA": 3,
          "monthlyInsurance": 415.2,
          "monthlyTaxes": 745.85,
          "monthlyPMI": 617.46,
          "monthlyHOA": 338.67,
          "closingCosts": 1935.21,
          "createDate": "2017-10-17T02:40:55",
          "updateDate": "2015-04-19T02:06:54",
          "from": "MLS",
          "createdBy": "madelinesparks@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6296e374db30d0f75",
            "5a152bd64bfa7ccd6e406666",
            "5a152bd6372d7f131ffc9cc9",
            "5a152bd628e55eb01f9106f6",
            "5a152bd6e20fd5938e44d47f",
            "5a152bd6a9af710361d84d6c",
            "5a152bd631441ed1632dce25",
            "5a152bd6d8d6c61b330ffd48"
          ],
          "price": 186932.23,
          "address": "937 Thornton Street",
          "city": "Hickory",
          "zip": 60687,
          "BR": 4,
          "BA": 1,
          "monthlyInsurance": 774.89,
          "monthlyTaxes": 317.96,
          "monthlyPMI": 636.15,
          "monthlyHOA": 887.1,
          "closingCosts": 1447.06,
          "createDate": "2015-05-29T01:01:35",
          "updateDate": "2015-12-09T07:09:54",
          "from": "Zillow",
          "createdBy": "madelinesparks@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd60694165b78fc55e3",
            "5a152bd67bd03e799000b444"
          ],
          "price": 849743.85,
          "address": "227 College Place",
          "city": "Sehili",
          "zip": 60608,
          "BR": 1,
          "BA": 2,
          "monthlyInsurance": 236.89,
          "monthlyTaxes": 859.52,
          "monthlyPMI": 238.69,
          "monthlyHOA": 446.77,
          "closingCosts": 1674.94,
          "createDate": "2015-03-21T04:04:59",
          "updateDate": "2016-04-03T07:04:07",
          "from": "HomeBloq",
          "createdBy": "madelinesparks@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6331f5240cd9dac7f"
          ],
          "price": 643111.48,
          "address": "650 Lacon Court",
          "city": "Wescosville",
          "zip": 60648,
          "BR": 3,
          "BA": 3,
          "monthlyInsurance": 478.14,
          "monthlyTaxes": 605.32,
          "monthlyPMI": 690.29,
          "monthlyHOA": 691.6,
          "closingCosts": 1402.59,
          "createDate": "2017-10-23T06:37:35",
          "updateDate": "2016-04-26T08:02:05",
          "from": "MLS",
          "createdBy": "madelinesparks@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6343fe78d98a50ab3",
            "5a152bd662d90b4937f48f9e",
            "5a152bd632c7f4ce97a27a3b"
          ],
          "price": 69138.35,
          "address": "918 Ide Court",
          "city": "Ribera",
          "zip": 60631,
          "BR": 2,
          "BA": 2,
          "monthlyInsurance": 257.15,
          "monthlyTaxes": 442.79,
          "monthlyPMI": 840.93,
          "monthlyHOA": 945.79,
          "closingCosts": 1124.38,
          "createDate": "2017-07-22T01:16:00",
          "updateDate": "2014-11-28T08:13:55",
          "from": "HomeBloq",
          "createdBy": "madelinesparks@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd61ab3f50e7ccedcd4",
            "5a152bd64eb11f2dc3342d85",
            "5a152bd60f30c241859ca5e8",
            "5a152bd69464eba703a648e3",
            "5a152bd632b68a8873d338b3"
          ],
          "price": 400315.82,
          "address": "488 Congress Street",
          "city": "Bancroft",
          "zip": 60624,
          "BR": 1,
          "BA": 1,
          "monthlyInsurance": 225.59,
          "monthlyTaxes": 820.36,
          "monthlyPMI": 807.6,
          "monthlyHOA": 304.91,
          "closingCosts": 1507.66,
          "createDate": "2017-03-07T01:53:26",
          "updateDate": "2014-08-31T03:54:26",
          "from": "Zillow",
          "createdBy": "madelinesparks@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd62dbb205a81bc91b3",
            "5a152bd6c47663d86b99057d",
            "5a152bd6470a181f98b6b957",
            "5a152bd60b4dccf011414bb7",
            "5a152bd6ff5bf3e50e641d09",
            "5a152bd6e24c7b89c5253056",
            "5a152bd67bd868842c83f9ac"
          ],
          "price": 34716.79,
          "address": "965 Willow Street",
          "city": "Oberlin",
          "zip": 60669,
          "BR": 3,
          "BA": 2,
          "monthlyInsurance": 535.51,
          "monthlyTaxes": 415.71,
          "monthlyPMI": 720.14,
          "monthlyHOA": 377.1,
          "closingCosts": 1645.18,
          "createDate": "2014-02-20T03:01:43",
          "updateDate": "2016-01-14T08:55:02",
          "from": "HomeBloq",
          "createdBy": "madelinesparks@zillactic.com"
        }
      ],
      "createDate": "2017-11-08T12:58:42",
      "updateDate": "2014-08-29T06:25:45",
      "createdBy": "madelinesparks@zillactic.com"
    },
    {
      "Id": "5a152bd63e52c7d6aa580e8f",
      "lastname": "Fischer",
      "firstname": "Marci",
      "status": "Looking without Agent",
      "state": "DENY",
      "email": "marcifischer@zillactic.com",
      "monthlyGross": 2740.42,
      "debt": 87061.35,
      "expenses": 1000,
      "targetedZipCodes": [
        60662,
        60620,
        60630,
        60663,
        60617,
        60677
      ],
      "totalCash": 4219.76,
      "downPayment": 6642.2,
      "mortgageType": "30Yr",
      "totalCashToClose": 5215.27,
      "emergencyFund": 1472.9,
      "creditScoreRange": "<640",
      "trackingHistory": [
        {
          "Id": "5a152bd68af3572e8a32e713",
          "status": "SENT",
          "professionalId": "5a152bd693b15af4bdee534f",
          "createDate": "2017-09-20T01:21:24"
        },
        {
          "Id": "5a152bd6371d1de864a085d7",
          "status": "DENY",
          "professionalId": "5a152bd6857d5f47932ea362",
          "createDate": "2016-05-11T11:49:48"
        },
        {
          "Id": "5a152bd63d9369a67f6d4d16",
          "status": "CONFIRM",
          "professionalId": "5a152bd6b50f3d34cadd84f5",
          "createDate": "2015-08-05T08:23:48"
        },
        {
          "Id": "5a152bd645fef4576558bdf5",
          "status": "TRACK",
          "professionalId": "5a152bd628faaea2cb8ff711",
          "createDate": "2015-01-15T10:32:26"
        }
      ],
      "need": {
        "priceMin": 750928.77,
        "priceMax": 661855.46,
        "BA": 3,
        "BR": 1,
        "needs": [
          "Close to L",
          "Balcony"
        ]
      },
      "wants": [
        "Pool",
        "Rooftop Balcony"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd6ae15223be31c97d1",
            "5a152bd6465d03469a4a81c9",
            "5a152bd6756f4fbd443ae6d1"
          ],
          "price": 387585.5,
          "address": "294 Goodwin Place",
          "city": "Alafaya",
          "zip": 60609,
          "BR": 4,
          "BA": 4,
          "monthlyInsurance": 897.76,
          "monthlyTaxes": 767.14,
          "monthlyPMI": 266.2,
          "monthlyHOA": 454.65,
          "closingCosts": 1363.52,
          "createDate": "2014-07-22T11:39:43",
          "updateDate": "2017-06-20T02:32:50",
          "from": "HomeBloq",
          "createdBy": "marcifischer@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6ed33e92ae77f0e89",
            "5a152bd63e363c801321da9d"
          ],
          "price": 413689.5,
          "address": "330 Clifton Place",
          "city": "Ryderwood",
          "zip": 60662,
          "BR": 4,
          "BA": 3,
          "monthlyInsurance": 313.6,
          "monthlyTaxes": 786.58,
          "monthlyPMI": 525.04,
          "monthlyHOA": 761.72,
          "closingCosts": 1087.47,
          "createDate": "2015-05-25T01:38:23",
          "updateDate": "2017-09-08T08:46:02",
          "from": "Zillow",
          "createdBy": "marcifischer@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd61e9b98ccf58be3dc",
            "5a152bd680cac384559ff55f",
            "5a152bd6c858be02be627657",
            "5a152bd6ff463ffd33f35a51",
            "5a152bd614c315d73b853807",
            "5a152bd6340c1e6dce27bfde"
          ],
          "price": 563554.51,
          "address": "375 Kensington Walk",
          "city": "Fruitdale",
          "zip": 60665,
          "BR": 2,
          "BA": 2,
          "monthlyInsurance": 381.75,
          "monthlyTaxes": 713.56,
          "monthlyPMI": 791.17,
          "monthlyHOA": 474.96,
          "closingCosts": 1982.45,
          "createDate": "2014-08-05T01:05:01",
          "updateDate": "2017-11-16T03:50:04",
          "from": "Redfin",
          "createdBy": "marcifischer@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd675f860bf203e5af0",
            "5a152bd6c307d25c70e2b3ab",
            "5a152bd6535ceb203470d3fe",
            "5a152bd612c820bb287c7ac6",
            "5a152bd6dbe6a1c238a4708e",
            "5a152bd6e0323e0729c528cc",
            "5a152bd60f1e6fe81de16eb3",
            "5a152bd62e561d07ee838eaa"
          ],
          "price": 186352,
          "address": "798 Portal Street",
          "city": "Delwood",
          "zip": 60646,
          "BR": 4,
          "BA": 3,
          "monthlyInsurance": 552.18,
          "monthlyTaxes": 962.38,
          "monthlyPMI": 794.33,
          "monthlyHOA": 708,
          "closingCosts": 1685.96,
          "createDate": "2017-04-09T08:41:49",
          "updateDate": "2014-10-22T10:39:18",
          "from": "HomeBloq",
          "createdBy": "marcifischer@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6c1d7275d371033a6",
            "5a152bd666fb94500e65d68b",
            "5a152bd60c6e321e61fc7675",
            "5a152bd64ca774b9a21bdab6",
            "5a152bd641b3dc4e52201827",
            "5a152bd611f081e885cb1466",
            "5a152bd69e724585d489635b",
            "5a152bd6efb0498cdb3cdc23"
          ],
          "price": 923353.57,
          "address": "842 Wallabout Street",
          "city": "Bethany",
          "zip": 60624,
          "BR": 2,
          "BA": 1,
          "monthlyInsurance": 553.57,
          "monthlyTaxes": 308.11,
          "monthlyPMI": 690.56,
          "monthlyHOA": 963.09,
          "closingCosts": 1605.63,
          "createDate": "2017-11-17T03:35:30",
          "updateDate": "2015-05-04T07:43:53",
          "from": "Redfin",
          "createdBy": "marcifischer@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6909eb51695c21197",
            "5a152bd6c67281efec145e63",
            "5a152bd642981222bbf7c8e4"
          ],
          "price": 856877.43,
          "address": "575 Delmonico Place",
          "city": "Fontanelle",
          "zip": 60689,
          "BR": 3,
          "BA": 2,
          "monthlyInsurance": 255.15,
          "monthlyTaxes": 606.49,
          "monthlyPMI": 880.07,
          "monthlyHOA": 644.21,
          "closingCosts": 1606.53,
          "createDate": "2017-05-10T11:15:38",
          "updateDate": "2017-01-24T02:33:58",
          "from": "Redfin",
          "createdBy": "marcifischer@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6e363b5ae5b4137d0",
            "5a152bd684e35d47228ec083",
            "5a152bd66a5db1ca61cb362a"
          ],
          "price": 46770.58,
          "address": "624 Monroe Place",
          "city": "Lynn",
          "zip": 60637,
          "BR": 1,
          "BA": 3,
          "monthlyInsurance": 423.45,
          "monthlyTaxes": 469.6,
          "monthlyPMI": 749.14,
          "monthlyHOA": 484,
          "closingCosts": 1354.28,
          "createDate": "2015-06-04T02:18:36",
          "updateDate": "2016-06-14T08:19:41",
          "from": "Redfin",
          "createdBy": "marcifischer@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6ba972f45585e57d7",
            "5a152bd68dff42980d173e0f",
            "5a152bd64acd9cb16c5f564f",
            "5a152bd678479a3b98117e47",
            "5a152bd6ab750768dfe48041",
            "5a152bd6747d879e7e2a7caf"
          ],
          "price": 461007.18,
          "address": "692 Wyona Street",
          "city": "Barronett",
          "zip": 60624,
          "BR": 4,
          "BA": 4,
          "monthlyInsurance": 267.96,
          "monthlyTaxes": 562.76,
          "monthlyPMI": 214.06,
          "monthlyHOA": 390.11,
          "closingCosts": 1970.2,
          "createDate": "2017-10-16T03:59:49",
          "updateDate": "2016-05-14T12:10:44",
          "from": "Zillow",
          "createdBy": "marcifischer@zillactic.com"
        }
      ],
      "createDate": "2017-11-15T03:36:13",
      "updateDate": "2015-04-24T08:49:28",
      "createdBy": "marcifischer@zillactic.com"
    },
    {
      "Id": "5a152bd6f4bf8e2873f670f8",
      "lastname": "Francis",
      "firstname": "Kara",
      "status": "Just Starting Out",
      "state": "TRACK",
      "email": "karafrancis@zillactic.com",
      "monthlyGross": 3624.46,
      "debt": 94400.49,
      "expenses": 1000,
      "targetedZipCodes": [
        60608,
        60632,
        60645,
        60658,
        60637,
        60668
      ],
      "totalCash": 6207.19,
      "downPayment": 7496.78,
      "mortgageType": "5Yr",
      "totalCashToClose": 9127.96,
      "emergencyFund": 3264.5,
      "creditScoreRange": "<640",
      "trackingHistory": [
        {
          "Id": "5a152bd61bac0a7488f76294",
          "status": "SENT",
          "professionalId": "5a152bd60a3ce35a19f67060",
          "createDate": "2015-12-12T12:10:08"
        },
        {
          "Id": "5a152bd61f5b996e68548ecb",
          "status": "DENY",
          "professionalId": "5a152bd68dad617dc11fa6fe",
          "createDate": "2015-11-11T10:49:06"
        },
        {
          "Id": "5a152bd6511da0edee7cd777",
          "status": "CONFIRM",
          "professionalId": "5a152bd678d35048685f8222",
          "createDate": "2017-07-08T03:04:27"
        },
        {
          "Id": "5a152bd657fa3bc945c3c2cd",
          "status": "TRACK",
          "professionalId": "5a152bd6cd206c25aa2e2d8f",
          "createDate": "2017-04-05T01:32:25"
        }
      ],
      "need": {
        "priceMin": 291874.06,
        "priceMax": 747616.41,
        "BA": 1,
        "BR": 4,
        "needs": [
          "Balcony",
          "Balcony",
          "Garage",
          "Deck"
        ]
      },
      "wants": [
        "Pool",
        "Rooftop Balcony"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd6ba3850d960ef74e3",
            "5a152bd662d6399a858e2810",
            "5a152bd6c803347ea675c003",
            "5a152bd6bdb09b809b76f266"
          ],
          "price": 475768.14,
          "address": "211 Vanderbilt Street",
          "city": "Elfrida",
          "zip": 60622,
          "BR": 4,
          "BA": 3,
          "monthlyInsurance": 298.67,
          "monthlyTaxes": 639.05,
          "monthlyPMI": 784.32,
          "monthlyHOA": 626.82,
          "closingCosts": 1405.93,
          "createDate": "2017-11-07T01:00:40",
          "updateDate": "2015-10-19T01:10:20",
          "from": "HomeBloq",
          "createdBy": "karafrancis@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6424ceb83eb579656",
            "5a152bd605bd4551a86c5fb0",
            "5a152bd6058a32bcdbc5d0e3"
          ],
          "price": 962670.77,
          "address": "571 Crosby Avenue",
          "city": "Wheaton",
          "zip": 60678,
          "BR": 3,
          "BA": 1,
          "monthlyInsurance": 851.27,
          "monthlyTaxes": 972.12,
          "monthlyPMI": 477.22,
          "monthlyHOA": 276.33,
          "closingCosts": 1930.25,
          "createDate": "2014-11-08T06:06:56",
          "updateDate": "2015-10-07T08:44:11",
          "from": "HomeBloq",
          "createdBy": "karafrancis@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd64d48a789bd55dd6e"
          ],
          "price": 44799.73,
          "address": "888 Howard Place",
          "city": "Breinigsville",
          "zip": 60673,
          "BR": 3,
          "BA": 2,
          "monthlyInsurance": 747.59,
          "monthlyTaxes": 692,
          "monthlyPMI": 924.15,
          "monthlyHOA": 631.67,
          "closingCosts": 1494.53,
          "createDate": "2015-06-16T03:23:16",
          "updateDate": "2016-07-18T04:22:03",
          "from": "Redfin",
          "createdBy": "karafrancis@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd60cae86ec70f47882",
            "5a152bd6edc5cf9bd853b070",
            "5a152bd6863a03086071a4de",
            "5a152bd67dd77e64affb4c5f",
            "5a152bd65c249d3e8a3279ff",
            "5a152bd623fd19a6e7946847",
            "5a152bd646ec7e68232c87d1",
            "5a152bd630fbf0bf254ce66f"
          ],
          "price": 435659.93,
          "address": "853 Karweg Place",
          "city": "Blue",
          "zip": 60616,
          "BR": 3,
          "BA": 4,
          "monthlyInsurance": 493.14,
          "monthlyTaxes": 900.94,
          "monthlyPMI": 849.76,
          "monthlyHOA": 595.44,
          "closingCosts": 1282.69,
          "createDate": "2017-04-29T02:41:26",
          "updateDate": "2014-12-25T11:53:13",
          "from": "Redfin",
          "createdBy": "karafrancis@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd69b124b29cfbe9218",
            "5a152bd6ce63d86fe4ce9511",
            "5a152bd63ad87cfb7909c2a5",
            "5a152bd659934ccd6aacd726",
            "5a152bd61909983eaa118c9c",
            "5a152bd65a8035095914516e",
            "5a152bd6608560756acd6c0a",
            "5a152bd66cb7643d568d18d4"
          ],
          "price": 109486.12,
          "address": "416 Hopkins Street",
          "city": "Clarktown",
          "zip": 60650,
          "BR": 4,
          "BA": 2,
          "monthlyInsurance": 970.91,
          "monthlyTaxes": 994.51,
          "monthlyPMI": 640.82,
          "monthlyHOA": 445.41,
          "closingCosts": 1973.06,
          "createDate": "2014-12-03T08:59:05",
          "updateDate": "2014-02-20T07:34:57",
          "from": "MLS",
          "createdBy": "karafrancis@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd63fb41efbf9e4be36",
            "5a152bd65a4b6ef8db5df1a7",
            "5a152bd63e0c3e8f6f265791",
            "5a152bd680425d37f2a5ebc7",
            "5a152bd6f0bb1ab34590582c",
            "5a152bd68859c574e5d526f8",
            "5a152bd658030c4b4b6b2146"
          ],
          "price": 284573.8,
          "address": "567 Seagate Avenue",
          "city": "Downsville",
          "zip": 60665,
          "BR": 4,
          "BA": 2,
          "monthlyInsurance": 549.46,
          "monthlyTaxes": 870.88,
          "monthlyPMI": 535.54,
          "monthlyHOA": 722.73,
          "closingCosts": 1999.06,
          "createDate": "2017-01-27T03:46:06",
          "updateDate": "2014-11-20T01:24:01",
          "from": "Redfin",
          "createdBy": "karafrancis@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6420be2131135f059",
            "5a152bd6011c8ba09bf7144a",
            "5a152bd6b1990a4c6e618c22",
            "5a152bd63bb1687fe7d66caf",
            "5a152bd673461b49d9d2dd5f"
          ],
          "price": 897425.07,
          "address": "537 Dewey Place",
          "city": "Enlow",
          "zip": 60637,
          "BR": 2,
          "BA": 1,
          "monthlyInsurance": 558.42,
          "monthlyTaxes": 253.01,
          "monthlyPMI": 614.16,
          "monthlyHOA": 583.67,
          "closingCosts": 1972.43,
          "createDate": "2014-08-16T03:37:22",
          "updateDate": "2015-01-28T10:01:17",
          "from": "Zillow",
          "createdBy": "karafrancis@zillactic.com"
        }
      ],
      "createDate": "2015-07-01T12:55:08",
      "updateDate": "2017-03-12T08:17:06",
      "createdBy": "karafrancis@zillactic.com"
    },
    {
      "Id": "5a152bd62d6c65953594ec36",
      "lastname": "Oneill",
      "firstname": "Garrett",
      "status": "Actively Looking w/ Agent",
      "state": "CONFIRM",
      "email": "garrettoneill@zillactic.com",
      "monthlyGross": 2876.42,
      "debt": 25132.87,
      "expenses": 1000,
      "targetedZipCodes": [
        60666,
        60648,
        60650,
        60669,
        60609,
        60643
      ],
      "totalCash": 5015.47,
      "downPayment": 4995.26,
      "mortgageType": "15yr",
      "totalCashToClose": 4934.23,
      "emergencyFund": 3223,
      "creditScoreRange": "760+",
      "trackingHistory": [
        {
          "Id": "5a152bd6c26705b5832deb84",
          "status": "SENT",
          "professionalId": "5a152bd67f705e03f5676caf",
          "createDate": "2015-07-27T02:22:02"
        },
        {
          "Id": "5a152bd6f9d3cbea9be0e273",
          "status": "DENY",
          "professionalId": "5a152bd64148ed950c7e3a49",
          "createDate": "2014-06-01T10:29:56"
        },
        {
          "Id": "5a152bd635cd40cf9cdee4a0",
          "status": "CONFIRM",
          "professionalId": "5a152bd6a873480c9bd3c40f",
          "createDate": "2017-04-07T06:08:44"
        },
        {
          "Id": "5a152bd6909b82ea86c44cd9",
          "status": "TRACK",
          "professionalId": "5a152bd6f82c7d8d386dae7f",
          "createDate": "2014-11-17T07:59:17"
        }
      ],
      "need": {
        "priceMin": 883407.97,
        "priceMax": 840095.87,
        "BA": 1,
        "BR": 2,
        "needs": [
          "Deck",
          "Close to L",
          "Deck",
          "Deck"
        ]
      },
      "wants": [
        "Wired for Sound",
        "Rooftop Balcony",
        "Pool"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd612ce0d82aa4e53df",
            "5a152bd68eda0cc552e96f8f",
            "5a152bd6efd07ede4e13a254"
          ],
          "price": 649254.2,
          "address": "273 Freeman Street",
          "city": "Lawrence",
          "zip": 60673,
          "BR": 4,
          "BA": 1,
          "monthlyInsurance": 206.26,
          "monthlyTaxes": 550.18,
          "monthlyPMI": 317.94,
          "monthlyHOA": 283.91,
          "closingCosts": 1016.8,
          "createDate": "2014-04-09T07:32:54",
          "updateDate": "2014-06-08T02:02:29",
          "from": "HomeBloq",
          "createdBy": "garrettoneill@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6aa63ac65552e0c7a",
            "5a152bd6b3bb37d7bd9b3d4a",
            "5a152bd67d04197e735a5148",
            "5a152bd6f31ae39394f6b927",
            "5a152bd68a853449094e40e2",
            "5a152bd62a5f16c3e65c9c24"
          ],
          "price": 248300.65,
          "address": "950 Williamsburg Street",
          "city": "Wikieup",
          "zip": 60617,
          "BR": 3,
          "BA": 2,
          "monthlyInsurance": 572.74,
          "monthlyTaxes": 363.43,
          "monthlyPMI": 930.96,
          "monthlyHOA": 985.9,
          "closingCosts": 1769.38,
          "createDate": "2014-05-08T11:33:19",
          "updateDate": "2014-04-09T11:23:17",
          "from": "Zillow",
          "createdBy": "garrettoneill@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd68b7e1f5281999a2f",
            "5a152bd6ffddc5affe8f33a9",
            "5a152bd635fae9f27d9ab907",
            "5a152bd604e5935347e038c5",
            "5a152bd6f1fd8e20bf227f7e",
            "5a152bd6c6e2cef59eb63671"
          ],
          "price": 577718.33,
          "address": "264 Arlington Place",
          "city": "Toftrees",
          "zip": 60657,
          "BR": 4,
          "BA": 3,
          "monthlyInsurance": 282.54,
          "monthlyTaxes": 917.08,
          "monthlyPMI": 953.47,
          "monthlyHOA": 854.69,
          "closingCosts": 1734.02,
          "createDate": "2014-11-06T07:42:45",
          "updateDate": "2016-05-28T02:27:11",
          "from": "HomeBloq",
          "createdBy": "garrettoneill@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd696ba41e810114a3f",
            "5a152bd610d910a7db48f897",
            "5a152bd6530e6f688cd6daab",
            "5a152bd6df2780030c88f7aa",
            "5a152bd6f2610b1b9e0341b8",
            "5a152bd65b1deaf4de6ab559",
            "5a152bd661ed4baa6d9d2b2e",
            "5a152bd6c1c3214846659fdf"
          ],
          "price": 152319.97,
          "address": "727 Sunnyside Court",
          "city": "Duryea",
          "zip": 60652,
          "BR": 1,
          "BA": 4,
          "monthlyInsurance": 465.7,
          "monthlyTaxes": 944.95,
          "monthlyPMI": 927.74,
          "monthlyHOA": 325.45,
          "closingCosts": 1451.24,
          "createDate": "2017-05-07T02:15:39",
          "updateDate": "2017-10-13T02:57:14",
          "from": "Zillow",
          "createdBy": "garrettoneill@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd63935cbbb00459f82"
          ],
          "price": 929639.92,
          "address": "480 Woodrow Court",
          "city": "Mulberry",
          "zip": 60664,
          "BR": 1,
          "BA": 2,
          "monthlyInsurance": 763.42,
          "monthlyTaxes": 278.44,
          "monthlyPMI": 356.47,
          "monthlyHOA": 829.03,
          "closingCosts": 1593.84,
          "createDate": "2015-07-19T07:03:11",
          "updateDate": "2017-01-14T04:25:03",
          "from": "Zillow",
          "createdBy": "garrettoneill@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd669df17e47e8d9e3d",
            "5a152bd6c41cd4277a9d63df",
            "5a152bd64481dab2da5f6df1",
            "5a152bd65375a7072a2c5600",
            "5a152bd6bbca29f67e6b3d22",
            "5a152bd6ea3af5edd1d43045"
          ],
          "price": 454729.7,
          "address": "944 Bond Street",
          "city": "Convent",
          "zip": 60637,
          "BR": 1,
          "BA": 3,
          "monthlyInsurance": 713.71,
          "monthlyTaxes": 426.1,
          "monthlyPMI": 406.93,
          "monthlyHOA": 390.96,
          "closingCosts": 1739.61,
          "createDate": "2016-04-20T01:46:35",
          "updateDate": "2015-11-13T08:46:29",
          "from": "MLS",
          "createdBy": "garrettoneill@zillactic.com"
        }
      ],
      "createDate": "2014-05-20T11:42:58",
      "updateDate": "2017-06-13T05:56:14",
      "createdBy": "garrettoneill@zillactic.com"
    },
    {
      "Id": "5a152bd64f11635df358aadf",
      "lastname": "Goff",
      "firstname": "Christie",
      "status": "Just Starting Out",
      "state": "TRACK",
      "email": "christiegoff@zillactic.com",
      "monthlyGross": 2729.93,
      "debt": 30002.25,
      "expenses": 1000,
      "targetedZipCodes": [
        60666,
        60657,
        60602,
        60633,
        60658,
        60665,
        60643
      ],
      "totalCash": 7863.08,
      "downPayment": 4278.99,
      "mortgageType": "30Yr",
      "totalCashToClose": 5357.03,
      "emergencyFund": 3623,
      "creditScoreRange": "640-659",
      "trackingHistory": [
        {
          "Id": "5a152bd6fb4c4ee4fae77497",
          "status": "SENT",
          "professionalId": "5a152bd693d0938b34cd1a09",
          "createDate": "2015-11-25T04:16:02"
        },
        {
          "Id": "5a152bd6848427ef073d7115",
          "status": "DENY",
          "professionalId": "5a152bd6bcb30b99bc6037d5",
          "createDate": "2014-09-20T11:45:22"
        },
        {
          "Id": "5a152bd6234fecb5db30ad5e",
          "status": "CONFIRM",
          "professionalId": "5a152bd60154a1155d536b0c",
          "createDate": "2017-07-12T09:02:54"
        },
        {
          "Id": "5a152bd6c75510fea8f72eaa",
          "status": "TRACK",
          "professionalId": "5a152bd615c5b2a2b7fcfd3f",
          "createDate": "2017-08-15T03:04:08"
        }
      ],
      "need": {
        "priceMin": 352058.5,
        "priceMax": 997868.29,
        "BA": 4,
        "BR": 2,
        "needs": [
          "Granite Counter Tops",
          "Close to L",
          "Deck",
          "Garage"
        ]
      },
      "wants": [
        "Pool",
        "Rooftop Balcony",
        "Wired for Sound"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd63d1d24ca05adf0ee"
          ],
          "price": 238244.8,
          "address": "270 Balfour Place",
          "city": "Roy",
          "zip": 60679,
          "BR": 4,
          "BA": 1,
          "monthlyInsurance": 377.79,
          "monthlyTaxes": 652.3,
          "monthlyPMI": 940.85,
          "monthlyHOA": 607.43,
          "closingCosts": 1419.89,
          "createDate": "2014-11-18T12:54:23",
          "updateDate": "2017-01-04T07:27:39",
          "from": "Zillow",
          "createdBy": "christiegoff@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6417dd7d36e20eeb1",
            "5a152bd6f6b41311317281d1"
          ],
          "price": 780571.38,
          "address": "587 Kane Place",
          "city": "Irwin",
          "zip": 60664,
          "BR": 1,
          "BA": 2,
          "monthlyInsurance": 750.89,
          "monthlyTaxes": 534.39,
          "monthlyPMI": 860.32,
          "monthlyHOA": 427.16,
          "closingCosts": 1816.53,
          "createDate": "2015-02-16T01:15:23",
          "updateDate": "2015-12-03T08:20:36",
          "from": "HomeBloq",
          "createdBy": "christiegoff@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6772cf48f3e26eb26"
          ],
          "price": 874262.35,
          "address": "713 Fiske Place",
          "city": "Coinjock",
          "zip": 60601,
          "BR": 1,
          "BA": 2,
          "monthlyInsurance": 824.4,
          "monthlyTaxes": 846.03,
          "monthlyPMI": 501.35,
          "monthlyHOA": 865.8,
          "closingCosts": 1194.65,
          "createDate": "2014-08-02T10:06:50",
          "updateDate": "2017-03-15T05:14:19",
          "from": "Redfin",
          "createdBy": "christiegoff@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6c04c4f2fe53f3f78",
            "5a152bd6fac3e8366086fc50",
            "5a152bd6b67f4002712e4c0b",
            "5a152bd60daa289cb287261a"
          ],
          "price": 451239.15,
          "address": "776 Schaefer Street",
          "city": "Northchase",
          "zip": 60633,
          "BR": 3,
          "BA": 3,
          "monthlyInsurance": 975.02,
          "monthlyTaxes": 778.98,
          "monthlyPMI": 739.64,
          "monthlyHOA": 340.61,
          "closingCosts": 1161.5,
          "createDate": "2016-08-06T08:15:37",
          "updateDate": "2015-04-12T10:18:33",
          "from": "HomeBloq",
          "createdBy": "christiegoff@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6b62df9bcb3f3f4e1",
            "5a152bd630b3d46744491d40",
            "5a152bd6fec6e082560267f9",
            "5a152bd610b3c594188723ac"
          ],
          "price": 141888.01,
          "address": "163 Bath Avenue",
          "city": "Chesapeake",
          "zip": 60621,
          "BR": 2,
          "BA": 1,
          "monthlyInsurance": 982.55,
          "monthlyTaxes": 794.82,
          "monthlyPMI": 782.99,
          "monthlyHOA": 280.49,
          "closingCosts": 1078.55,
          "createDate": "2015-08-14T01:06:56",
          "updateDate": "2014-12-15T12:21:54",
          "from": "HomeBloq",
          "createdBy": "christiegoff@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd646ab928084b8dea6",
            "5a152bd6b1bd35b9eed4d3a1",
            "5a152bd663256946a71417c5",
            "5a152bd6d5f76717eea76c86"
          ],
          "price": 515211.67,
          "address": "709 Driggs Avenue",
          "city": "Limestone",
          "zip": 60624,
          "BR": 2,
          "BA": 4,
          "monthlyInsurance": 951.23,
          "monthlyTaxes": 380.07,
          "monthlyPMI": 316.06,
          "monthlyHOA": 672.46,
          "closingCosts": 1970.37,
          "createDate": "2014-01-02T07:51:08",
          "updateDate": "2017-08-08T01:46:15",
          "from": "MLS",
          "createdBy": "christiegoff@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6587f6586d649467a",
            "5a152bd656f6d3f2f2327db9",
            "5a152bd611558a7abe2e63d1",
            "5a152bd618e0688fd1eeb047",
            "5a152bd6a9b3328c20cce389",
            "5a152bd6d14f1009492c4927",
            "5a152bd6ba8376b1cb6b477a",
            "5a152bd60fcf3bebfbdf5923"
          ],
          "price": 143988.68,
          "address": "323 Herkimer Court",
          "city": "Bendon",
          "zip": 60675,
          "BR": 1,
          "BA": 1,
          "monthlyInsurance": 424.14,
          "monthlyTaxes": 581.51,
          "monthlyPMI": 722.64,
          "monthlyHOA": 222.02,
          "closingCosts": 1156.82,
          "createDate": "2016-06-03T07:35:07",
          "updateDate": "2015-07-24T05:30:47",
          "from": "MLS",
          "createdBy": "christiegoff@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6ec2daf10fed9f0d7",
            "5a152bd68cdd9028d470deb8",
            "5a152bd67ecf95ecae21d225"
          ],
          "price": 644935.33,
          "address": "864 Prospect Street",
          "city": "Sanborn",
          "zip": 60635,
          "BR": 4,
          "BA": 1,
          "monthlyInsurance": 746.27,
          "monthlyTaxes": 776.13,
          "monthlyPMI": 720.24,
          "monthlyHOA": 430.35,
          "closingCosts": 1097.05,
          "createDate": "2017-01-26T12:45:13",
          "updateDate": "2014-12-06T04:42:04",
          "from": "MLS",
          "createdBy": "christiegoff@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6bb7cf4244452179a",
            "5a152bd685d6bf9355b5956f",
            "5a152bd6c1a5dcb96b9bdf48",
            "5a152bd61f8556bb013c2cd6",
            "5a152bd68b4fbce4a4542219",
            "5a152bd62015ca5059c14c0e",
            "5a152bd6a3d37a2b65f3321e"
          ],
          "price": 976801.01,
          "address": "552 Dahl Court",
          "city": "Wanship",
          "zip": 60626,
          "BR": 1,
          "BA": 3,
          "monthlyInsurance": 215.42,
          "monthlyTaxes": 485.7,
          "monthlyPMI": 745.39,
          "monthlyHOA": 655.81,
          "closingCosts": 1371.41,
          "createDate": "2017-02-03T03:48:13",
          "updateDate": "2017-09-20T08:00:12",
          "from": "MLS",
          "createdBy": "christiegoff@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6f841d9dfccabaf6e",
            "5a152bd6d01789c6f876d846",
            "5a152bd69021a544e0309ba8",
            "5a152bd67495103bd9eb48b0",
            "5a152bd6384b2f111556fa43"
          ],
          "price": 508261.54,
          "address": "760 Euclid Avenue",
          "city": "Wilsonia",
          "zip": 60619,
          "BR": 4,
          "BA": 2,
          "monthlyInsurance": 522.31,
          "monthlyTaxes": 439.22,
          "monthlyPMI": 943.77,
          "monthlyHOA": 378.28,
          "closingCosts": 1397.81,
          "createDate": "2016-03-15T08:08:41",
          "updateDate": "2017-01-03T10:01:38",
          "from": "Zillow",
          "createdBy": "christiegoff@zillactic.com"
        }
      ],
      "createDate": "2015-04-11T11:09:44",
      "updateDate": "2015-02-01T01:46:58",
      "createdBy": "christiegoff@zillactic.com"
    },
    {
      "Id": "5a152bd67a6af62f1257016e",
      "lastname": "Anthony",
      "firstname": "Lily",
      "status": "Actively Looking w/ Agent",
      "state": "CONFIRM",
      "email": "lilyanthony@zillactic.com",
      "monthlyGross": 2637.55,
      "debt": 62396.68,
      "expenses": 1000,
      "targetedZipCodes": [
        60687,
        60610,
        60625
      ],
      "totalCash": 6592.07,
      "downPayment": 6060.45,
      "mortgageType": "15yr",
      "totalCashToClose": 9890.13,
      "emergencyFund": 3480.9,
      "creditScoreRange": "640-659",
      "trackingHistory": [
        {
          "Id": "5a152bd65c81d5b4643527fb",
          "status": "SENT",
          "professionalId": "5a152bd6c472c1cbebfb4b21",
          "createDate": "2016-08-27T07:05:08"
        },
        {
          "Id": "5a152bd67f864da0d5a325bc",
          "status": "DENY",
          "professionalId": "5a152bd6999936792d42a7c2",
          "createDate": "2016-09-02T06:39:09"
        },
        {
          "Id": "5a152bd62eee5b94364364c5",
          "status": "CONFIRM",
          "professionalId": "5a152bd658722bdf9328d5e5",
          "createDate": "2016-12-09T06:15:54"
        },
        {
          "Id": "5a152bd613f0112b866d82b0",
          "status": "TRACK",
          "professionalId": "5a152bd6ba61369702ef51b6",
          "createDate": "2016-01-13T07:35:16"
        }
      ],
      "need": {
        "priceMin": 449672.1,
        "priceMax": 983459.16,
        "BA": 2,
        "BR": 4,
        "needs": [
          "Granite Counter Tops",
          "Close to L",
          "Granite Counter Tops",
          "Close to L"
        ]
      },
      "wants": [
        "Wired for Sound",
        "Pool"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd672a1e80ad46d5442",
            "5a152bd6c3e848041f432156",
            "5a152bd68d2e6b618c81d711",
            "5a152bd6b18fadabe303185d",
            "5a152bd6cb2869f0a070b404"
          ],
          "price": 96448.98,
          "address": "313 Polar Street",
          "city": "Klagetoh",
          "zip": 60684,
          "BR": 1,
          "BA": 1,
          "monthlyInsurance": 381.12,
          "monthlyTaxes": 735.72,
          "monthlyPMI": 657.16,
          "monthlyHOA": 791.85,
          "closingCosts": 1283.33,
          "createDate": "2014-06-08T04:08:04",
          "updateDate": "2015-01-18T07:29:41",
          "from": "Zillow",
          "createdBy": "lilyanthony@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6817f42a9c63c28d9",
            "5a152bd62e9b35fb5214300e",
            "5a152bd6ce69ebe29c11e271",
            "5a152bd645fcfdb5aaeaf08b",
            "5a152bd69932caaa00269b8a",
            "5a152bd6925cce97d26d54c4",
            "5a152bd6b350d238c3a7d078"
          ],
          "price": 342722.27,
          "address": "466 Crooke Avenue",
          "city": "Welch",
          "zip": 60668,
          "BR": 3,
          "BA": 3,
          "monthlyInsurance": 774.65,
          "monthlyTaxes": 860.54,
          "monthlyPMI": 351.3,
          "monthlyHOA": 345.71,
          "closingCosts": 1172.66,
          "createDate": "2017-06-02T03:54:18",
          "updateDate": "2017-05-02T02:31:30",
          "from": "Redfin",
          "createdBy": "lilyanthony@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd68f89775b3ddd4db9",
            "5a152bd6e25ff25642bf551d"
          ],
          "price": 696439.3,
          "address": "558 Bleecker Street",
          "city": "Nescatunga",
          "zip": 60608,
          "BR": 3,
          "BA": 3,
          "monthlyInsurance": 276.58,
          "monthlyTaxes": 660.94,
          "monthlyPMI": 420.54,
          "monthlyHOA": 701.93,
          "closingCosts": 1382.79,
          "createDate": "2016-04-21T10:09:52",
          "updateDate": "2017-10-14T08:22:28",
          "from": "HomeBloq",
          "createdBy": "lilyanthony@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd62631b4fb1cb77631",
            "5a152bd6ab8d4fa41d47275e"
          ],
          "price": 981966.83,
          "address": "725 Howard Alley",
          "city": "Foscoe",
          "zip": 60677,
          "BR": 2,
          "BA": 1,
          "monthlyInsurance": 589.55,
          "monthlyTaxes": 362.01,
          "monthlyPMI": 244.22,
          "monthlyHOA": 473.5,
          "closingCosts": 1119.76,
          "createDate": "2014-01-13T09:11:58",
          "updateDate": "2014-05-11T05:13:03",
          "from": "Zillow",
          "createdBy": "lilyanthony@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd60defb1541b6e707f",
            "5a152bd63b20ab76ee5bc0aa",
            "5a152bd6ac6b6ebdf442e355"
          ],
          "price": 924430.95,
          "address": "653 Scott Avenue",
          "city": "Kohatk",
          "zip": 60651,
          "BR": 3,
          "BA": 2,
          "monthlyInsurance": 770.23,
          "monthlyTaxes": 245.94,
          "monthlyPMI": 926.86,
          "monthlyHOA": 595.31,
          "closingCosts": 1944.09,
          "createDate": "2015-06-22T06:38:04",
          "updateDate": "2014-09-08T01:43:57",
          "from": "Zillow",
          "createdBy": "lilyanthony@zillactic.com"
        }
      ],
      "createDate": "2016-05-30T01:08:22",
      "updateDate": "2014-04-05T07:20:10",
      "createdBy": "lilyanthony@zillactic.com"
    },
    {
      "Id": "5a152bd65260468d8fb3ddce",
      "lastname": "Cox",
      "firstname": "Hope",
      "status": "Actively Looking",
      "state": "TRACK",
      "email": "hopecox@zillactic.com",
      "monthlyGross": 2517.52,
      "debt": 82488.23,
      "expenses": 1000,
      "targetedZipCodes": [
        60667,
        60624,
        60683,
        60676,
        60644,
        60655
      ],
      "totalCash": 4624.41,
      "downPayment": 5793.79,
      "mortgageType": "15yr",
      "totalCashToClose": 4214.13,
      "emergencyFund": 3006.96,
      "creditScoreRange": "720-739",
      "trackingHistory": [
        {
          "Id": "5a152bd6c3c9b05fe0b90451",
          "status": "SENT",
          "professionalId": "5a152bd6ee993818e9e7d725",
          "createDate": "2015-01-19T04:46:07"
        },
        {
          "Id": "5a152bd6126a92000356b752",
          "status": "DENY",
          "professionalId": "5a152bd6288a78ccca6e673c",
          "createDate": "2014-01-08T07:10:16"
        },
        {
          "Id": "5a152bd60ebb7055ef7ef58c",
          "status": "CONFIRM",
          "professionalId": "5a152bd626feb265a9439706",
          "createDate": "2015-10-02T11:03:06"
        },
        {
          "Id": "5a152bd6c6dc6228d77805e2",
          "status": "TRACK",
          "professionalId": "5a152bd67725babf830e199e",
          "createDate": "2016-06-18T09:34:46"
        }
      ],
      "need": {
        "priceMin": 995252.85,
        "priceMax": 94187.45,
        "BA": 4,
        "BR": 1,
        "needs": [
          "Close to L",
          "Deck",
          "Close to L",
          "Close to L",
          "Garage",
          "Garage",
          "Balcony",
          "Balcony"
        ]
      },
      "wants": [
        "Wired for Sound",
        "Wired for Sound"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd69c04d5b9ed530608",
            "5a152bd61abc78b4f18c0216",
            "5a152bd68c9ac21f3c45ebe5",
            "5a152bd61f492cdd660eded0",
            "5a152bd6ab47bf1e08da3adc"
          ],
          "price": 281339.09,
          "address": "530 Locust Street",
          "city": "Marshall",
          "zip": 60673,
          "BR": 1,
          "BA": 1,
          "monthlyInsurance": 997.87,
          "monthlyTaxes": 378.83,
          "monthlyPMI": 338.58,
          "monthlyHOA": 600.38,
          "closingCosts": 1894.72,
          "createDate": "2014-02-01T04:15:25",
          "updateDate": "2014-03-21T03:31:11",
          "from": "Zillow",
          "createdBy": "hopecox@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd66767e81f408ca7ff",
            "5a152bd6a8098e919ce7b42a",
            "5a152bd6962840d7a50b3c08",
            "5a152bd6929e29eef8d467ad"
          ],
          "price": 750427.68,
          "address": "573 Jamaica Avenue",
          "city": "Gouglersville",
          "zip": 60663,
          "BR": 4,
          "BA": 4,
          "monthlyInsurance": 798.37,
          "monthlyTaxes": 626.91,
          "monthlyPMI": 345.22,
          "monthlyHOA": 875.62,
          "closingCosts": 1447.15,
          "createDate": "2015-08-11T12:51:04",
          "updateDate": "2015-08-28T04:20:02",
          "from": "HomeBloq",
          "createdBy": "hopecox@zillactic.com"
        }
      ],
      "createDate": "2016-04-18T04:34:26",
      "updateDate": "2014-07-22T07:24:12",
      "createdBy": "hopecox@zillactic.com"
    },
    {
      "Id": "5a152bd65fe8c38658541adb",
      "lastname": "Daugherty",
      "firstname": "Florine",
      "status": "Actively Looking",
      "state": "SENT",
      "email": "florinedaugherty@zillactic.com",
      "monthlyGross": 2425.96,
      "debt": 62156.34,
      "expenses": 1000,
      "targetedZipCodes": [
        60677,
        60664,
        60632
      ],
      "totalCash": 5246.06,
      "downPayment": 4478.05,
      "mortgageType": "15yr",
      "totalCashToClose": 8127.86,
      "emergencyFund": 1517.39,
      "creditScoreRange": "720-739",
      "trackingHistory": [
        {
          "Id": "5a152bd6d47e0ff0fec71d69",
          "status": "SENT",
          "professionalId": "5a152bd67d1aed55f2b9d9c1",
          "createDate": "2017-03-12T05:59:27"
        },
        {
          "Id": "5a152bd662812e68f0afc2bd",
          "status": "DENY",
          "professionalId": "5a152bd62e3ac7fd78e86046",
          "createDate": "2014-01-25T06:40:20"
        },
        {
          "Id": "5a152bd6baeec51bf85dc197",
          "status": "CONFIRM",
          "professionalId": "5a152bd6987ff1876d0883bf",
          "createDate": "2016-07-17T08:48:57"
        },
        {
          "Id": "5a152bd66724a7ed09d85a90",
          "status": "TRACK",
          "professionalId": "5a152bd6006797174239cb23",
          "createDate": "2016-06-25T08:17:13"
        }
      ],
      "need": {
        "priceMin": 264275.55,
        "priceMax": 714959.81,
        "BA": 3,
        "BR": 3,
        "needs": [
          "Garage",
          "Close to L"
        ]
      },
      "wants": [
        "Rooftop Balcony"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd6a17af067de285499",
            "5a152bd6f0c59625faeb7bb2",
            "5a152bd662322c3c3c7c5bfd",
            "5a152bd6463a61ac09fefae6"
          ],
          "price": 921804.29,
          "address": "492 Norman Avenue",
          "city": "Gibsonia",
          "zip": 60646,
          "BR": 2,
          "BA": 3,
          "monthlyInsurance": 632.92,
          "monthlyTaxes": 300.44,
          "monthlyPMI": 491.54,
          "monthlyHOA": 252.64,
          "closingCosts": 1208.04,
          "createDate": "2017-07-02T03:31:36",
          "updateDate": "2014-07-18T04:01:11",
          "from": "Redfin",
          "createdBy": "florinedaugherty@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd64061ef74be14beac",
            "5a152bd609908856c231df19",
            "5a152bd67cfb94d71275918d",
            "5a152bd619d362028309fa99",
            "5a152bd6b18befcf00517419"
          ],
          "price": 898483.85,
          "address": "362 Church Lane",
          "city": "Gila",
          "zip": 60652,
          "BR": 3,
          "BA": 3,
          "monthlyInsurance": 495.45,
          "monthlyTaxes": 326.35,
          "monthlyPMI": 860.69,
          "monthlyHOA": 889.13,
          "closingCosts": 1632.15,
          "createDate": "2016-06-20T11:30:20",
          "updateDate": "2015-05-24T06:13:50",
          "from": "HomeBloq",
          "createdBy": "florinedaugherty@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd675da83b32f1d3809"
          ],
          "price": 997832.25,
          "address": "700 Lewis Avenue",
          "city": "Otranto",
          "zip": 60617,
          "BR": 4,
          "BA": 2,
          "monthlyInsurance": 925.44,
          "monthlyTaxes": 246.87,
          "monthlyPMI": 798.3,
          "monthlyHOA": 479.13,
          "closingCosts": 1118.03,
          "createDate": "2016-04-09T01:11:10",
          "updateDate": "2014-06-30T01:43:02",
          "from": "HomeBloq",
          "createdBy": "florinedaugherty@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd641b3963a8c66d98c",
            "5a152bd61fd6c859767b23fa",
            "5a152bd66ca843be22929e3c",
            "5a152bd60e088c0da12171d6",
            "5a152bd624a58a9b610a792d",
            "5a152bd63a0e80b7ca148ac5"
          ],
          "price": 139602.48,
          "address": "984 Oxford Street",
          "city": "Succasunna",
          "zip": 60608,
          "BR": 3,
          "BA": 3,
          "monthlyInsurance": 309.86,
          "monthlyTaxes": 302.55,
          "monthlyPMI": 915.9,
          "monthlyHOA": 992.29,
          "closingCosts": 1580.3,
          "createDate": "2017-03-04T04:23:41",
          "updateDate": "2015-01-12T08:36:57",
          "from": "MLS",
          "createdBy": "florinedaugherty@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6464a375f00864184",
            "5a152bd63aa4defa5c9be800"
          ],
          "price": 169290.99,
          "address": "594 Himrod Street",
          "city": "Sultana",
          "zip": 60649,
          "BR": 1,
          "BA": 3,
          "monthlyInsurance": 279.46,
          "monthlyTaxes": 533.05,
          "monthlyPMI": 209.04,
          "monthlyHOA": 364.32,
          "closingCosts": 1971.09,
          "createDate": "2015-10-26T04:54:49",
          "updateDate": "2016-10-20T03:29:26",
          "from": "Zillow",
          "createdBy": "florinedaugherty@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd61aaa8074a9169f08",
            "5a152bd65fa926c7f8154c20"
          ],
          "price": 652400.24,
          "address": "938 Rogers Avenue",
          "city": "Broadlands",
          "zip": 60685,
          "BR": 2,
          "BA": 1,
          "monthlyInsurance": 692.41,
          "monthlyTaxes": 947.36,
          "monthlyPMI": 760.05,
          "monthlyHOA": 675.4,
          "closingCosts": 1743.59,
          "createDate": "2016-07-01T07:17:04",
          "updateDate": "2017-03-31T02:11:07",
          "from": "Zillow",
          "createdBy": "florinedaugherty@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6cb7234d81e3da309"
          ],
          "price": 836514.24,
          "address": "611 Gerry Street",
          "city": "Brecon",
          "zip": 60614,
          "BR": 1,
          "BA": 1,
          "monthlyInsurance": 597.23,
          "monthlyTaxes": 215.15,
          "monthlyPMI": 849.48,
          "monthlyHOA": 613.47,
          "closingCosts": 1146.05,
          "createDate": "2016-05-17T04:56:20",
          "updateDate": "2014-10-04T07:40:49",
          "from": "MLS",
          "createdBy": "florinedaugherty@zillactic.com"
        }
      ],
      "createDate": "2015-10-25T10:52:44",
      "updateDate": "2015-01-20T07:32:11",
      "createdBy": "florinedaugherty@zillactic.com"
    },
    {
      "Id": "5a152bd6e7bd5a49c8c1fe5a",
      "lastname": "Owen",
      "firstname": "Rose",
      "status": "Just Starting Out",
      "state": "CONFIRM",
      "email": "roseowen@zillactic.com",
      "monthlyGross": 1115.03,
      "debt": 94951.86,
      "expenses": 1000,
      "targetedZipCodes": [
        60647,
        60643,
        60684,
        60629,
        60604
      ],
      "totalCash": 6104.5,
      "downPayment": 4731.42,
      "mortgageType": "30Yr",
      "totalCashToClose": 4761.46,
      "emergencyFund": 1531.64,
      "creditScoreRange": "<640",
      "trackingHistory": [
        {
          "Id": "5a152bd6b56194f895c93358",
          "status": "SENT",
          "professionalId": "5a152bd6f922d531bfc0ee85",
          "createDate": "2017-05-27T01:11:32"
        },
        {
          "Id": "5a152bd6262df0d246fea4b3",
          "status": "DENY",
          "professionalId": "5a152bd6db554d98c3a3fcb4",
          "createDate": "2015-06-23T12:00:49"
        },
        {
          "Id": "5a152bd61b462f7f0c31f5cc",
          "status": "CONFIRM",
          "professionalId": "5a152bd67c880a3507960c49",
          "createDate": "2017-09-19T12:25:43"
        },
        {
          "Id": "5a152bd60d6737005800194c",
          "status": "TRACK",
          "professionalId": "5a152bd60f698d8efaf09e7c",
          "createDate": "2014-06-12T07:16:07"
        }
      ],
      "need": {
        "priceMin": 342844.64,
        "priceMax": 371479.47,
        "BA": 2,
        "BR": 4,
        "needs": [
          "Garage"
        ]
      },
      "wants": [
        "Wired for Sound"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd6b7e5cfdbd3b277ac",
            "5a152bd609597e3295b16a17",
            "5a152bd64c724807586c1a4d"
          ],
          "price": 746086.42,
          "address": "836 Ovington Avenue",
          "city": "Rockingham",
          "zip": 60614,
          "BR": 2,
          "BA": 4,
          "monthlyInsurance": 624.98,
          "monthlyTaxes": 665.9,
          "monthlyPMI": 210.93,
          "monthlyHOA": 295.66,
          "closingCosts": 1988.32,
          "createDate": "2014-10-28T06:13:42",
          "updateDate": "2017-09-20T09:19:50",
          "from": "Redfin",
          "createdBy": "roseowen@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6d514a97e6fbf0d61",
            "5a152bd60bd6d0ad238516cf",
            "5a152bd6ad31651cc071e684",
            "5a152bd6657674b253d97532",
            "5a152bd6a3b48b2355e4dcd3"
          ],
          "price": 371592.43,
          "address": "885 Ingraham Street",
          "city": "Odessa",
          "zip": 60669,
          "BR": 4,
          "BA": 1,
          "monthlyInsurance": 792.24,
          "monthlyTaxes": 588.22,
          "monthlyPMI": 883.75,
          "monthlyHOA": 720.16,
          "closingCosts": 1617.57,
          "createDate": "2016-02-24T04:59:23",
          "updateDate": "2017-09-27T07:41:55",
          "from": "HomeBloq",
          "createdBy": "roseowen@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd62ef41de7465cf102",
            "5a152bd68f99be32e1b11724",
            "5a152bd6d762cce95df0a84a",
            "5a152bd6da40546cb71f6b2f",
            "5a152bd68a146d7c051d3093",
            "5a152bd6b81adfc5d744c607"
          ],
          "price": 887329.16,
          "address": "899 Albemarle Road",
          "city": "Whitehaven",
          "zip": 60676,
          "BR": 4,
          "BA": 2,
          "monthlyInsurance": 831.27,
          "monthlyTaxes": 813.62,
          "monthlyPMI": 986.25,
          "monthlyHOA": 438.51,
          "closingCosts": 1340.62,
          "createDate": "2017-11-15T04:22:00",
          "updateDate": "2014-06-06T05:15:56",
          "from": "Zillow",
          "createdBy": "roseowen@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6cf2f7ee347a35ac5",
            "5a152bd6b79c907627f734cc",
            "5a152bd6b2d2e3f76b1b6800",
            "5a152bd60280d0979e79f1b6",
            "5a152bd63053e1cac9712b91",
            "5a152bd6ff61cff002d3ac0a",
            "5a152bd65a223b8c9119aa91"
          ],
          "price": 55060.72,
          "address": "640 Virginia Place",
          "city": "Bourg",
          "zip": 60681,
          "BR": 4,
          "BA": 1,
          "monthlyInsurance": 378.03,
          "monthlyTaxes": 463.42,
          "monthlyPMI": 862.46,
          "monthlyHOA": 792.16,
          "closingCosts": 1283.09,
          "createDate": "2015-01-09T12:03:52",
          "updateDate": "2016-01-06T09:53:59",
          "from": "Zillow",
          "createdBy": "roseowen@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd62aa59cdf9af15966",
            "5a152bd61e683a620150a222"
          ],
          "price": 123501.15,
          "address": "359 Madison Place",
          "city": "Turah",
          "zip": 60639,
          "BR": 3,
          "BA": 3,
          "monthlyInsurance": 798.82,
          "monthlyTaxes": 260.39,
          "monthlyPMI": 237.81,
          "monthlyHOA": 310.28,
          "closingCosts": 1566.98,
          "createDate": "2016-11-07T01:07:50",
          "updateDate": "2014-07-21T04:07:34",
          "from": "HomeBloq",
          "createdBy": "roseowen@zillactic.com"
        }
      ],
      "createDate": "2016-11-21T04:35:00",
      "updateDate": "2014-07-19T07:51:17",
      "createdBy": "roseowen@zillactic.com"
    },
    {
      "Id": "5a152bd670c4764797a68c3e",
      "lastname": "Santos",
      "firstname": "Staci",
      "status": "Looking without Agent",
      "state": "SENT",
      "email": "stacisantos@zillactic.com",
      "monthlyGross": 2420.94,
      "debt": 64855,
      "expenses": 1000,
      "targetedZipCodes": [
        60661,
        60604,
        60631,
        60619,
        60601,
        60626
      ],
      "totalCash": 7460.75,
      "downPayment": 8603.42,
      "mortgageType": "30Yr",
      "totalCashToClose": 5655.21,
      "emergencyFund": 2395.6,
      "creditScoreRange": "720-739",
      "trackingHistory": [
        {
          "Id": "5a152bd67e0460604a531116",
          "status": "SENT",
          "professionalId": "5a152bd662ed58d664376ada",
          "createDate": "2017-11-20T10:29:57"
        },
        {
          "Id": "5a152bd6063e5183e0729daf",
          "status": "DENY",
          "professionalId": "5a152bd656c149ce6681088a",
          "createDate": "2014-12-07T04:49:28"
        },
        {
          "Id": "5a152bd6c0ce1900e2e0bdc7",
          "status": "CONFIRM",
          "professionalId": "5a152bd617f3ac09f69b28c0",
          "createDate": "2017-10-22T04:44:37"
        },
        {
          "Id": "5a152bd6dc7ea3028f67097d",
          "status": "TRACK",
          "professionalId": "5a152bd6e631af7cc5389135",
          "createDate": "2015-05-26T12:15:31"
        }
      ],
      "need": {
        "priceMin": 720638.47,
        "priceMax": 555412.27,
        "BA": 4,
        "BR": 3,
        "needs": [
          "Garage",
          "Close to L",
          "Garage",
          "Balcony",
          "Garage",
          "Deck"
        ]
      },
      "wants": [
        "Wired for Sound",
        "Wired for Sound",
        "Wired for Sound"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd616456a9c812557e8",
            "5a152bd667776663c2690ae6",
            "5a152bd636952d1277b60337",
            "5a152bd61156008f5d121f49",
            "5a152bd6ba03376d12abc65c",
            "5a152bd6c42a58a3ac8e4f23",
            "5a152bd63c7c04beef4055ff",
            "5a152bd663ed3e42e4ae7730"
          ],
          "price": 699137.72,
          "address": "137 Dobbin Street",
          "city": "Weogufka",
          "zip": 60609,
          "BR": 4,
          "BA": 4,
          "monthlyInsurance": 913.61,
          "monthlyTaxes": 876.05,
          "monthlyPMI": 924.21,
          "monthlyHOA": 656.56,
          "closingCosts": 1473.43,
          "createDate": "2016-02-12T02:58:38",
          "updateDate": "2016-09-11T09:32:05",
          "from": "MLS",
          "createdBy": "stacisantos@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd650099889ddf22482",
            "5a152bd65513e9adfcf93db7",
            "5a152bd66613ee73cc999c5d",
            "5a152bd605d032caf73e709f",
            "5a152bd696fa04343e60d4be",
            "5a152bd6b93755d565d4bea7",
            "5a152bd6a7610e7a75c1b809",
            "5a152bd66df1d518dc976c07"
          ],
          "price": 846276.59,
          "address": "367 Catherine Street",
          "city": "Richford",
          "zip": 60689,
          "BR": 4,
          "BA": 2,
          "monthlyInsurance": 269.75,
          "monthlyTaxes": 900.12,
          "monthlyPMI": 548.59,
          "monthlyHOA": 578.86,
          "closingCosts": 1952.18,
          "createDate": "2015-03-05T03:25:14",
          "updateDate": "2017-03-02T01:20:13",
          "from": "Redfin",
          "createdBy": "stacisantos@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd600945396107e1db8"
          ],
          "price": 962457.37,
          "address": "858 Estate Road",
          "city": "Finderne",
          "zip": 60677,
          "BR": 4,
          "BA": 1,
          "monthlyInsurance": 974.37,
          "monthlyTaxes": 979.49,
          "monthlyPMI": 216.64,
          "monthlyHOA": 812.14,
          "closingCosts": 1395.05,
          "createDate": "2016-10-22T10:56:43",
          "updateDate": "2016-05-01T08:36:44",
          "from": "HomeBloq",
          "createdBy": "stacisantos@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd64ae6979ae578cb63",
            "5a152bd668d2afdb9cc89597",
            "5a152bd65fc99d5b62e12f51"
          ],
          "price": 224995.05,
          "address": "778 Ash Street",
          "city": "Vivian",
          "zip": 60687,
          "BR": 2,
          "BA": 2,
          "monthlyInsurance": 940.11,
          "monthlyTaxes": 435.44,
          "monthlyPMI": 652.68,
          "monthlyHOA": 208.87,
          "closingCosts": 1915.02,
          "createDate": "2016-09-22T08:31:00",
          "updateDate": "2016-03-16T03:08:33",
          "from": "HomeBloq",
          "createdBy": "stacisantos@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6fe5197e679a8d157",
            "5a152bd6fd17eba1cae742ba",
            "5a152bd6d8bbc9b5f36c0a92",
            "5a152bd6d6209789579c3e0a",
            "5a152bd6be0952e8ae5cee37"
          ],
          "price": 792802.62,
          "address": "700 Llama Court",
          "city": "Camas",
          "zip": 60640,
          "BR": 1,
          "BA": 2,
          "monthlyInsurance": 309.04,
          "monthlyTaxes": 612.56,
          "monthlyPMI": 844.81,
          "monthlyHOA": 943.19,
          "closingCosts": 1445.13,
          "createDate": "2016-08-24T09:34:52",
          "updateDate": "2016-04-21T05:21:16",
          "from": "Redfin",
          "createdBy": "stacisantos@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd641f4a9fe3ff3957d",
            "5a152bd645bf15e982663d04",
            "5a152bd6516951c55a6c9f4c",
            "5a152bd66c997f11158f8af1",
            "5a152bd6d36f3fb7bae4ec63"
          ],
          "price": 580463.22,
          "address": "634 Ivan Court",
          "city": "Newkirk",
          "zip": 60627,
          "BR": 4,
          "BA": 4,
          "monthlyInsurance": 833.81,
          "monthlyTaxes": 792.62,
          "monthlyPMI": 827.21,
          "monthlyHOA": 694.91,
          "closingCosts": 1674.5,
          "createDate": "2015-09-01T06:46:56",
          "updateDate": "2014-02-03T04:26:42",
          "from": "Zillow",
          "createdBy": "stacisantos@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6af57aa3d4a3f91cb",
            "5a152bd6baed35f50cee0949",
            "5a152bd6fe1cfe0f1de393ae",
            "5a152bd67b6a35d428bbc2ea",
            "5a152bd64aed1631c1258baf",
            "5a152bd6c5d88a8d6e8f6e58"
          ],
          "price": 299514.81,
          "address": "151 Falmouth Street",
          "city": "Greenfields",
          "zip": 60681,
          "BR": 4,
          "BA": 2,
          "monthlyInsurance": 502.84,
          "monthlyTaxes": 529.27,
          "monthlyPMI": 935.08,
          "monthlyHOA": 984.88,
          "closingCosts": 1803.14,
          "createDate": "2017-01-12T12:49:08",
          "updateDate": "2016-02-24T03:31:22",
          "from": "Redfin",
          "createdBy": "stacisantos@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6dea7d3fbe7341d91",
            "5a152bd6a4fd7d6950b47a28",
            "5a152bd6654a0b56b2134312",
            "5a152bd69a290da9a7db0e4c"
          ],
          "price": 939853,
          "address": "882 Hunterfly Place",
          "city": "Roland",
          "zip": 60618,
          "BR": 2,
          "BA": 1,
          "monthlyInsurance": 566.76,
          "monthlyTaxes": 598.37,
          "monthlyPMI": 697.5,
          "monthlyHOA": 682.32,
          "closingCosts": 1223.7,
          "createDate": "2017-10-22T01:31:46",
          "updateDate": "2017-07-23T07:42:15",
          "from": "HomeBloq",
          "createdBy": "stacisantos@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd61a897824b22f4705",
            "5a152bd6a617f4026dd0484f",
            "5a152bd6d4a482cf89158da6",
            "5a152bd6d079dcd27844fc2c",
            "5a152bd6a4310e8787c8dceb",
            "5a152bd6531845ea39fc5be1",
            "5a152bd68a0b3f8e6f825b6f",
            "5a152bd68832997cd8c72f93"
          ],
          "price": 382259.15,
          "address": "899 Schenck Place",
          "city": "Hannasville",
          "zip": 60660,
          "BR": 3,
          "BA": 2,
          "monthlyInsurance": 787.75,
          "monthlyTaxes": 784.92,
          "monthlyPMI": 328.1,
          "monthlyHOA": 887.24,
          "closingCosts": 1394.68,
          "createDate": "2015-12-09T05:42:56",
          "updateDate": "2017-05-03T10:12:52",
          "from": "Redfin",
          "createdBy": "stacisantos@zillactic.com"
        }
      ],
      "createDate": "2016-09-02T05:45:06",
      "updateDate": "2017-05-19T03:28:18",
      "createdBy": "stacisantos@zillactic.com"
    },
    {
      "Id": "5a152bd6608fa60062ecc0b1",
      "lastname": "Brown",
      "firstname": "Maldonado",
      "status": "Looking without Agent",
      "state": "TRACK",
      "email": "maldonadobrown@zillactic.com",
      "monthlyGross": 1997.26,
      "debt": 63389.01,
      "expenses": 1000,
      "targetedZipCodes": [
        60635,
        60678
      ],
      "totalCash": 5205,
      "downPayment": 8203.54,
      "mortgageType": "5Yr",
      "totalCashToClose": 7099.98,
      "emergencyFund": 2487.53,
      "creditScoreRange": "<640",
      "trackingHistory": [
        {
          "Id": "5a152bd62315c58314962a3a",
          "status": "SENT",
          "professionalId": "5a152bd6ad36095768944bba",
          "createDate": "2014-10-11T06:27:21"
        },
        {
          "Id": "5a152bd6d1a3fac1ecbce5a4",
          "status": "DENY",
          "professionalId": "5a152bd64dd614546bbabc5c",
          "createDate": "2016-02-18T07:21:57"
        },
        {
          "Id": "5a152bd66ac516933981def1",
          "status": "CONFIRM",
          "professionalId": "5a152bd69b095b1254f60b35",
          "createDate": "2017-11-03T04:13:17"
        },
        {
          "Id": "5a152bd6c81683f7ff6ee44b",
          "status": "TRACK",
          "professionalId": "5a152bd65228727448170aa6",
          "createDate": "2015-02-16T06:21:07"
        }
      ],
      "need": {
        "priceMin": 122245.25,
        "priceMax": 450489.3,
        "BA": 2,
        "BR": 1,
        "needs": [
          "Garage",
          "Close to L",
          "Balcony"
        ]
      },
      "wants": [
        "Pool",
        "Wired for Sound",
        "Wired for Sound"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd605c41fc7074c9851",
            "5a152bd671c1fe6bb6047688",
            "5a152bd6b143a599fd0ec2fc",
            "5a152bd678ef81f60c87a1bd",
            "5a152bd609615791e29fc48a",
            "5a152bd6e31aec9a4067c768"
          ],
          "price": 684533.19,
          "address": "971 Linden Street",
          "city": "Weeksville",
          "zip": 60657,
          "BR": 4,
          "BA": 2,
          "monthlyInsurance": 316.34,
          "monthlyTaxes": 251.92,
          "monthlyPMI": 311.5,
          "monthlyHOA": 729.48,
          "closingCosts": 1654.96,
          "createDate": "2017-05-27T08:22:15",
          "updateDate": "2017-10-03T07:57:43",
          "from": "Redfin",
          "createdBy": "maldonadobrown@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6392905ad93ea6d26",
            "5a152bd6319ebf8854b69792",
            "5a152bd62eab8e8248a2982c",
            "5a152bd63dcc531878d6f0e4",
            "5a152bd6911dce44e926ae51",
            "5a152bd63f020220909bde77",
            "5a152bd61be8e71e8c2d854f",
            "5a152bd68f90954757682dd6"
          ],
          "price": 406020.01,
          "address": "380 Gelston Avenue",
          "city": "Suitland",
          "zip": 60618,
          "BR": 1,
          "BA": 1,
          "monthlyInsurance": 339.86,
          "monthlyTaxes": 952.16,
          "monthlyPMI": 308.34,
          "monthlyHOA": 809.46,
          "closingCosts": 1046.13,
          "createDate": "2017-07-18T02:27:01",
          "updateDate": "2014-01-15T09:00:09",
          "from": "Redfin",
          "createdBy": "maldonadobrown@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd60e4b9d61dc6eba8c",
            "5a152bd620ed7ed429ee3e08",
            "5a152bd6965d164b27fc498c",
            "5a152bd6289a281416ecf723",
            "5a152bd65ecd2b43c7a16bb9",
            "5a152bd6b21001fec31521b2"
          ],
          "price": 7681.62,
          "address": "394 Jay Street",
          "city": "Vienna",
          "zip": 60642,
          "BR": 1,
          "BA": 1,
          "monthlyInsurance": 322.47,
          "monthlyTaxes": 556.01,
          "monthlyPMI": 931.1,
          "monthlyHOA": 324.14,
          "closingCosts": 1316.9,
          "createDate": "2015-08-15T02:07:04",
          "updateDate": "2017-01-14T07:00:10",
          "from": "Zillow",
          "createdBy": "maldonadobrown@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd66e4affd4d6b38466",
            "5a152bd6fb32e67e9a1af554",
            "5a152bd62f540e6f67a93321",
            "5a152bd6897ff0308ea23e64",
            "5a152bd6260fcab547631a87",
            "5a152bd6808a414c3bd0318d",
            "5a152bd625049c97bc711221"
          ],
          "price": 539482.82,
          "address": "377 Banner Avenue",
          "city": "Tilden",
          "zip": 60689,
          "BR": 2,
          "BA": 1,
          "monthlyInsurance": 961.49,
          "monthlyTaxes": 607.08,
          "monthlyPMI": 289.36,
          "monthlyHOA": 531.87,
          "closingCosts": 1545.36,
          "createDate": "2015-06-24T03:04:10",
          "updateDate": "2014-02-05T03:40:33",
          "from": "Zillow",
          "createdBy": "maldonadobrown@zillactic.com"
        }
      ],
      "createDate": "2016-12-14T09:32:40",
      "updateDate": "2014-10-18T10:21:14",
      "createdBy": "maldonadobrown@zillactic.com"
    },
    {
      "Id": "5a152bd6bab56bc3845fc6e3",
      "lastname": "Bradshaw",
      "firstname": "Cochran",
      "status": "Actively Looking",
      "state": "TRACK",
      "email": "cochranbradshaw@zillactic.com",
      "monthlyGross": 2679.16,
      "debt": 34503.3,
      "expenses": 1000,
      "targetedZipCodes": [
        60651,
        60636
      ],
      "totalCash": 6021.3,
      "downPayment": 5742.96,
      "mortgageType": "5Yr",
      "totalCashToClose": 7262.67,
      "emergencyFund": 2493.98,
      "creditScoreRange": "720-739",
      "trackingHistory": [
        {
          "Id": "5a152bd6e0c6d64702d350bf",
          "status": "SENT",
          "professionalId": "5a152bd67abc1e07213a05fb",
          "createDate": "2014-03-03T01:37:26"
        },
        {
          "Id": "5a152bd6e85580009248b0cc",
          "status": "DENY",
          "professionalId": "5a152bd63c305da0d9e86aeb",
          "createDate": "2015-11-10T12:02:57"
        },
        {
          "Id": "5a152bd656368586c97879fc",
          "status": "CONFIRM",
          "professionalId": "5a152bd6e81c2d060d1fb83a",
          "createDate": "2015-08-09T10:15:59"
        },
        {
          "Id": "5a152bd6b39341735794c15c",
          "status": "TRACK",
          "professionalId": "5a152bd63dd985e953f12640",
          "createDate": "2015-12-15T12:39:14"
        }
      ],
      "need": {
        "priceMin": 741821.49,
        "priceMax": 539039.71,
        "BA": 3,
        "BR": 4,
        "needs": [
          "Granite Counter Tops",
          "Close to L",
          "Garage",
          "Garage",
          "Garage",
          "Balcony"
        ]
      },
      "wants": [
        "Wired for Sound"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd6012ca3c7cf188a1d"
          ],
          "price": 594354.95,
          "address": "487 Varanda Place",
          "city": "Rivers",
          "zip": 60639,
          "BR": 3,
          "BA": 1,
          "monthlyInsurance": 704.76,
          "monthlyTaxes": 330.57,
          "monthlyPMI": 900.62,
          "monthlyHOA": 420.65,
          "closingCosts": 1720.97,
          "createDate": "2014-01-28T04:52:38",
          "updateDate": "2015-11-16T10:45:18",
          "from": "Zillow",
          "createdBy": "cochranbradshaw@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6133e78715b83fd04",
            "5a152bd6308e3701d1195537",
            "5a152bd6e309ec397b348c37",
            "5a152bd652936055021bbcc4",
            "5a152bd6f9584fe03298dd3b"
          ],
          "price": 116364.41,
          "address": "210 Mill Avenue",
          "city": "Chesterfield",
          "zip": 60617,
          "BR": 4,
          "BA": 2,
          "monthlyInsurance": 383.26,
          "monthlyTaxes": 948.31,
          "monthlyPMI": 464.19,
          "monthlyHOA": 715.89,
          "closingCosts": 1699.6,
          "createDate": "2016-09-04T03:47:44",
          "updateDate": "2015-12-24T12:52:30",
          "from": "HomeBloq",
          "createdBy": "cochranbradshaw@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd67f5541f23482bb9f",
            "5a152bd623a6be073f87cce2",
            "5a152bd62980892637a9fee4",
            "5a152bd6e2d33f9b59696143",
            "5a152bd6c70f81cd10ccbe6d",
            "5a152bd6a95bccb391169e8f",
            "5a152bd6267d5800225e413d"
          ],
          "price": 359419.65,
          "address": "510 Hamilton Avenue",
          "city": "Datil",
          "zip": 60628,
          "BR": 3,
          "BA": 2,
          "monthlyInsurance": 665,
          "monthlyTaxes": 890.95,
          "monthlyPMI": 756.61,
          "monthlyHOA": 906.49,
          "closingCosts": 1707.51,
          "createDate": "2015-02-09T08:46:18",
          "updateDate": "2017-03-09T07:26:45",
          "from": "Zillow",
          "createdBy": "cochranbradshaw@zillactic.com"
        }
      ],
      "createDate": "2015-10-06T11:46:04",
      "updateDate": "2015-07-04T03:07:30",
      "createdBy": "cochranbradshaw@zillactic.com"
    },
    {
      "Id": "5a152bd629b472ae5375d7dd",
      "lastname": "Salazar",
      "firstname": "Nadia",
      "status": "Just Starting Out",
      "state": "CONFIRM",
      "email": "nadiasalazar@zillactic.com",
      "monthlyGross": 1709.95,
      "debt": 68557.33,
      "expenses": 1000,
      "targetedZipCodes": [
        60674,
        60608,
        60689,
        60653,
        60635,
        60663
      ],
      "totalCash": 8935.89,
      "downPayment": 4671.76,
      "mortgageType": "15yr",
      "totalCashToClose": 7363.8,
      "emergencyFund": 1387.64,
      "creditScoreRange": "640-659",
      "trackingHistory": [
        {
          "Id": "5a152bd64609b4d875efb0ed",
          "status": "SENT",
          "professionalId": "5a152bd64b01e883d251a9c2",
          "createDate": "2015-07-18T03:40:19"
        },
        {
          "Id": "5a152bd6371f0c1f63755b1b",
          "status": "DENY",
          "professionalId": "5a152bd6969c4cec2911bb09",
          "createDate": "2014-05-23T01:37:06"
        },
        {
          "Id": "5a152bd626865f1e465086d9",
          "status": "CONFIRM",
          "professionalId": "5a152bd63f8a889e97799550",
          "createDate": "2015-11-08T12:46:39"
        },
        {
          "Id": "5a152bd69ac56ce9657f6976",
          "status": "TRACK",
          "professionalId": "5a152bd63e33c52043f2cff6",
          "createDate": "2016-11-17T11:29:09"
        }
      ],
      "need": {
        "priceMin": 276826.72,
        "priceMax": 194829.03,
        "BA": 1,
        "BR": 3,
        "needs": [
          "Balcony",
          "Granite Counter Tops",
          "Close to L",
          "Deck",
          "Granite Counter Tops",
          "Garage"
        ]
      },
      "wants": [
        "Pool"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd64b2acc965ccdfae6",
            "5a152bd64d962d67eb9232c9",
            "5a152bd66b49c8be72af49fc",
            "5a152bd6a022b9d5a8470f68",
            "5a152bd6de63a4724558efd0",
            "5a152bd66a165f6a1fe450f0"
          ],
          "price": 721050.35,
          "address": "787 Danforth Street",
          "city": "Bennett",
          "zip": 60689,
          "BR": 4,
          "BA": 2,
          "monthlyInsurance": 492.05,
          "monthlyTaxes": 836.48,
          "monthlyPMI": 378.77,
          "monthlyHOA": 982.88,
          "closingCosts": 1731.33,
          "createDate": "2014-12-17T06:56:37",
          "updateDate": "2015-11-02T06:59:01",
          "from": "Zillow",
          "createdBy": "nadiasalazar@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd613c00f48e84ceaff",
            "5a152bd6bab96d2614d26875",
            "5a152bd695bb7aed19c759c8",
            "5a152bd661bf9c84c82a5faf",
            "5a152bd69b2c2390d33595e4",
            "5a152bd681425ee5509c03ab",
            "5a152bd6b02862bd9a68102c",
            "5a152bd6673bebc9b5174d4c"
          ],
          "price": 81979.43,
          "address": "694 Clay Street",
          "city": "Avalon",
          "zip": 60625,
          "BR": 3,
          "BA": 2,
          "monthlyInsurance": 781.76,
          "monthlyTaxes": 471.61,
          "monthlyPMI": 207.55,
          "monthlyHOA": 761.09,
          "closingCosts": 1171.91,
          "createDate": "2015-06-23T01:34:28",
          "updateDate": "2014-02-17T08:39:31",
          "from": "MLS",
          "createdBy": "nadiasalazar@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd658123100c50e1bbd",
            "5a152bd681097b45fe6b04ee"
          ],
          "price": 484662.99,
          "address": "513 Moultrie Street",
          "city": "Hatteras",
          "zip": 60620,
          "BR": 2,
          "BA": 2,
          "monthlyInsurance": 343.59,
          "monthlyTaxes": 861.48,
          "monthlyPMI": 218.73,
          "monthlyHOA": 641.88,
          "closingCosts": 1471.65,
          "createDate": "2015-04-27T04:35:10",
          "updateDate": "2017-03-07T03:31:19",
          "from": "MLS",
          "createdBy": "nadiasalazar@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd64bbd5f0d7c340d46"
          ],
          "price": 276292.62,
          "address": "345 Sutton Street",
          "city": "Driftwood",
          "zip": 60625,
          "BR": 3,
          "BA": 4,
          "monthlyInsurance": 247.97,
          "monthlyTaxes": 727.16,
          "monthlyPMI": 847.06,
          "monthlyHOA": 258.01,
          "closingCosts": 1422.32,
          "createDate": "2014-09-27T09:28:48",
          "updateDate": "2015-08-20T01:50:43",
          "from": "MLS",
          "createdBy": "nadiasalazar@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6fc2a1a63961c7a3d",
            "5a152bd63c9732a9a7a07a9f",
            "5a152bd60cfb04dbc86e1e82",
            "5a152bd6d5c9c69b12f7f929",
            "5a152bd6222f221ec03d30b4",
            "5a152bd6736de8e9082b490e",
            "5a152bd69eaa9d31a1f8e672"
          ],
          "price": 219141.18,
          "address": "340 Celeste Court",
          "city": "Derwood",
          "zip": 60612,
          "BR": 3,
          "BA": 2,
          "monthlyInsurance": 334.79,
          "monthlyTaxes": 703.59,
          "monthlyPMI": 952.46,
          "monthlyHOA": 475.09,
          "closingCosts": 1160.37,
          "createDate": "2014-12-03T07:12:58",
          "updateDate": "2016-03-11T06:13:40",
          "from": "MLS",
          "createdBy": "nadiasalazar@zillactic.com"
        }
      ],
      "createDate": "2017-04-26T01:36:44",
      "updateDate": "2016-06-16T06:40:15",
      "createdBy": "nadiasalazar@zillactic.com"
    },
    {
      "Id": "5a152bd628f3a62bcae62630",
      "lastname": "Freeman",
      "firstname": "Roberts",
      "status": "Just Starting Out",
      "state": "DENY",
      "email": "robertsfreeman@zillactic.com",
      "monthlyGross": 2195.8,
      "debt": 75604.17,
      "expenses": 1000,
      "targetedZipCodes": [
        60616,
        60674,
        60644,
        60647,
        60657,
        60628
      ],
      "totalCash": 8993.51,
      "downPayment": 6507.33,
      "mortgageType": "30Yr",
      "totalCashToClose": 4519.74,
      "emergencyFund": 3697.19,
      "creditScoreRange": "680-99",
      "trackingHistory": [
        {
          "Id": "5a152bd6baf8c4284ef03856",
          "status": "SENT",
          "professionalId": "5a152bd6cd4c018892deb292",
          "createDate": "2014-07-03T11:57:53"
        },
        {
          "Id": "5a152bd6acb80611d123fab1",
          "status": "DENY",
          "professionalId": "5a152bd6e39804ed523f4d81",
          "createDate": "2014-01-10T12:22:06"
        },
        {
          "Id": "5a152bd688c79e638837a73c",
          "status": "CONFIRM",
          "professionalId": "5a152bd6bf8a6bdcf336a014",
          "createDate": "2015-04-05T06:49:19"
        },
        {
          "Id": "5a152bd6c8382a9ae250dff1",
          "status": "TRACK",
          "professionalId": "5a152bd61029bb1c591df07f",
          "createDate": "2016-01-05T08:57:27"
        }
      ],
      "need": {
        "priceMin": 516405.4,
        "priceMax": 85155.57,
        "BA": 3,
        "BR": 2,
        "needs": [
          "Deck",
          "Close to L",
          "Balcony"
        ]
      },
      "wants": [
        "Rooftop Balcony",
        "Pool"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd64ded9ba221cf7efa",
            "5a152bd65be85d890a68d8f3",
            "5a152bd6daf26724faccfa83",
            "5a152bd6b1b63c1b2577afe2"
          ],
          "price": 428772.47,
          "address": "797 Flatbush Avenue",
          "city": "Edmund",
          "zip": 60601,
          "BR": 3,
          "BA": 1,
          "monthlyInsurance": 808.88,
          "monthlyTaxes": 348.08,
          "monthlyPMI": 991.26,
          "monthlyHOA": 414.53,
          "closingCosts": 1518.38,
          "createDate": "2017-11-01T05:17:36",
          "updateDate": "2015-05-24T10:14:17",
          "from": "MLS",
          "createdBy": "robertsfreeman@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6b59d9904969712e4",
            "5a152bd698d9cf0b557859ad",
            "5a152bd6696ce648441d29e5",
            "5a152bd642c6cb32d1560112",
            "5a152bd6ff3688bd14910e58",
            "5a152bd6b2499fcd054e0a96",
            "5a152bd6b6f627d2f4f97796"
          ],
          "price": 68432.65,
          "address": "609 Ashford Street",
          "city": "Leola",
          "zip": 60621,
          "BR": 2,
          "BA": 2,
          "monthlyInsurance": 734.5,
          "monthlyTaxes": 625.55,
          "monthlyPMI": 360.47,
          "monthlyHOA": 268.84,
          "closingCosts": 1622.74,
          "createDate": "2015-12-11T05:44:39",
          "updateDate": "2015-05-07T11:31:09",
          "from": "HomeBloq",
          "createdBy": "robertsfreeman@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd665339279ee7e1e04",
            "5a152bd6fd85e480f1ced604",
            "5a152bd630a504ad7507fff9",
            "5a152bd6825f13385684363f",
            "5a152bd61b17d62d786d9bb2",
            "5a152bd62222277c8a4c509e"
          ],
          "price": 658058.25,
          "address": "106 Gerald Court",
          "city": "Blackgum",
          "zip": 60639,
          "BR": 1,
          "BA": 3,
          "monthlyInsurance": 688.45,
          "monthlyTaxes": 641.96,
          "monthlyPMI": 774.53,
          "monthlyHOA": 754.93,
          "closingCosts": 1259.71,
          "createDate": "2015-04-25T08:07:27",
          "updateDate": "2015-06-12T07:42:34",
          "from": "Zillow",
          "createdBy": "robertsfreeman@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6721920adf3e814c9",
            "5a152bd63a29d9ea5287d2e3"
          ],
          "price": 488947.37,
          "address": "929 Front Street",
          "city": "Eden",
          "zip": 60617,
          "BR": 2,
          "BA": 2,
          "monthlyInsurance": 894.77,
          "monthlyTaxes": 839.52,
          "monthlyPMI": 965.15,
          "monthlyHOA": 982.43,
          "closingCosts": 1850.1,
          "createDate": "2015-05-29T07:13:14",
          "updateDate": "2016-01-20T12:33:51",
          "from": "MLS",
          "createdBy": "robertsfreeman@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6cced7ee7b0cfc5f3",
            "5a152bd6ca09a3356d9fe593",
            "5a152bd6f589d4c15554db1b",
            "5a152bd6c25d0f03dc4021bc",
            "5a152bd65daf8a9b0792b0e8",
            "5a152bd6529b41277f88f879",
            "5a152bd6f40a6a56fd54b18a"
          ],
          "price": 88468.2,
          "address": "983 Meserole Street",
          "city": "Shrewsbury",
          "zip": 60620,
          "BR": 1,
          "BA": 1,
          "monthlyInsurance": 642.94,
          "monthlyTaxes": 430.28,
          "monthlyPMI": 558.59,
          "monthlyHOA": 608.96,
          "closingCosts": 1135.16,
          "createDate": "2015-01-01T06:04:11",
          "updateDate": "2014-06-23T04:00:01",
          "from": "MLS",
          "createdBy": "robertsfreeman@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd67eadd840b0e83637",
            "5a152bd64d1ec749341a42ff",
            "5a152bd67e1c224e23398b38",
            "5a152bd6c76839fd6b1a96fe",
            "5a152bd6aa234e677f519d91",
            "5a152bd67ec21b37525aef09"
          ],
          "price": 106958.78,
          "address": "169 Gardner Avenue",
          "city": "Laurelton",
          "zip": 60619,
          "BR": 2,
          "BA": 4,
          "monthlyInsurance": 318.98,
          "monthlyTaxes": 270.57,
          "monthlyPMI": 267.03,
          "monthlyHOA": 210.6,
          "closingCosts": 1248.72,
          "createDate": "2017-09-17T05:48:09",
          "updateDate": "2016-03-05T08:08:48",
          "from": "HomeBloq",
          "createdBy": "robertsfreeman@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd69041fb8bc6e6e351",
            "5a152bd67507990778031380",
            "5a152bd6006b25e116c53bf0",
            "5a152bd6a138a16349021f86",
            "5a152bd6c18e68f7599fbd50",
            "5a152bd6641a70511778935e"
          ],
          "price": 414379.25,
          "address": "127 Bragg Court",
          "city": "Dante",
          "zip": 60612,
          "BR": 3,
          "BA": 4,
          "monthlyInsurance": 345.3,
          "monthlyTaxes": 632.69,
          "monthlyPMI": 397.46,
          "monthlyHOA": 369.43,
          "closingCosts": 1768.18,
          "createDate": "2015-10-27T12:15:54",
          "updateDate": "2015-03-03T05:06:15",
          "from": "Redfin",
          "createdBy": "robertsfreeman@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd645e470ea440c6813",
            "5a152bd6d486fc101fc5ec54",
            "5a152bd6448ec2d138aba8f8",
            "5a152bd6797bbe37a377bb07",
            "5a152bd63003d8d8f1b23e15"
          ],
          "price": 517126.48,
          "address": "424 Fleet Place",
          "city": "Stewart",
          "zip": 60641,
          "BR": 1,
          "BA": 1,
          "monthlyInsurance": 707.75,
          "monthlyTaxes": 681.17,
          "monthlyPMI": 861.86,
          "monthlyHOA": 900.87,
          "closingCosts": 1140.96,
          "createDate": "2016-11-02T08:36:21",
          "updateDate": "2014-09-17T01:52:57",
          "from": "Zillow",
          "createdBy": "robertsfreeman@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6ce3df40857c8902c",
            "5a152bd6b48f6f6a771cbca2",
            "5a152bd65edf83b85118c2cc",
            "5a152bd66550ff5c8c61e3f6",
            "5a152bd66e7149d119fbfc94",
            "5a152bd6df58128ce4338dfd",
            "5a152bd6277ca19bb2daba1a",
            "5a152bd6bd7983e0c27baa90"
          ],
          "price": 255091.8,
          "address": "760 Corbin Place",
          "city": "Onton",
          "zip": 60681,
          "BR": 4,
          "BA": 4,
          "monthlyInsurance": 471.4,
          "monthlyTaxes": 203.82,
          "monthlyPMI": 744.39,
          "monthlyHOA": 803.67,
          "closingCosts": 1560.32,
          "createDate": "2015-06-01T01:24:39",
          "updateDate": "2017-09-30T03:34:25",
          "from": "HomeBloq",
          "createdBy": "robertsfreeman@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6c5ad5b704fe0f9be"
          ],
          "price": 4202.88,
          "address": "897 Claver Place",
          "city": "Linwood",
          "zip": 60615,
          "BR": 4,
          "BA": 2,
          "monthlyInsurance": 697.99,
          "monthlyTaxes": 567.14,
          "monthlyPMI": 860.85,
          "monthlyHOA": 264.82,
          "closingCosts": 1175.05,
          "createDate": "2016-05-25T08:13:54",
          "updateDate": "2017-06-24T12:36:30",
          "from": "Zillow",
          "createdBy": "robertsfreeman@zillactic.com"
        }
      ],
      "createDate": "2016-06-07T09:57:51",
      "updateDate": "2015-01-29T12:42:57",
      "createdBy": "robertsfreeman@zillactic.com"
    },
    {
      "Id": "5a152bd6cb6fbafbc5acd0a0",
      "lastname": "English",
      "firstname": "Noel",
      "status": "Looking without Agent",
      "state": "CONFIRM",
      "email": "noelenglish@zillactic.com",
      "monthlyGross": 3183.06,
      "debt": 97165.37,
      "expenses": 1000,
      "targetedZipCodes": [
        60671,
        60648,
        60654,
        60675,
        60684
      ],
      "totalCash": 7510.13,
      "downPayment": 6037.69,
      "mortgageType": "15yr",
      "totalCashToClose": 5174.87,
      "emergencyFund": 3680.05,
      "creditScoreRange": "760+",
      "trackingHistory": [
        {
          "Id": "5a152bd6ea0354888b5aef64",
          "status": "SENT",
          "professionalId": "5a152bd6bce51ff1e386e610",
          "createDate": "2014-11-17T08:35:53"
        },
        {
          "Id": "5a152bd6dc69f090ff1b5432",
          "status": "DENY",
          "professionalId": "5a152bd6cce0707ebb376a4a",
          "createDate": "2015-07-26T10:34:41"
        },
        {
          "Id": "5a152bd611a6e622e3754689",
          "status": "CONFIRM",
          "professionalId": "5a152bd64e58ef6bfa3a29d5",
          "createDate": "2015-03-06T04:47:34"
        },
        {
          "Id": "5a152bd6b3b1eb7d8e828ed5",
          "status": "TRACK",
          "professionalId": "5a152bd617bac2d761ea2902",
          "createDate": "2017-02-04T11:22:04"
        }
      ],
      "need": {
        "priceMin": 904696.97,
        "priceMax": 778542.6,
        "BA": 4,
        "BR": 2,
        "needs": [
          "Garage",
          "Close to L",
          "Garage",
          "Garage"
        ]
      },
      "wants": [
        "Wired for Sound",
        "Rooftop Balcony"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd6b3879c94805c5a06",
            "5a152bd66c857ec21f7e49bd"
          ],
          "price": 28870.64,
          "address": "419 Berry Street",
          "city": "Bartonsville",
          "zip": 60601,
          "BR": 4,
          "BA": 2,
          "monthlyInsurance": 766.65,
          "monthlyTaxes": 316.7,
          "monthlyPMI": 682.18,
          "monthlyHOA": 890.94,
          "closingCosts": 1423.06,
          "createDate": "2016-04-05T03:52:36",
          "updateDate": "2015-10-01T11:01:18",
          "from": "MLS",
          "createdBy": "noelenglish@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6296207e2b896a664",
            "5a152bd67218cf83229b90ef",
            "5a152bd65e64bbd25744ec2d",
            "5a152bd69e801a5309f09fee"
          ],
          "price": 45790.15,
          "address": "849 Benson Avenue",
          "city": "Springville",
          "zip": 60600,
          "BR": 4,
          "BA": 4,
          "monthlyInsurance": 223.15,
          "monthlyTaxes": 943.88,
          "monthlyPMI": 919.98,
          "monthlyHOA": 354.96,
          "closingCosts": 1267.19,
          "createDate": "2017-11-12T02:48:35",
          "updateDate": "2015-06-01T10:40:56",
          "from": "Zillow",
          "createdBy": "noelenglish@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd64abf6fee0d5de257"
          ],
          "price": 595725.3,
          "address": "723 Loring Avenue",
          "city": "Juarez",
          "zip": 60625,
          "BR": 1,
          "BA": 1,
          "monthlyInsurance": 646.59,
          "monthlyTaxes": 294.84,
          "monthlyPMI": 968.42,
          "monthlyHOA": 429.47,
          "closingCosts": 1408.11,
          "createDate": "2015-09-22T10:49:35",
          "updateDate": "2015-12-26T03:48:14",
          "from": "MLS",
          "createdBy": "noelenglish@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd699d081ca5bffc46c",
            "5a152bd6d3acda9e588994f9",
            "5a152bd6107e85f2058b9c75",
            "5a152bd6c321d86e4d6c86f3"
          ],
          "price": 503234.91,
          "address": "622 Ford Street",
          "city": "Venice",
          "zip": 60648,
          "BR": 4,
          "BA": 1,
          "monthlyInsurance": 368.09,
          "monthlyTaxes": 795.48,
          "monthlyPMI": 998.83,
          "monthlyHOA": 978.9,
          "closingCosts": 1824.01,
          "createDate": "2016-11-07T07:15:55",
          "updateDate": "2014-01-04T07:20:56",
          "from": "MLS",
          "createdBy": "noelenglish@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd646ab16f69c45aa52",
            "5a152bd6da145d1e4463820e",
            "5a152bd6bb2157ed06013be8",
            "5a152bd69d9a324ad3b433e7",
            "5a152bd6ffff1d6f24a6bb7a",
            "5a152bd623608afb73769ed2",
            "5a152bd67370ee48f3612b08"
          ],
          "price": 801984.99,
          "address": "483 Brooklyn Road",
          "city": "Ellerslie",
          "zip": 60603,
          "BR": 2,
          "BA": 3,
          "monthlyInsurance": 455.64,
          "monthlyTaxes": 304.39,
          "monthlyPMI": 333.12,
          "monthlyHOA": 473.92,
          "closingCosts": 1921.75,
          "createDate": "2017-07-01T11:44:53",
          "updateDate": "2017-08-11T06:10:15",
          "from": "Zillow",
          "createdBy": "noelenglish@zillactic.com"
        }
      ],
      "createDate": "2017-06-12T06:42:25",
      "updateDate": "2016-12-26T03:58:04",
      "createdBy": "noelenglish@zillactic.com"
    },
    {
      "Id": "5a152bd665f8c7c0e0b5afc2",
      "lastname": "Peck",
      "firstname": "Knight",
      "status": "Actively Looking w/ Agent",
      "state": "DENY",
      "email": "knightpeck@zillactic.com",
      "monthlyGross": 3770.47,
      "debt": 48954.32,
      "expenses": 1000,
      "targetedZipCodes": [
        60649,
        60655
      ],
      "totalCash": 6849.47,
      "downPayment": 6678.23,
      "mortgageType": "15yr",
      "totalCashToClose": 9519.3,
      "emergencyFund": 3857.4,
      "creditScoreRange": "680-99",
      "trackingHistory": [
        {
          "Id": "5a152bd65c5dd48e9e6a5a53",
          "status": "SENT",
          "professionalId": "5a152bd63d1035e16f06f22e",
          "createDate": "2014-07-07T06:12:53"
        },
        {
          "Id": "5a152bd6fc80a0d4a56d180a",
          "status": "DENY",
          "professionalId": "5a152bd6946773ecccea177f",
          "createDate": "2014-05-05T09:02:23"
        },
        {
          "Id": "5a152bd6daac536238b181a3",
          "status": "CONFIRM",
          "professionalId": "5a152bd6a01859dd06db074e",
          "createDate": "2017-01-05T03:14:03"
        },
        {
          "Id": "5a152bd6428063c55ee796cb",
          "status": "TRACK",
          "professionalId": "5a152bd67f1facc307bd1bb9",
          "createDate": "2015-03-13T05:59:21"
        }
      ],
      "need": {
        "priceMin": 452656.03,
        "priceMax": 937270.17,
        "BA": 3,
        "BR": 3,
        "needs": [
          "Balcony",
          "Deck",
          "Garage",
          "Granite Counter Tops",
          "Granite Counter Tops",
          "Garage",
          "Close to L"
        ]
      },
      "wants": [
        "Wired for Sound",
        "Pool",
        "Pool"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd6dc9d420b3018adfa",
            "5a152bd6855fea2141b58b8f",
            "5a152bd62f1cc41b5f09dc5b",
            "5a152bd684c9a2bdec8c1dca",
            "5a152bd6bf7efd5b64ba22e5",
            "5a152bd606018fe1c0cb0219",
            "5a152bd6e74722b6cae780c1"
          ],
          "price": 661286.11,
          "address": "257 Glen Street",
          "city": "Williston",
          "zip": 60669,
          "BR": 1,
          "BA": 1,
          "monthlyInsurance": 382.44,
          "monthlyTaxes": 734.73,
          "monthlyPMI": 536.19,
          "monthlyHOA": 722.48,
          "closingCosts": 1325.66,
          "createDate": "2015-07-22T08:09:25",
          "updateDate": "2016-07-09T09:26:46",
          "from": "Zillow",
          "createdBy": "knightpeck@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6a958b1afe934049a"
          ],
          "price": 773641.35,
          "address": "714 Louis Place",
          "city": "Curtice",
          "zip": 60632,
          "BR": 4,
          "BA": 2,
          "monthlyInsurance": 596.62,
          "monthlyTaxes": 873.97,
          "monthlyPMI": 579,
          "monthlyHOA": 757.95,
          "closingCosts": 1777.98,
          "createDate": "2016-06-28T12:28:58",
          "updateDate": "2015-08-31T10:49:07",
          "from": "HomeBloq",
          "createdBy": "knightpeck@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6402b1c4f08adec8a"
          ],
          "price": 934491.04,
          "address": "979 Ryerson Street",
          "city": "Wiscon",
          "zip": 60638,
          "BR": 4,
          "BA": 3,
          "monthlyInsurance": 893.2,
          "monthlyTaxes": 540.16,
          "monthlyPMI": 976.85,
          "monthlyHOA": 548.83,
          "closingCosts": 1847.45,
          "createDate": "2015-04-13T06:21:17",
          "updateDate": "2017-07-11T05:20:00",
          "from": "Redfin",
          "createdBy": "knightpeck@zillactic.com"
        }
      ],
      "createDate": "2017-01-15T03:40:33",
      "updateDate": "2014-08-30T07:47:26",
      "createdBy": "knightpeck@zillactic.com"
    },
    {
      "Id": "5a152bd65e8d5aa410a28e42",
      "lastname": "Mathis",
      "firstname": "Chambers",
      "status": "Just Starting Out",
      "state": "TRACK",
      "email": "chambersmathis@zillactic.com",
      "monthlyGross": 2385.1,
      "debt": 70508.76,
      "expenses": 1000,
      "targetedZipCodes": [
        60684,
        60604,
        60651,
        60651,
        60618,
        60672
      ],
      "totalCash": 5957.32,
      "downPayment": 9825.07,
      "mortgageType": "30Yr",
      "totalCashToClose": 5706.51,
      "emergencyFund": 1860.89,
      "creditScoreRange": "720-739",
      "trackingHistory": [
        {
          "Id": "5a152bd6101337b5cb62e7a7",
          "status": "SENT",
          "professionalId": "5a152bd6f3b0d5bfa745c6d8",
          "createDate": "2015-11-13T02:03:10"
        },
        {
          "Id": "5a152bd66452118352acd289",
          "status": "DENY",
          "professionalId": "5a152bd6c2111d29769a1711",
          "createDate": "2015-12-30T03:37:16"
        },
        {
          "Id": "5a152bd6f81c86f0aed7ec08",
          "status": "CONFIRM",
          "professionalId": "5a152bd6219dabfe2f46eeba",
          "createDate": "2016-07-17T02:06:41"
        },
        {
          "Id": "5a152bd65691edae4cda1b51",
          "status": "TRACK",
          "professionalId": "5a152bd6194a58a85e4d57dc",
          "createDate": "2016-01-09T09:39:13"
        }
      ],
      "need": {
        "priceMin": 847324.06,
        "priceMax": 532739.43,
        "BA": 1,
        "BR": 4,
        "needs": [
          "Deck",
          "Granite Counter Tops",
          "Granite Counter Tops",
          "Garage",
          "Close to L",
          "Granite Counter Tops",
          "Deck"
        ]
      },
      "wants": [
        "Wired for Sound"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd66a3fa0d28bab6208",
            "5a152bd65166447463196c11",
            "5a152bd6898e27b49fead805",
            "5a152bd63dc22cc363dc74dc",
            "5a152bd6f48dcdf49f8e267c",
            "5a152bd6aadad51c965ed03d",
            "5a152bd6abfe482ca4221f15"
          ],
          "price": 579798.21,
          "address": "220 Midwood Street",
          "city": "Castleton",
          "zip": 60656,
          "BR": 4,
          "BA": 4,
          "monthlyInsurance": 920.41,
          "monthlyTaxes": 553.57,
          "monthlyPMI": 762.07,
          "monthlyHOA": 391.82,
          "closingCosts": 1991.44,
          "createDate": "2016-11-03T03:59:59",
          "updateDate": "2014-10-03T03:49:23",
          "from": "HomeBloq",
          "createdBy": "chambersmathis@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6ccb1b88124954422",
            "5a152bd65c507ebdf08ed04c",
            "5a152bd6b780410d11f55ae6"
          ],
          "price": 9919.82,
          "address": "198 Harden Street",
          "city": "Caberfae",
          "zip": 60641,
          "BR": 3,
          "BA": 4,
          "monthlyInsurance": 935.93,
          "monthlyTaxes": 209.1,
          "monthlyPMI": 475.11,
          "monthlyHOA": 568.14,
          "closingCosts": 1445.34,
          "createDate": "2014-10-14T05:08:24",
          "updateDate": "2016-04-18T05:55:52",
          "from": "MLS",
          "createdBy": "chambersmathis@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6f22556afe5d760cb",
            "5a152bd66296c86cc345a08c",
            "5a152bd6eb4ad541ac61935e"
          ],
          "price": 732802.5,
          "address": "612 Losee Terrace",
          "city": "Floris",
          "zip": 60682,
          "BR": 1,
          "BA": 1,
          "monthlyInsurance": 913.88,
          "monthlyTaxes": 529.22,
          "monthlyPMI": 275,
          "monthlyHOA": 453.66,
          "closingCosts": 1449.5,
          "createDate": "2015-06-08T04:30:31",
          "updateDate": "2017-06-29T06:04:41",
          "from": "Zillow",
          "createdBy": "chambersmathis@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6c10580e59d58f539",
            "5a152bd635c192aabbb407f2"
          ],
          "price": 130606.61,
          "address": "276 Hooper Street",
          "city": "Norvelt",
          "zip": 60627,
          "BR": 2,
          "BA": 2,
          "monthlyInsurance": 827.73,
          "monthlyTaxes": 317.84,
          "monthlyPMI": 540.25,
          "monthlyHOA": 931.49,
          "closingCosts": 1074.11,
          "createDate": "2015-12-06T05:16:02",
          "updateDate": "2017-02-25T03:17:23",
          "from": "Zillow",
          "createdBy": "chambersmathis@zillactic.com"
        }
      ],
      "createDate": "2014-12-17T10:34:32",
      "updateDate": "2014-07-14T10:29:39",
      "createdBy": "chambersmathis@zillactic.com"
    },
    {
      "Id": "5a152bd67b12fee25c784053",
      "lastname": "Casey",
      "firstname": "Hardin",
      "status": "Actively Looking w/ Agent",
      "state": "TRACK",
      "email": "hardincasey@zillactic.com",
      "monthlyGross": 2456.63,
      "debt": 68486.53,
      "expenses": 1000,
      "targetedZipCodes": [
        60687,
        60685
      ],
      "totalCash": 6724.68,
      "downPayment": 4196.54,
      "mortgageType": "30Yr",
      "totalCashToClose": 6011.03,
      "emergencyFund": 3473.84,
      "creditScoreRange": "680-99",
      "trackingHistory": [
        {
          "Id": "5a152bd61bedba6124d66e69",
          "status": "SENT",
          "professionalId": "5a152bd6edf85369d678c43d",
          "createDate": "2017-10-14T04:17:57"
        },
        {
          "Id": "5a152bd67b2886ea0785ccf8",
          "status": "DENY",
          "professionalId": "5a152bd620bfba1101390c10",
          "createDate": "2014-12-18T11:07:55"
        },
        {
          "Id": "5a152bd69f8729c6600a7d09",
          "status": "CONFIRM",
          "professionalId": "5a152bd6d2e75d22dfa27cdf",
          "createDate": "2014-01-14T05:57:13"
        },
        {
          "Id": "5a152bd60206dfff02a97614",
          "status": "TRACK",
          "professionalId": "5a152bd6d30b196a1b16c199",
          "createDate": "2017-06-10T08:50:44"
        }
      ],
      "need": {
        "priceMin": 472720.77,
        "priceMax": 462706.32,
        "BA": 4,
        "BR": 1,
        "needs": [
          "Deck",
          "Close to L",
          "Balcony",
          "Close to L",
          "Granite Counter Tops",
          "Balcony",
          "Garage"
        ]
      },
      "wants": [
        "Rooftop Balcony",
        "Pool",
        "Rooftop Balcony"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd60c15073d28bd4d12",
            "5a152bd66c7b78d6ed2e5855",
            "5a152bd661007bdf8f41c4c7",
            "5a152bd677e03c33df12514b",
            "5a152bd61b47718dd9d478b4",
            "5a152bd6746a168b995145eb",
            "5a152bd6ede09f5bfc15280e"
          ],
          "price": 931941.34,
          "address": "787 Glenwood Road",
          "city": "Sunriver",
          "zip": 60643,
          "BR": 2,
          "BA": 2,
          "monthlyInsurance": 753.34,
          "monthlyTaxes": 923.64,
          "monthlyPMI": 779.87,
          "monthlyHOA": 654.07,
          "closingCosts": 1957.88,
          "createDate": "2014-08-10T11:42:08",
          "updateDate": "2016-05-04T02:34:49",
          "from": "HomeBloq",
          "createdBy": "hardincasey@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd66ea9fd86c132c458"
          ],
          "price": 732385.26,
          "address": "675 Cedar Street",
          "city": "Gilmore",
          "zip": 60671,
          "BR": 4,
          "BA": 4,
          "monthlyInsurance": 245.1,
          "monthlyTaxes": 608.88,
          "monthlyPMI": 543.53,
          "monthlyHOA": 327.74,
          "closingCosts": 1587.21,
          "createDate": "2017-04-18T12:32:43",
          "updateDate": "2014-01-03T08:49:16",
          "from": "Zillow",
          "createdBy": "hardincasey@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6e5efc9105c434cf0",
            "5a152bd60d9eeabdf174a216",
            "5a152bd6de4163282cd5debf",
            "5a152bd6dfd60ed1a95f5a9c"
          ],
          "price": 825759.77,
          "address": "670 Dahlgreen Place",
          "city": "Enetai",
          "zip": 60618,
          "BR": 1,
          "BA": 2,
          "monthlyInsurance": 496.51,
          "monthlyTaxes": 451.99,
          "monthlyPMI": 467.75,
          "monthlyHOA": 468.96,
          "closingCosts": 1133.17,
          "createDate": "2014-09-30T09:03:43",
          "updateDate": "2015-01-10T04:55:43",
          "from": "MLS",
          "createdBy": "hardincasey@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6cb59f419efa74773",
            "5a152bd671d7a44bd7159389",
            "5a152bd660ca5e8f1df984ef"
          ],
          "price": 171857.98,
          "address": "109 Mayfair Drive",
          "city": "Fivepointville",
          "zip": 60634,
          "BR": 3,
          "BA": 2,
          "monthlyInsurance": 768.68,
          "monthlyTaxes": 250.45,
          "monthlyPMI": 437.2,
          "monthlyHOA": 231.25,
          "closingCosts": 1191.53,
          "createDate": "2017-08-15T11:56:47",
          "updateDate": "2015-12-31T02:39:56",
          "from": "Zillow",
          "createdBy": "hardincasey@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6e022bc1de11ecf97",
            "5a152bd6d16bc8f63e5d254a",
            "5a152bd6ea9056fe45484732"
          ],
          "price": 515232.66,
          "address": "194 Suydam Place",
          "city": "Topaz",
          "zip": 60623,
          "BR": 4,
          "BA": 3,
          "monthlyInsurance": 883.08,
          "monthlyTaxes": 556.24,
          "monthlyPMI": 408.6,
          "monthlyHOA": 835.02,
          "closingCosts": 1248.98,
          "createDate": "2015-07-22T04:48:13",
          "updateDate": "2016-08-04T12:37:18",
          "from": "MLS",
          "createdBy": "hardincasey@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd63130d57b792de1f1",
            "5a152bd612ceb73a5106f15f",
            "5a152bd6e5e7722078ecac05",
            "5a152bd662e68fa4b514d7f0"
          ],
          "price": 525036.28,
          "address": "657 Tompkins Place",
          "city": "Rockbridge",
          "zip": 60606,
          "BR": 3,
          "BA": 2,
          "monthlyInsurance": 273.27,
          "monthlyTaxes": 857.75,
          "monthlyPMI": 454.58,
          "monthlyHOA": 761.87,
          "closingCosts": 1612.77,
          "createDate": "2017-03-08T12:12:48",
          "updateDate": "2015-06-26T12:19:00",
          "from": "Redfin",
          "createdBy": "hardincasey@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd673e5d67cd75696a8",
            "5a152bd6bf8e083dbed917c1"
          ],
          "price": 797575.19,
          "address": "516 Debevoise Street",
          "city": "Smeltertown",
          "zip": 60651,
          "BR": 3,
          "BA": 1,
          "monthlyInsurance": 228.05,
          "monthlyTaxes": 367.8,
          "monthlyPMI": 220.42,
          "monthlyHOA": 859.18,
          "closingCosts": 1510.19,
          "createDate": "2017-05-08T09:58:30",
          "updateDate": "2017-10-31T07:31:04",
          "from": "MLS",
          "createdBy": "hardincasey@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd60a06020f34e8f457",
            "5a152bd64fdf6b6cc0cbc2bc",
            "5a152bd6152d1c00c7ced858"
          ],
          "price": 104487.04,
          "address": "202 Tapscott Street",
          "city": "Gambrills",
          "zip": 60623,
          "BR": 3,
          "BA": 3,
          "monthlyInsurance": 759.73,
          "monthlyTaxes": 852.25,
          "monthlyPMI": 934.37,
          "monthlyHOA": 610.41,
          "closingCosts": 1293.97,
          "createDate": "2014-02-14T11:04:07",
          "updateDate": "2016-11-25T07:40:35",
          "from": "Redfin",
          "createdBy": "hardincasey@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd60745360963474824",
            "5a152bd6ad2bcf6764f3c7e2",
            "5a152bd6232050bdb5bd05fe",
            "5a152bd690971b15925878a2",
            "5a152bd67962a153126923d6"
          ],
          "price": 652175.92,
          "address": "552 Seaview Court",
          "city": "Shindler",
          "zip": 60668,
          "BR": 1,
          "BA": 3,
          "monthlyInsurance": 550.86,
          "monthlyTaxes": 940.41,
          "monthlyPMI": 262.52,
          "monthlyHOA": 230.92,
          "closingCosts": 1075.31,
          "createDate": "2015-01-09T09:18:16",
          "updateDate": "2016-05-20T02:44:22",
          "from": "Redfin",
          "createdBy": "hardincasey@zillactic.com"
        }
      ],
      "createDate": "2016-08-31T11:26:52",
      "updateDate": "2017-08-01T11:29:06",
      "createdBy": "hardincasey@zillactic.com"
    },
    {
      "Id": "5a152bd6af7e939ce8df7d90",
      "lastname": "Mullins",
      "firstname": "Giles",
      "status": "Just Starting Out",
      "state": "CONFIRM",
      "email": "gilesmullins@zillactic.com",
      "monthlyGross": 1488.49,
      "debt": 23601.99,
      "expenses": 1000,
      "targetedZipCodes": [
        60629,
        60616,
        60629,
        60685,
        60680,
        60671,
        60654
      ],
      "totalCash": 5873.58,
      "downPayment": 8095.18,
      "mortgageType": "5Yr",
      "totalCashToClose": 6900.5,
      "emergencyFund": 2847.07,
      "creditScoreRange": "680-99",
      "trackingHistory": [
        {
          "Id": "5a152bd63b11265fb897aece",
          "status": "SENT",
          "professionalId": "5a152bd6a65e2bf6d717b7b9",
          "createDate": "2015-06-28T04:44:57"
        },
        {
          "Id": "5a152bd6cf2cfbe144371d49",
          "status": "DENY",
          "professionalId": "5a152bd63f05f3937ae11eaa",
          "createDate": "2014-09-25T04:38:25"
        },
        {
          "Id": "5a152bd62a6e0a7468e2bead",
          "status": "CONFIRM",
          "professionalId": "5a152bd64b2e5e2838b7dea4",
          "createDate": "2014-07-12T06:49:12"
        },
        {
          "Id": "5a152bd696436e7c2d9435e7",
          "status": "TRACK",
          "professionalId": "5a152bd651cdb34be7c44c6d",
          "createDate": "2015-02-07T04:01:41"
        }
      ],
      "need": {
        "priceMin": 449367.36,
        "priceMax": 548429.7,
        "BA": 2,
        "BR": 3,
        "needs": [
          "Granite Counter Tops",
          "Deck",
          "Deck",
          "Granite Counter Tops",
          "Deck",
          "Deck",
          "Garage",
          "Close to L"
        ]
      },
      "wants": [
        "Wired for Sound",
        "Rooftop Balcony",
        "Pool"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd6c33f45970f8b6f49",
            "5a152bd64767818cd76a8916",
            "5a152bd6be5ea18254839c52",
            "5a152bd6c0c87691be4d799a"
          ],
          "price": 72477.6,
          "address": "177 Roosevelt Court",
          "city": "Websterville",
          "zip": 60645,
          "BR": 1,
          "BA": 2,
          "monthlyInsurance": 343.85,
          "monthlyTaxes": 201.35,
          "monthlyPMI": 652.16,
          "monthlyHOA": 343.22,
          "closingCosts": 1489.42,
          "createDate": "2016-02-15T08:11:25",
          "updateDate": "2016-07-05T07:11:18",
          "from": "Redfin",
          "createdBy": "gilesmullins@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd690e0beb95b82a11a",
            "5a152bd699a385438bd71f8b"
          ],
          "price": 677474.54,
          "address": "657 Ridge Court",
          "city": "Interlochen",
          "zip": 60651,
          "BR": 4,
          "BA": 2,
          "monthlyInsurance": 241.74,
          "monthlyTaxes": 604.72,
          "monthlyPMI": 911.87,
          "monthlyHOA": 202.24,
          "closingCosts": 1435.06,
          "createDate": "2014-06-08T02:07:26",
          "updateDate": "2015-09-25T05:52:43",
          "from": "MLS",
          "createdBy": "gilesmullins@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6b1603cdad13d9982",
            "5a152bd63d9bfe260de3a537",
            "5a152bd6588390465d2bbb18",
            "5a152bd601b4071c560946e7",
            "5a152bd65d1c33dfba6fdb61"
          ],
          "price": 429636,
          "address": "578 Summit Street",
          "city": "Maplewood",
          "zip": 60685,
          "BR": 1,
          "BA": 2,
          "monthlyInsurance": 804.89,
          "monthlyTaxes": 850.92,
          "monthlyPMI": 610.79,
          "monthlyHOA": 990.44,
          "closingCosts": 1698.87,
          "createDate": "2016-05-11T11:13:30",
          "updateDate": "2014-03-31T02:09:39",
          "from": "Redfin",
          "createdBy": "gilesmullins@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6521771334d59b8a5",
            "5a152bd66dbd980fa5ad535b",
            "5a152bd637b1079c52fbbd91",
            "5a152bd696094f9fe5a359f5",
            "5a152bd69ccf2f2c9b17d025",
            "5a152bd633c9007c944de5b9",
            "5a152bd652bdd40d13acda36"
          ],
          "price": 145363.47,
          "address": "583 Lloyd Court",
          "city": "Marbury",
          "zip": 60628,
          "BR": 3,
          "BA": 1,
          "monthlyInsurance": 892.86,
          "monthlyTaxes": 413.83,
          "monthlyPMI": 361.54,
          "monthlyHOA": 580.6,
          "closingCosts": 1402.12,
          "createDate": "2014-09-21T07:45:24",
          "updateDate": "2016-01-14T11:30:01",
          "from": "Redfin",
          "createdBy": "gilesmullins@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd63d8d5bc76d81f788",
            "5a152bd6d38f355c6736963b",
            "5a152bd6a56a9f41d50997ce",
            "5a152bd6f391727500420638"
          ],
          "price": 882767.59,
          "address": "431 Crescent Street",
          "city": "Choctaw",
          "zip": 60671,
          "BR": 2,
          "BA": 3,
          "monthlyInsurance": 549.72,
          "monthlyTaxes": 791.07,
          "monthlyPMI": 880.3,
          "monthlyHOA": 669.77,
          "closingCosts": 1589.27,
          "createDate": "2015-11-22T09:08:22",
          "updateDate": "2014-06-21T09:58:23",
          "from": "Redfin",
          "createdBy": "gilesmullins@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd687d1399c626e3673",
            "5a152bd6b5800b9bd5cdccac"
          ],
          "price": 869677.83,
          "address": "184 Ridgewood Place",
          "city": "Kansas",
          "zip": 60662,
          "BR": 1,
          "BA": 4,
          "monthlyInsurance": 948.51,
          "monthlyTaxes": 428.89,
          "monthlyPMI": 761.49,
          "monthlyHOA": 784.63,
          "closingCosts": 1083.72,
          "createDate": "2017-05-22T07:13:04",
          "updateDate": "2015-08-07T11:47:58",
          "from": "Redfin",
          "createdBy": "gilesmullins@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd60b60f1f5b255dd7a",
            "5a152bd69e235502ef89ddfb",
            "5a152bd68c9690e9d2ab26fc"
          ],
          "price": 6793.38,
          "address": "903 Hinckley Place",
          "city": "Yardville",
          "zip": 60634,
          "BR": 1,
          "BA": 1,
          "monthlyInsurance": 855.59,
          "monthlyTaxes": 420.21,
          "monthlyPMI": 200.28,
          "monthlyHOA": 569.63,
          "closingCosts": 1278.75,
          "createDate": "2015-04-29T05:52:40",
          "updateDate": "2014-10-25T07:22:06",
          "from": "MLS",
          "createdBy": "gilesmullins@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6fe5751f65decead7"
          ],
          "price": 170866.5,
          "address": "864 Kings Place",
          "city": "Brandywine",
          "zip": 60623,
          "BR": 1,
          "BA": 1,
          "monthlyInsurance": 752.9,
          "monthlyTaxes": 298.41,
          "monthlyPMI": 265.63,
          "monthlyHOA": 748.2,
          "closingCosts": 1769.53,
          "createDate": "2017-08-06T02:52:28",
          "updateDate": "2015-05-24T01:02:40",
          "from": "MLS",
          "createdBy": "gilesmullins@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd68cb834a062dce291",
            "5a152bd69f67cdf9f66341b5"
          ],
          "price": 473025.96,
          "address": "177 Lyme Avenue",
          "city": "Dowling",
          "zip": 60631,
          "BR": 4,
          "BA": 3,
          "monthlyInsurance": 575.3,
          "monthlyTaxes": 959.36,
          "monthlyPMI": 403.8,
          "monthlyHOA": 326.1,
          "closingCosts": 1993.62,
          "createDate": "2014-04-23T03:07:06",
          "updateDate": "2017-03-26T03:37:02",
          "from": "HomeBloq",
          "createdBy": "gilesmullins@zillactic.com"
        }
      ],
      "createDate": "2014-10-09T10:44:53",
      "updateDate": "2016-01-12T07:23:53",
      "createdBy": "gilesmullins@zillactic.com"
    },
    {
      "Id": "5a152bd6694972a512415fb5",
      "lastname": "Ferrell",
      "firstname": "Sheila",
      "status": "Looking without Agent",
      "state": "DENY",
      "email": "sheilaferrell@zillactic.com",
      "monthlyGross": 3371.22,
      "debt": 92511.1,
      "expenses": 1000,
      "targetedZipCodes": [
        60645,
        60653
      ],
      "totalCash": 5469.52,
      "downPayment": 8990.6,
      "mortgageType": "5Yr",
      "totalCashToClose": 7555.93,
      "emergencyFund": 1912.28,
      "creditScoreRange": "720-739",
      "trackingHistory": [
        {
          "Id": "5a152bd641914c2e0f67129e",
          "status": "SENT",
          "professionalId": "5a152bd682bf2a124f7a6713",
          "createDate": "2016-08-09T01:48:52"
        },
        {
          "Id": "5a152bd6eeed48b9a034a626",
          "status": "DENY",
          "professionalId": "5a152bd6269b20b119f9ac38",
          "createDate": "2017-07-12T07:24:30"
        },
        {
          "Id": "5a152bd6a4f753adc4fee3cb",
          "status": "CONFIRM",
          "professionalId": "5a152bd6c057ab760e66981f",
          "createDate": "2014-02-14T11:58:33"
        },
        {
          "Id": "5a152bd65c3d79a8ea3b8671",
          "status": "TRACK",
          "professionalId": "5a152bd687de2f85b758982f",
          "createDate": "2017-04-08T12:16:55"
        }
      ],
      "need": {
        "priceMin": 490496.83,
        "priceMax": 442198.95,
        "BA": 1,
        "BR": 2,
        "needs": [
          "Balcony",
          "Granite Counter Tops",
          "Granite Counter Tops",
          "Close to L",
          "Deck",
          "Granite Counter Tops"
        ]
      },
      "wants": [
        "Pool",
        "Rooftop Balcony",
        "Pool"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd642b503c58968035c",
            "5a152bd6ec90f906357a606b",
            "5a152bd69c3b27b5ec28a512"
          ],
          "price": 114626.99,
          "address": "770 Guider Avenue",
          "city": "Waumandee",
          "zip": 60631,
          "BR": 4,
          "BA": 4,
          "monthlyInsurance": 740.53,
          "monthlyTaxes": 974.2,
          "monthlyPMI": 518.27,
          "monthlyHOA": 938.61,
          "closingCosts": 1470.66,
          "createDate": "2014-09-12T01:26:41",
          "updateDate": "2014-09-24T04:20:31",
          "from": "Zillow",
          "createdBy": "sheilaferrell@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd667c7ad202cdd1c9e",
            "5a152bd656b75f8c2735bc6b",
            "5a152bd654c8487dec48d354",
            "5a152bd6069629f9aa9bb83b",
            "5a152bd62f3ce15de006c347",
            "5a152bd6e952f0e15b177c0d",
            "5a152bd66ad915a6f94d1971",
            "5a152bd634d35dd3dfe95e50"
          ],
          "price": 191345.23,
          "address": "264 Hampton Avenue",
          "city": "Dixie",
          "zip": 60608,
          "BR": 3,
          "BA": 2,
          "monthlyInsurance": 908.32,
          "monthlyTaxes": 396.74,
          "monthlyPMI": 258.13,
          "monthlyHOA": 693,
          "closingCosts": 1254.13,
          "createDate": "2016-11-27T06:53:31",
          "updateDate": "2014-08-09T02:43:56",
          "from": "MLS",
          "createdBy": "sheilaferrell@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6e278a4e7d79b9716"
          ],
          "price": 417884.21,
          "address": "869 Argyle Road",
          "city": "Nelson",
          "zip": 60619,
          "BR": 3,
          "BA": 2,
          "monthlyInsurance": 438.04,
          "monthlyTaxes": 729.47,
          "monthlyPMI": 813.32,
          "monthlyHOA": 732.62,
          "closingCosts": 1345.92,
          "createDate": "2016-07-27T12:14:05",
          "updateDate": "2015-04-09T01:07:37",
          "from": "Redfin",
          "createdBy": "sheilaferrell@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6ab2b011c1c4c8432",
            "5a152bd6c71296c4d0bfdc4b",
            "5a152bd6bb7ee1053dea3fd3",
            "5a152bd6c623440f5625f54f",
            "5a152bd6f0176f87cd8e4259"
          ],
          "price": 481851.6,
          "address": "484 Butler Place",
          "city": "Summerfield",
          "zip": 60684,
          "BR": 4,
          "BA": 4,
          "monthlyInsurance": 314.51,
          "monthlyTaxes": 747.76,
          "monthlyPMI": 337.32,
          "monthlyHOA": 226.96,
          "closingCosts": 1855.78,
          "createDate": "2016-10-19T12:24:36",
          "updateDate": "2014-06-20T01:31:03",
          "from": "Zillow",
          "createdBy": "sheilaferrell@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd65cb5af0a3fab4276",
            "5a152bd647e833d4160430e1",
            "5a152bd6728968dab6a277ce",
            "5a152bd6a5393415ca221d48",
            "5a152bd678cc499515fecfe6",
            "5a152bd6ea76184ef0371596"
          ],
          "price": 110188.35,
          "address": "823 Elliott Walk",
          "city": "Winston",
          "zip": 60669,
          "BR": 3,
          "BA": 4,
          "monthlyInsurance": 688.84,
          "monthlyTaxes": 941.73,
          "monthlyPMI": 815.04,
          "monthlyHOA": 409.43,
          "closingCosts": 1361.52,
          "createDate": "2016-02-05T09:23:45",
          "updateDate": "2015-07-03T08:57:34",
          "from": "HomeBloq",
          "createdBy": "sheilaferrell@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd66390a14121a4e0a8"
          ],
          "price": 559765.44,
          "address": "609 Scholes Street",
          "city": "Alleghenyville",
          "zip": 60654,
          "BR": 3,
          "BA": 3,
          "monthlyInsurance": 691.77,
          "monthlyTaxes": 789.67,
          "monthlyPMI": 205.15,
          "monthlyHOA": 493.77,
          "closingCosts": 1507.3,
          "createDate": "2014-06-28T03:14:27",
          "updateDate": "2014-02-19T01:08:33",
          "from": "MLS",
          "createdBy": "sheilaferrell@zillactic.com"
        }
      ],
      "createDate": "2015-08-25T05:56:49",
      "updateDate": "2016-03-22T08:35:19",
      "createdBy": "sheilaferrell@zillactic.com"
    },
    {
      "Id": "5a152bd6f5d2df52f72790e0",
      "lastname": "Kirk",
      "firstname": "Sharp",
      "status": "Actively Looking",
      "state": "DENY",
      "email": "sharpkirk@zillactic.com",
      "monthlyGross": 3211.96,
      "debt": 7483.05,
      "expenses": 1000,
      "targetedZipCodes": [
        60680,
        60608,
        60602,
        60636,
        60615
      ],
      "totalCash": 8283.24,
      "downPayment": 6418.69,
      "mortgageType": "30Yr",
      "totalCashToClose": 4084.16,
      "emergencyFund": 3490.99,
      "creditScoreRange": "760+",
      "trackingHistory": [
        {
          "Id": "5a152bd6980fff5bb1ec0bc3",
          "status": "SENT",
          "professionalId": "5a152bd6f24692215665754d",
          "createDate": "2017-05-25T07:17:26"
        },
        {
          "Id": "5a152bd6aed4dcf1ee582885",
          "status": "DENY",
          "professionalId": "5a152bd6f161ad6a439be09e",
          "createDate": "2015-06-19T10:10:23"
        },
        {
          "Id": "5a152bd63ccb8d79faff4ad4",
          "status": "CONFIRM",
          "professionalId": "5a152bd6623cde7fa2c74d04",
          "createDate": "2015-03-20T01:14:19"
        },
        {
          "Id": "5a152bd63a2746109b7e1518",
          "status": "TRACK",
          "professionalId": "5a152bd6875a43e35e477782",
          "createDate": "2016-10-12T03:13:56"
        }
      ],
      "need": {
        "priceMin": 444768.15,
        "priceMax": 96697.56,
        "BA": 2,
        "BR": 3,
        "needs": [
          "Balcony",
          "Garage",
          "Close to L",
          "Garage",
          "Granite Counter Tops"
        ]
      },
      "wants": [
        "Wired for Sound"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd6a43522e924a00d13",
            "5a152bd6bfcc48ce80687eb0"
          ],
          "price": 435895.15,
          "address": "440 Durland Place",
          "city": "Graball",
          "zip": 60648,
          "BR": 2,
          "BA": 3,
          "monthlyInsurance": 794.57,
          "monthlyTaxes": 329.05,
          "monthlyPMI": 232.64,
          "monthlyHOA": 857.77,
          "closingCosts": 1052.65,
          "createDate": "2015-02-18T06:58:11",
          "updateDate": "2017-10-23T12:11:24",
          "from": "Zillow",
          "createdBy": "sharpkirk@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd63e684440ea13d26c",
            "5a152bd6d568b660e618e32b",
            "5a152bd68239626687ddab01",
            "5a152bd6509cad8da95ed877",
            "5a152bd6015a973cc8f2098a",
            "5a152bd62a30c95e37961b81",
            "5a152bd6bc7ee5ffe6d08510"
          ],
          "price": 541270.35,
          "address": "933 Gaylord Drive",
          "city": "Cazadero",
          "zip": 60685,
          "BR": 1,
          "BA": 4,
          "monthlyInsurance": 719.73,
          "monthlyTaxes": 761.49,
          "monthlyPMI": 369.93,
          "monthlyHOA": 336.31,
          "closingCosts": 1463.29,
          "createDate": "2017-01-17T10:51:27",
          "updateDate": "2015-04-19T07:18:36",
          "from": "MLS",
          "createdBy": "sharpkirk@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd639a273370025ce92",
            "5a152bd6ad4b2544868cc255",
            "5a152bd637e689d1032573c4"
          ],
          "price": 351588.41,
          "address": "781 Merit Court",
          "city": "Bladensburg",
          "zip": 60608,
          "BR": 4,
          "BA": 3,
          "monthlyInsurance": 398.28,
          "monthlyTaxes": 810.28,
          "monthlyPMI": 992.6,
          "monthlyHOA": 539.72,
          "closingCosts": 1458.88,
          "createDate": "2015-09-02T12:00:35",
          "updateDate": "2017-10-28T02:47:56",
          "from": "Redfin",
          "createdBy": "sharpkirk@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6f30b744c5b4f9686",
            "5a152bd69c845f6f4a8ee1ea",
            "5a152bd652289b2a57c238e3",
            "5a152bd6f4e0560cfdd15d93"
          ],
          "price": 974072.96,
          "address": "591 Navy Street",
          "city": "Fedora",
          "zip": 60613,
          "BR": 1,
          "BA": 3,
          "monthlyInsurance": 239.92,
          "monthlyTaxes": 842.06,
          "monthlyPMI": 239.79,
          "monthlyHOA": 570.87,
          "closingCosts": 1244.82,
          "createDate": "2017-06-22T01:34:56",
          "updateDate": "2017-02-12T11:03:23",
          "from": "HomeBloq",
          "createdBy": "sharpkirk@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd67796635d8e16a31b",
            "5a152bd6fc092bc999797058",
            "5a152bd6ebbfc1dd06f88db4",
            "5a152bd6af6d9142432c1c9c"
          ],
          "price": 386353.09,
          "address": "774 Main Street",
          "city": "Maybell",
          "zip": 60645,
          "BR": 1,
          "BA": 2,
          "monthlyInsurance": 845.02,
          "monthlyTaxes": 530.7,
          "monthlyPMI": 391.48,
          "monthlyHOA": 578.78,
          "closingCosts": 1317.55,
          "createDate": "2016-01-07T12:46:18",
          "updateDate": "2016-06-25T04:41:09",
          "from": "Zillow",
          "createdBy": "sharpkirk@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6f05a65ccbbf4dd51",
            "5a152bd621a492b737cf5079",
            "5a152bd6c631e49167136c20",
            "5a152bd6451c40cf165d8fce",
            "5a152bd63b151244f582cb73",
            "5a152bd6eaef3a5f904ab8f0",
            "5a152bd673d9d9e981c8f7cb"
          ],
          "price": 907984.61,
          "address": "101 Louisiana Avenue",
          "city": "Keyport",
          "zip": 60668,
          "BR": 3,
          "BA": 2,
          "monthlyInsurance": 421.29,
          "monthlyTaxes": 441.47,
          "monthlyPMI": 472.06,
          "monthlyHOA": 218.5,
          "closingCosts": 1386.58,
          "createDate": "2016-02-16T02:22:52",
          "updateDate": "2017-09-01T11:20:13",
          "from": "HomeBloq",
          "createdBy": "sharpkirk@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd606718d7c21f05190",
            "5a152bd67a2e04945d905169",
            "5a152bd6d20958f5eae0e207"
          ],
          "price": 80979.53,
          "address": "986 Dupont Street",
          "city": "Snelling",
          "zip": 60601,
          "BR": 3,
          "BA": 3,
          "monthlyInsurance": 804.36,
          "monthlyTaxes": 316.36,
          "monthlyPMI": 929.78,
          "monthlyHOA": 911.69,
          "closingCosts": 1332.58,
          "createDate": "2015-04-05T01:21:38",
          "updateDate": "2014-08-19T02:38:47",
          "from": "MLS",
          "createdBy": "sharpkirk@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6519b13c17a7ab141",
            "5a152bd6ba2df7c302bbdd22"
          ],
          "price": 600203.63,
          "address": "371 Grace Court",
          "city": "Southview",
          "zip": 60647,
          "BR": 2,
          "BA": 2,
          "monthlyInsurance": 386.91,
          "monthlyTaxes": 524.94,
          "monthlyPMI": 506.96,
          "monthlyHOA": 945.45,
          "closingCosts": 1963.19,
          "createDate": "2016-05-04T06:14:30",
          "updateDate": "2015-02-23T09:03:33",
          "from": "Zillow",
          "createdBy": "sharpkirk@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd603922db654d8dd40",
            "5a152bd699d371ff57ba0eac",
            "5a152bd6928089a689ffc94a",
            "5a152bd6344659f8c6e25b8a"
          ],
          "price": 198864.95,
          "address": "586 Union Street",
          "city": "Ilchester",
          "zip": 60617,
          "BR": 1,
          "BA": 3,
          "monthlyInsurance": 551.4,
          "monthlyTaxes": 388.57,
          "monthlyPMI": 788.87,
          "monthlyHOA": 631.73,
          "closingCosts": 1950.44,
          "createDate": "2015-05-28T07:37:04",
          "updateDate": "2016-10-15T09:56:08",
          "from": "Zillow",
          "createdBy": "sharpkirk@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6aee5e800d1178883",
            "5a152bd63baee89c22201bd2"
          ],
          "price": 350964.98,
          "address": "370 Fenimore Street",
          "city": "Coyote",
          "zip": 60639,
          "BR": 1,
          "BA": 1,
          "monthlyInsurance": 673.28,
          "monthlyTaxes": 634.08,
          "monthlyPMI": 566.82,
          "monthlyHOA": 287.34,
          "closingCosts": 1718.51,
          "createDate": "2017-04-15T03:00:24",
          "updateDate": "2015-08-07T07:49:25",
          "from": "Redfin",
          "createdBy": "sharpkirk@zillactic.com"
        }
      ],
      "createDate": "2016-01-24T06:56:26",
      "updateDate": "2017-01-27T03:42:26",
      "createdBy": "sharpkirk@zillactic.com"
    },
    {
      "Id": "5a152bd64f972b7bca6a7be8",
      "lastname": "Ashley",
      "firstname": "Petty",
      "status": "Just Starting Out",
      "state": "DENY",
      "email": "pettyashley@zillactic.com",
      "monthlyGross": 2757.29,
      "debt": 29785.18,
      "expenses": 1000,
      "targetedZipCodes": [
        60681,
        60678,
        60619,
        60656,
        60664,
        60637,
        60638
      ],
      "totalCash": 5491.28,
      "downPayment": 5155.71,
      "mortgageType": "5Yr",
      "totalCashToClose": 9460.85,
      "emergencyFund": 1559.76,
      "creditScoreRange": "760+",
      "trackingHistory": [
        {
          "Id": "5a152bd694b0a088a4fd3840",
          "status": "SENT",
          "professionalId": "5a152bd6645c36400f716672",
          "createDate": "2017-05-14T09:12:07"
        },
        {
          "Id": "5a152bd6c891d8a032c72168",
          "status": "DENY",
          "professionalId": "5a152bd6237aeb9fe0d06630",
          "createDate": "2016-02-26T08:43:17"
        },
        {
          "Id": "5a152bd60136ee0d59f67c0a",
          "status": "CONFIRM",
          "professionalId": "5a152bd6edbedffc981ca1a7",
          "createDate": "2014-07-19T07:38:20"
        },
        {
          "Id": "5a152bd602386ee60c3fa65b",
          "status": "TRACK",
          "professionalId": "5a152bd670bddb3e886cc694",
          "createDate": "2016-03-20T12:14:17"
        }
      ],
      "need": {
        "priceMin": 602481.38,
        "priceMax": 358559.34,
        "BA": 3,
        "BR": 2,
        "needs": [
          "Close to L",
          "Balcony",
          "Balcony"
        ]
      },
      "wants": [
        "Wired for Sound",
        "Wired for Sound",
        "Rooftop Balcony"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd6082ba7ac1018555c",
            "5a152bd6926d2426645a52e9",
            "5a152bd6a8c3c796ad6aa20c"
          ],
          "price": 113359.31,
          "address": "108 Pierrepont Place",
          "city": "Gallina",
          "zip": 60681,
          "BR": 4,
          "BA": 1,
          "monthlyInsurance": 328.81,
          "monthlyTaxes": 212.01,
          "monthlyPMI": 307.76,
          "monthlyHOA": 339.05,
          "closingCosts": 1917.06,
          "createDate": "2014-08-09T04:24:43",
          "updateDate": "2015-01-13T11:35:21",
          "from": "MLS",
          "createdBy": "pettyashley@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd64c79057ff02f1200",
            "5a152bd692b8d680db1620b5",
            "5a152bd6ecad0d022316a538",
            "5a152bd680c5e8f7eb4178ce"
          ],
          "price": 350141.01,
          "address": "587 Ryder Avenue",
          "city": "Rose",
          "zip": 60685,
          "BR": 4,
          "BA": 3,
          "monthlyInsurance": 950.57,
          "monthlyTaxes": 973.84,
          "monthlyPMI": 463.04,
          "monthlyHOA": 266.98,
          "closingCosts": 1308.87,
          "createDate": "2014-03-15T05:19:58",
          "updateDate": "2014-06-14T07:40:35",
          "from": "Zillow",
          "createdBy": "pettyashley@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd66afcef33eb84980e",
            "5a152bd633e2e8d38df027a9",
            "5a152bd6b524033661db1bb2",
            "5a152bd67f84143135ae200a",
            "5a152bd64955daa0cb088f65",
            "5a152bd659475060a8d60c94",
            "5a152bd6cf8d388a60434ac2",
            "5a152bd6bff411fb215d0f2a"
          ],
          "price": 52989.54,
          "address": "437 Jefferson Street",
          "city": "Troy",
          "zip": 60639,
          "BR": 2,
          "BA": 1,
          "monthlyInsurance": 445.48,
          "monthlyTaxes": 852.54,
          "monthlyPMI": 537.7,
          "monthlyHOA": 984.37,
          "closingCosts": 1614.57,
          "createDate": "2014-05-27T04:16:28",
          "updateDate": "2014-04-22T08:45:26",
          "from": "HomeBloq",
          "createdBy": "pettyashley@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6150a2ee1deb96e29",
            "5a152bd639dab7d9e0ef6ecf",
            "5a152bd6f0a24d876731289b",
            "5a152bd6c25800286bf0aeba",
            "5a152bd6b30d5305e61b1764",
            "5a152bd607d4535fbbdaddd5",
            "5a152bd6a7f923d16fe07007",
            "5a152bd6bbd20d87428409b0"
          ],
          "price": 620960.43,
          "address": "529 Evans Street",
          "city": "Jeff",
          "zip": 60650,
          "BR": 1,
          "BA": 3,
          "monthlyInsurance": 430.8,
          "monthlyTaxes": 981.39,
          "monthlyPMI": 489.15,
          "monthlyHOA": 358.87,
          "closingCosts": 1541.36,
          "createDate": "2016-03-18T07:01:49",
          "updateDate": "2014-11-09T05:31:46",
          "from": "MLS",
          "createdBy": "pettyashley@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd66f3c598529e24ff6",
            "5a152bd6e3f76d7d340bc485",
            "5a152bd6459c0f3ddad644b5",
            "5a152bd64fc9d858614af402",
            "5a152bd60f1021bc83530253",
            "5a152bd67aa5ba818a8c8c2d",
            "5a152bd6788d1cd4582900c3"
          ],
          "price": 358315.66,
          "address": "460 Matthews Court",
          "city": "Fowlerville",
          "zip": 60628,
          "BR": 4,
          "BA": 2,
          "monthlyInsurance": 473.19,
          "monthlyTaxes": 349.03,
          "monthlyPMI": 203.26,
          "monthlyHOA": 753.42,
          "closingCosts": 1046.23,
          "createDate": "2015-12-16T03:04:41",
          "updateDate": "2014-10-09T09:13:09",
          "from": "Zillow",
          "createdBy": "pettyashley@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd63a1077bc2fb5a0c3",
            "5a152bd6deda2e110e09f224",
            "5a152bd6e250593b62e7de3e",
            "5a152bd6c8cd69b52d94b4a5",
            "5a152bd698a6c3a86410e8d5",
            "5a152bd6c4d9a6d2c917059f"
          ],
          "price": 151390.85,
          "address": "668 Surf Avenue",
          "city": "Sardis",
          "zip": 60613,
          "BR": 1,
          "BA": 3,
          "monthlyInsurance": 636.33,
          "monthlyTaxes": 965.22,
          "monthlyPMI": 677.66,
          "monthlyHOA": 555.37,
          "closingCosts": 1809.43,
          "createDate": "2016-03-13T11:19:36",
          "updateDate": "2017-04-10T09:45:40",
          "from": "Redfin",
          "createdBy": "pettyashley@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6fadc177aaac216a3",
            "5a152bd6c63903cd2f10282c"
          ],
          "price": 950775.18,
          "address": "221 Wilson Avenue",
          "city": "Templeton",
          "zip": 60626,
          "BR": 3,
          "BA": 1,
          "monthlyInsurance": 768.74,
          "monthlyTaxes": 692.26,
          "monthlyPMI": 329.48,
          "monthlyHOA": 330.25,
          "closingCosts": 1519.55,
          "createDate": "2016-06-22T08:07:30",
          "updateDate": "2014-07-07T01:11:38",
          "from": "HomeBloq",
          "createdBy": "pettyashley@zillactic.com"
        }
      ],
      "createDate": "2017-08-29T05:33:26",
      "updateDate": "2016-12-08T07:49:15",
      "createdBy": "pettyashley@zillactic.com"
    },
    {
      "Id": "5a152bd6c5f8b2d12147b853",
      "lastname": "Dixon",
      "firstname": "Harding",
      "status": "Actively Looking w/ Agent",
      "state": "SENT",
      "email": "hardingdixon@zillactic.com",
      "monthlyGross": 3548.02,
      "debt": 99326.98,
      "expenses": 1000,
      "targetedZipCodes": [
        60654,
        60665,
        60657
      ],
      "totalCash": 5702.37,
      "downPayment": 9136.97,
      "mortgageType": "15yr",
      "totalCashToClose": 5512.33,
      "emergencyFund": 1951.51,
      "creditScoreRange": "680-99",
      "trackingHistory": [
        {
          "Id": "5a152bd6218b42db051bd9c5",
          "status": "SENT",
          "professionalId": "5a152bd6df6f6773e94884f6",
          "createDate": "2014-01-31T09:41:02"
        },
        {
          "Id": "5a152bd68f5f6f7c2f32ea4b",
          "status": "DENY",
          "professionalId": "5a152bd60d66858037ab8a2d",
          "createDate": "2016-09-04T05:27:56"
        },
        {
          "Id": "5a152bd612937030583b74b7",
          "status": "CONFIRM",
          "professionalId": "5a152bd6fa965f3decf04ee6",
          "createDate": "2016-07-08T03:23:46"
        },
        {
          "Id": "5a152bd651d5928516fd66eb",
          "status": "TRACK",
          "professionalId": "5a152bd62705142ca5e0bcd0",
          "createDate": "2017-01-27T12:44:15"
        }
      ],
      "need": {
        "priceMin": 517909.88,
        "priceMax": 277715.64,
        "BA": 1,
        "BR": 2,
        "needs": [
          "Close to L",
          "Close to L",
          "Close to L"
        ]
      },
      "wants": [
        "Rooftop Balcony",
        "Rooftop Balcony",
        "Rooftop Balcony"
      ],
      "propertyList": [
        {
          "professionalTrackingList": [
            "5a152bd676801eca14068d00",
            "5a152bd6965f761a21307bfd",
            "5a152bd617ca69dcc8ecb3d8",
            "5a152bd60b8e6ad07b14c566"
          ],
          "price": 268438.63,
          "address": "974 Adler Place",
          "city": "Thermal",
          "zip": 60673,
          "BR": 1,
          "BA": 2,
          "monthlyInsurance": 726.08,
          "monthlyTaxes": 893.18,
          "monthlyPMI": 560.19,
          "monthlyHOA": 205.47,
          "closingCosts": 1320.06,
          "createDate": "2016-05-20T01:50:00",
          "updateDate": "2016-02-09T12:23:56",
          "from": "Redfin",
          "createdBy": "hardingdixon@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6a016ba6010f214a3",
            "5a152bd6ab9a771a4fe7e3ca",
            "5a152bd64ab6ac381f2de53d",
            "5a152bd64d63fece173ca8f8",
            "5a152bd62306a624c02ff2fd",
            "5a152bd64a045e039dff7702",
            "5a152bd6be8e823491613095",
            "5a152bd6a2d5b7b7ec7cf6d9"
          ],
          "price": 424371.48,
          "address": "860 Greenpoint Avenue",
          "city": "Williams",
          "zip": 60670,
          "BR": 1,
          "BA": 3,
          "monthlyInsurance": 558.13,
          "monthlyTaxes": 252.28,
          "monthlyPMI": 438.78,
          "monthlyHOA": 903.11,
          "closingCosts": 1288.93,
          "createDate": "2015-07-28T06:01:39",
          "updateDate": "2014-04-04T10:44:56",
          "from": "HomeBloq",
          "createdBy": "hardingdixon@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6844fcc7b4e702c70",
            "5a152bd66293fc22bd55ee78",
            "5a152bd63ae4a6c3967c07f0",
            "5a152bd6c2cafc70add5ca26",
            "5a152bd6ac6ae765e456ac20"
          ],
          "price": 685135.24,
          "address": "899 Sunnyside Avenue",
          "city": "Roulette",
          "zip": 60602,
          "BR": 3,
          "BA": 3,
          "monthlyInsurance": 825.54,
          "monthlyTaxes": 838.52,
          "monthlyPMI": 790.94,
          "monthlyHOA": 297.06,
          "closingCosts": 1832.45,
          "createDate": "2015-08-15T10:59:46",
          "updateDate": "2016-09-03T08:38:52",
          "from": "Zillow",
          "createdBy": "hardingdixon@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6cab65aedea2409ac",
            "5a152bd6b24b744c2523e230"
          ],
          "price": 953195.11,
          "address": "703 Matthews Place",
          "city": "Inkerman",
          "zip": 60638,
          "BR": 2,
          "BA": 1,
          "monthlyInsurance": 286.22,
          "monthlyTaxes": 632.08,
          "monthlyPMI": 234.58,
          "monthlyHOA": 317.24,
          "closingCosts": 1819.36,
          "createDate": "2016-07-25T05:00:45",
          "updateDate": "2016-12-27T10:22:57",
          "from": "Redfin",
          "createdBy": "hardingdixon@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6f7d032b759d50e4b",
            "5a152bd6622ebc4a3b3b6aa9",
            "5a152bd669ab0459c336d127",
            "5a152bd6bd019f425310ab10",
            "5a152bd64b72f65faff65946"
          ],
          "price": 997651.69,
          "address": "368 Middleton Street",
          "city": "Chumuckla",
          "zip": 60676,
          "BR": 4,
          "BA": 1,
          "monthlyInsurance": 594.64,
          "monthlyTaxes": 761,
          "monthlyPMI": 986.63,
          "monthlyHOA": 909.22,
          "closingCosts": 1236.95,
          "createDate": "2017-08-04T11:24:43",
          "updateDate": "2016-02-17T10:13:28",
          "from": "MLS",
          "createdBy": "hardingdixon@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd69b772a599e00e331",
            "5a152bd664f47a16cd2c5a33",
            "5a152bd61ab05a33f9820b28",
            "5a152bd6520f2344ed9c5362",
            "5a152bd6e316add0e8923cdb"
          ],
          "price": 524087.1,
          "address": "293 Brightwater Court",
          "city": "Gasquet",
          "zip": 60630,
          "BR": 1,
          "BA": 1,
          "monthlyInsurance": 821.05,
          "monthlyTaxes": 968.32,
          "monthlyPMI": 495.76,
          "monthlyHOA": 470.21,
          "closingCosts": 1730.48,
          "createDate": "2014-09-01T03:33:44",
          "updateDate": "2015-03-06T12:55:34",
          "from": "Redfin",
          "createdBy": "hardingdixon@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6cce87a9a349243f4"
          ],
          "price": 421264.82,
          "address": "741 Macon Street",
          "city": "Outlook",
          "zip": 60650,
          "BR": 1,
          "BA": 1,
          "monthlyInsurance": 883.95,
          "monthlyTaxes": 686.08,
          "monthlyPMI": 703.54,
          "monthlyHOA": 983.58,
          "closingCosts": 1922.47,
          "createDate": "2016-06-25T12:27:02",
          "updateDate": "2016-10-01T09:18:02",
          "from": "MLS",
          "createdBy": "hardingdixon@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6618a67605a00b7d4",
            "5a152bd6c2ec3bf659ef8ee6",
            "5a152bd6598c548c2a681d5f"
          ],
          "price": 235541.13,
          "address": "183 Caton Place",
          "city": "Avoca",
          "zip": 60684,
          "BR": 4,
          "BA": 2,
          "monthlyInsurance": 945.27,
          "monthlyTaxes": 244.16,
          "monthlyPMI": 364.4,
          "monthlyHOA": 206.68,
          "closingCosts": 1243.12,
          "createDate": "2016-04-16T02:05:38",
          "updateDate": "2017-05-16T10:00:57",
          "from": "Redfin",
          "createdBy": "hardingdixon@zillactic.com"
        },
        {
          "professionalTrackingList": [
            "5a152bd6662ba774a8561449",
            "5a152bd6d64cf2952f4c5c15",
            "5a152bd6579945d6d5821c70",
            "5a152bd672522a7ba7dbf4c4"
          ],
          "price": 687852.95,
          "address": "768 Wogan Terrace",
          "city": "Itmann",
          "zip": 60668,
          "BR": 4,
          "BA": 1,
          "monthlyInsurance": 641.6,
          "monthlyTaxes": 375.72,
          "monthlyPMI": 395.3,
          "monthlyHOA": 876.72,
          "closingCosts": 1440.84,
          "createDate": "2015-03-05T02:11:38",
          "updateDate": "2017-06-21T12:06:00",
          "from": "HomeBloq",
          "createdBy": "hardingdixon@zillactic.com"
        }
      ],
      "createDate": "2017-05-06T10:23:20",
      "updateDate": "2016-05-12T08:00:13",
      "createdBy": "hardingdixon@zillactic.com"
    }
  ]
}
