import fetchJsonp from "fetch-jsonp"
import {
    ThirtyFred,
    FifteenFred,
    ARMFred,
    ThirtyJumbo,
    FifteenJumbo,
    ThirtyVA,
    ThirtyFHA
} from "./fredRateData"
//This could be pulled from a DB that is updated ONCE daily, so we can limit the number of API calls we're making.
//Both API calls shoud return the value sorted descending by date, so we're just grabbing the first available value instead of specifying a start date

export const FredRates = () => {
    var series = [
        {
            key: "ThirtyFred",
            param: "PR_CON_30YFIXED_IR",
            data: ThirtyFred()
        },
        {
            key: "FifteenFred",
            param: "PR_CON_15YFIXED_IR",
            data: FifteenFred()
        },
        { key: "ARMFred", ARMFred: "PR_CON_51ARM_IR", data: ARMFred() },
        {
            key: "ThirtyJumbo",
            param: "PR_JUMBO_30YFIXED_IR",
            data: ThirtyJumbo()
        },
        {
            key: "FifteenJumbo",
            param: "PR_JUMBO_15YFIXED_IR",
            data: FifteenJumbo()
        },
        { key: "ThirtyVA", ThirtyVA: "PR_GOV_30YFIXEDVA_IR", data: ThirtyVA() },
        {
            key: "ThirtyFHA",
            param: "PR_GOV_30YFIXEDFHA_IR",
            data: ThirtyFHA()
        }
    ]

    // put your api key here
    // you can find it at http://quandl.com/account
    var apikey = "ZXmqApbnen1vtjcyga8B"
    var RatesJSON = new Object() //add the individual rates and names here

    // need to loop through for (i=0; i < series.length; i++) and set 0 below to i
    // and append the results to RatesJSON via RatesJSON[key] = value
    for (let i = 0; i < 7; i++) {
        var url = "https://www.quandl.com/api/v3/datasets/"
        var key = series[i.key] //ThirtyQuandl, etc.
        var value = series[i].param //PR_CON_30YFIXED_IR, etc.
        var database = "WFC/" + value
        var parameters = ".json?auth_token="

        // here we put each of those pieces together
        var fullUrl = url + database + parameters + apikey

        RatesJSON[series[i].key] = series[i].data[1]
        // getting the data -- instead of adding to HTML element, append to JSON
        /*
        fetchJsonp(fullUrl)
            .then(data => {
                RatesJSON[key] = data.data[0][1] //appending to RatesJSON
            })
            .catch(function(error) {
                RatesJSON[key] = error
            })*/
    }

    return RatesJSON
}
