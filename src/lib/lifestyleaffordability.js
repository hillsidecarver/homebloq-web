import { mortgageRates } from "mortgagerates"

export const lifesytleAffordability = (grossincome, expenses, debts, totalcash, hometype, homeuse, creditscore, mortgagetype, maxPITI) => {

    var MonthlyGross = Number(grossincome) / 12;
    var MonthlyGross = Number(MonthlyGross);
    var expenses = Number(expenses);
    var debts = Number(debts);
    var totalcash = Number(totalcash);

    //convert creditscores to numericals

    if (creditscore == '760+' || creditscore == '740-759') {
        creditscore = 1;
    }


    if (creditscore == '720-739' || creditscore == '700-719') {
        creditscore = 2;
    }

    if (creditscore == '680-99' || creditscore == '660-679') {
        creditscore = 3;
    }

    if (creditscore == '640-659') {
        creditscore = 4;
    }

    if (creditscore == '<640') {
        creditscore = 5;
    }


    RatesList = FredRates();

    let ThirtyFred = RatesList['ThirtyFred'];
    let FifteenFred = RatesList['FifteenFred'];
    let ARMFred = RatesList['ARMFred'];

    //These need to be added to the app drop down lists...just the VA and FHA Options, Jumbo is calculated based on mortgage amount
    let ThirtyJumbo = RatesList['ThirtyJumbo']
    let FifteenJumbo = RatesList['FifteenJumbo']
    let ThirtyVA = RatesList['ThirtyVA']
    let ThirtyFHA = RatesList['ThirtyFHA']


    if (hometype == 'Condo') {
        HOA = 300.0;
    }


    else {
        HOA = 0.0;
    }

    //Assumes 20% Down

    let MinDownPct = 20.0;

    /* ======================================================================
    CALCULATE THE INTEREST RATE

        15% Down: +0.125%
        10% Down: +0.25%
        *Adjustment made up to 25% down
        *Reconfigure in Valuation based on Amt Down/Value/Offer
        Townhouse: 0
        Condo: +0.125%
        Coop: 0
        Mobile, et al: 0
        Vacation: 0
        Investment: minimum 20% down and +0.5%

    Need to add spread for Jumbo loans, and set min down payment % to 20
    and max mortgage at 417,000 (conforming limit...)
    ======================================================================= */

    if (mortgagetype == '30 Year Fixed') {
        Yrs = 30;


        if (hometype == 'Single Family Home') {
            Rate = ThirtyFred;
        }

        else {
            if (MinDownPct < 25.0) {
                Rate = ThirtyFred + 0.125;
            }

            else {
                Rate = ThirtyFred;
            }
        }
    }

    if (mortgagetype == '15 Year Fixed') {
        Yrs = 15;

        if (hometype == 'Single Family Home') {
            Rate = FifteenFred;
        }

        else {

            if (MinDownPct < 25.0) {
                Rate = FifteenFred + 0.125;
            }

            else {
                Rate = FifteenFred;
            }
        }
    }

    if (mortgagetype == '5/1 Adjustable Rate') {
        Yrs = 30;

        if (hometype == 'Single Family Home') {
            Rate = ARMFred;
        }

        else {

            if (MinDownPct < 25.0) {
                Rate = ARMFred + 0.125;
            }

            else {
                Rate = ARMFred;
            }
        }
    }

    if (mortgagetype == '30 Year VA') {
        Yrs = 30;

        if (hometype == 'Single Family Home') {
            Rate = ThirtyVA;
        }

        else {
            Rate = ThirtyVA + 0.125;
        }

        MinDownPct = 0.0;
    }


    //Change the rate by adding the FHA Rate
    if (mortgagetype == '30 Year FHA Loan') {
        Yrs = 30;

        if (hometype == 'Single Family Home') {
            Rate = ThirtyFHA;
        }

        else {

            if (MinDownPct < 25.0) {
                Rate = ThirtyFHA;
            }

            else {
                Rate = ThirtyFHA;
            }
        }

        //change down payment percentage to 3.5%
        let MinDownPct = 3.50;
    }

    if (mortgagetype == '30 Year FHA Loan') {
        annualpremium = 0.85;
        upfront = 0.0175;
    }

    if (mortgagetype == '30 Year VA') {
        annualpremium = 0.0;
        upfront = 2.15;
    }

    else {
        annualpremium = 0.0;
        upfront = 0.0;
    }


    if (creditscore == 1 || creditscore == 2) {
        Rate = Rate;
    }

    if (creditscore == 3) {
        Rate = Rate + 0.125;
    }

    if (creditscore == 4) {
        Rate = Rate + 0.25;
    }

    if (creditscore >= 5) {
        Rate = Rate + 0.5;
    }

    if (homeuse == 'Investment Property') {
        Rate = Rate + 0.50;
    }

    if (MinDownPct <20.0 && MinDownPct >= 15.0) {
        Rate = Rate + 0.125;
    }

    if (MinDownPct <15.0 && MinDownPct >= 10.0) {
        Rate = Rate + 0.25;
    }

    if (MinDownPct < 10.0 && mortgagetype != '30 Year FHA Loan') {
        Rate = Rate + 0.375;
    }

    let I = Rate/100;

    /* ======================================================================
    Max Affordability based on Income and Expenses

    Use Max Monthly PITI and assumptions about Insurance, Taxes, PMI, HOA
    and solve for maximum mortgage and home price as well as total cash
    to close and 6 (or 3 or other) living expenses = rainy day fund

    Assumptions:
        Rainy Day Fund = 6 Months Living Expenses
        20% Down Payment
        Condo HOA of $300 (average of 200-400/month)

    Should show the following scenarios:
        20%, 15%, 10%, 5% Down (subject to conventional mortgage max)
        6, 5, 4, 3 Month Rainy Day Fund

        Then let user choose which they prefer?
    ====================================================================== */

//    TDI = (0.36 * MonthlyGross) - debts
//    PITI = (0.28 * MonthlyGross)
    let MaxPITI = maxPITI

    let DP = MinDownPct / 100  //assumes 20% Down as base case;
    let M = Yrs                //30 || 15 years based on mortgage product;
    let N = 12                 //assumes monthly payments;
    let Tax = 0.02             //assumes taxes are 2% of home value per year;
    let Ins = 0.0017           //change this to min(Home Price/1000 * 3.50,1000)/12;
    let CC = 0.02              //assumes closing costs of 2% of home value;


    if (DP < 0.20) {          //assumes PMI as 0.25% of home value per year
        PMI = 0.0025;


        if (mortgagetype == '30 Year FHA Loan') {
            PMI = annualpremium / 100;
        }
    }

    else {
        PMI = 0;
    }

    let R = 3                  //assumes 3 months living expenses

    /*
    Methodology:
    Rainy Day Fund = 6 * (PnI + Taxes + Ins + TotalExpenses + TotalDebts + HOA)
        PnI = (i/n / (1-(1+i/n)^-m*n)) * Mortgage/(1-DP)
        Taxes = 0.02 * Mortgage/(1-DP) / 12
        Ins = 0.0017 * Mortgage/(1-DP) / 12
        Total Expenses = Known
        Total Debts = Known
        HOA = Known

        Mortgage / (1-DP) = Home Value (i.e. Mortgage / (1-0.20) if 20% is down payment percentage)

    Total Cash Req'd = 6*RainyDayFund + Closing Cost + DownPayment
        Total Cash Req'd = Total Cash on Hand
        Closing Costs = 0.02 * Mortgage / (1 - DP)
        DownPayment = 0.20 * Mortgage / (1 - DP)

    Solve for Mortgage and divide by 1 - Down Payment Percent = Home
    DP = Down Payment Percent = 20% for base case
     */

    let Divisor = 12 * (1 - DP);


    let ClosingCosts =  CC / (1 - DP);
    let DownPayment = 0.20 / (1 - DP);


    let Taxes = Tax / Divisor;
    let Insurance = Ins / Divisor;
    let PnI = (I / 12) / (1 - (1 + I/12)**-(M*N));
    let MIns = PMI / Divisor;
    let HOA = HOA;
    let Denominator = (R*Taxes + R*Insurance + R*MIns + R*PnI + ClosingCosts + DownPayment);
    let Mortgage = (totalcash - R*HOA - R*expenses - R*debts) / Denominator;
    let HomeValueNow = Mortgage / (1-DP);

    //calculate PMT as separate calculation
    let x = Math.pow(1 + (I / 12), N*M)
    let MonthlyPnI = ((I/12 * (0 + x * Mortgage))/(-1 + x));

    let MonthlyTaxes = Tax*HomeValueNow/12;
    let MonthlyInsurance = Ins*HomeValueNow/12;
    let MonthlyPMI = PMI*HomeValueNow/12;
    let MonthlyPITI = MonthlyPnI + MonthlyTaxes + MonthlyInsurance + MonthlyPMI;
    let ClosingCosts = HomeValueNow * 0.02 + (HomeValueNow * upfront);
    let DownPayment = HomeValueNow - Mortgage;

    let RainyDayFund = R*(MonthlyPnI + MonthlyTaxes + MonthlyInsurance + MonthlyPMI+HOA+expenses+debts);
    let TotalCashNeeded = RainyDayFund + ClosingCosts + DownPayment;
    let Difference = totalcash - TotalCashNeeded;
    let CashToClose = DownPayment + ClosingCosts;

    let PITIToIncome = (MonthlyPITI / MonthlyGross) * 100;
    let DebtToIncome = ((MonthlyPITI + debts) / MonthlyGross) * 100;

    let AffordNow = {};

    // AffordNow['Mortgage'] = int(Mortgage)
    // AffordNow['MaxHome'] = int(HomeValueNow)
    // AffordNow['PnI'] = int(MonthlyPnI)
    // AffordNow['MonthlyTaxes'] = int(MonthlyTaxes)
    // AffordNow['MonthlyInsurance'] = int(MonthlyInsurance)
    // AffordNow['PMI'] = int(MonthlyPMI)
    // AffordNow['PITI'] = int(MonthlyPITI)
    // AffordNow['ClosingCosts'] = int(ClosingCosts)
    // AffordNow['DownPayment'] = int(DownPayment)
    // AffordNow['RainyDayFund'] = int(RainyDayFund)
    // AffordNow['TotalCash'] = int(TotalCashNeeded)
    // AffordNow['Difference'] = int(Difference)
    // AffordNow['CashToClose'] = int(CashToClose)
    // AffordNow['PITI2Income'] = int(PITIToIncome)
    // AffordNow['Debt2Income'] = int(DebtToIncome)
    // AffordNow['Rate'] = float(Rate)
    // AffordNow['Years'] = int(Yrs)

    /* ======================================================================
    Need to also calculate Max based on Income. This is because the amount of
    cash you have may indicate a higher mortgage amount than calculated based
    on income, but a bank won't allow that, so it's the minimum of the two
    just to be safe
    ====================================================================== */

    let TDI = (0.36 * MonthlyGross) - debts;
    let PITI = (0.28 * MonthlyGross);

    if (mortgagetype == '30 Year FHA Loan') {

        TDI = (0.43 * MonthlyGross) - debts;
        PITI = (0.31 * MonthlyGross);
    }


    let MaxPITI = maxPITI;

    let MaxAffordOut = {};
    let Divisor = 12 * (1 - DP);

    let Taxes = Tax / Divisor;
    let Insurance = Ins / Divisor;
    let MortIns = PMI / Divisor;

    let TIM = Taxes + Insurance + MortIns;
    let PI = (I/12) / (1 - (1+I/12)**-(N*M));

    let TIMPI = TIM + PI;

    let MortgageMx = MaxPITI / TIMPI;
    let MaxHome = MortgageMx / (1 - DP);

    let MonthlyTaxesMx = Tax * MaxHome / 12;
    let MonthlyInsuranceMx = Ins * MaxHome / 12;
    let MonthlyPMIMx = PMI * MaxHome / 12;
    //calculate PMT as separate calculation
    let x = Math.pow(1 + (I / 12), N*M)
    let MonthlyPnIMx = ((I/12 * (0 + x * MortgageMx))/(-1 + x));

    let ClosingCostsMx = 0.02 * MaxHome;
    let DownPaymentMx = DP * MaxHome;
    let CashToCloseMx = DownPaymentMx + ClosingCostsMx;
    let RDFMx = (MonthlyTaxesMx + MonthlyInsuranceMx + MonthlyPMIMx + MonthlyPnIMx + expenses + debts + HOA) * R;

    let PITIToIncomeMx = (MaxPITI / MonthlyGross) * 100;
    let DebtToIncomeMx = ((MaxPITI + debts) / MonthlyGross) * 100;

    if (MaxHome <= HomeValueNow) {

        let AffordNow['Mortgage'] = Number(MortgageMx);
        let AffordNow['MaxHome'] = Number(MaxHome);
        let AffordNow['MonthlyTaxes'] = Number(MonthlyTaxesMx);
        let AffordNow['MonthlyInsurance'] = Number(MonthlyInsuranceMx);
        let AffordNow['PMI'] = Number(MonthlyPMIMx);
        let AffordNow['PnI'] = Number(MonthlyPnIMx);
        let AffordNow['PITI'] = Number(MonthlyTaxesMx + MonthlyInsuranceMx + MonthlyPMIMx + MonthlyPnIMx);
        let AffordNow['ClosingCosts'] = Number(ClosingCostsMx);
        let AffordNow['DownPayment'] = Number(DownPaymentMx);
        let AffordNow['CashToClose'] = Number(CashToCloseMx);
        let AffordNow['RainyDayFund'] = Number(RDFMx);
        let AffordNow['TotalCash'] = Number(ClosingCostsMx + DownPaymentMx + RDFMx);
        let AffordNow['PITIToIncome'] = Number(PITIToIncomeMx);
        let AffordNow['DebtToIncome'] = Number(DebtToIncomeMx);
        let AffordNow['Rate'] = float(Rate);
        let AffordNow['Years'] = Number(Yrs);
        let AffordNow['MaxPITI'] = Number(MaxPITI);
        let AffordNow['HOA'] = Number(HOA);
        }

    else {
        let AffordNow['Mortgage'] = Number(Mortgage);
        let AffordNow['MaxHome'] = Number(HomeValueNow);
        let AffordNow['PnI'] = Number(MonthlyPnI);
        let AffordNow['MonthlyTaxes'] = Number(MonthlyTaxes);
        let AffordNow['MonthlyInsurance'] = Number(MonthlyInsurance);
        let AffordNow['PMI'] = Number(MonthlyPMI);
        let AffordNow['PITI'] = Number(MonthlyPITI);
        let AffordNow['ClosingCosts'] = Number(ClosingCosts);
        let AffordNow['DownPayment'] = Number(DownPayment);
        let AffordNow['RainyDayFund'] = Number(RainyDayFund);
        let AffordNow['TotalCash'] = Number(TotalCashNeeded);
        let AffordNow['Difference'] = Number(Difference);
        let AffordNow['CashToClose'] = Number(CashToClose);
        let AffordNow['PITI2Income'] = Number(PITIToIncome);
        let AffordNow['Debt2Income'] = Number(DebtToIncome);
        let AffordNow['Rate'] = float(Rate);
        let AffordNow['Years'] = Number(Yrs);
        let AffordNow['MaxPITI'] = Number(MaxPITI);
        let AffordNow['HOA'] = Number(HOA);
    }
    //NEED TO ACCOUNT FOR TOTAL MAX AFFORDABILTY IN THE EVENT CASH > ACTUAL AFFORDABILITY!!
    //return MaxAffordOut
    return AffordNow
}

