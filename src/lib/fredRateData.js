export const ThirtyFred = () => {
  return ["2017-08-06", 4.625]
}
export const FifteenFred = () => {
  return ["2017-08-06", 3.375]
}
export const ARMFred = () => {
  return ["2017-08-06", 3.625]
}
export const ThirtyJumbo = () => {
  return ["2017-08-06", 4.0]
}
export const FifteenJumbo = () => {
  return ["2017-08-06", 3.875]
}
export const ThirtyVA = () => {
  return ["2017-08-06", 3.875]
}
export const ThirtyFHA = () => {
  return ["2017-08-06", 4.625]
}
