import { FredRates } from "./mortgagerates"

export const affordAbilityMax = (
    grossincome,
    expenses,
    debts,
    hometype,
    homeuse,
    creditscore,
    mortgagetype,
    totalcash
) => {
    var MonthlyGross = grossincome
    var expenses = expenses
    var debts = debts
    var totalcash = totalcash

    //convert creditscores to numericals

    if (creditscore === "760+" || creditscore === "740-759") {
        creditscore = 1
    }

    if (creditscore === "720-739" || creditscore === "700-719") {
        creditscore = 2
    }

    if (creditscore === "680-99" || creditscore === "660-679") {
        creditscore = 3
    }

    if (creditscore === "640-659") {
        creditscore = 4
    }

    if (creditscore === "<640") {
        creditscore = 5
    }

    //Call the mortgage rates from the DB Table

    let RatesList = FredRates()

    let ThirtyFred = RatesList["ThirtyFred"]
    let FifteenFred = RatesList["FifteenFred"]
    let ARMFred = RatesList["ARMFred"]
    //These need to be added to the app drop down lists...just the VA and FHA Options, Jumbo is calculated based on mortgage amount
    let ThirtyJumbo = RatesList["ThirtyJumbo"]
    let FifteenJumbo = RatesList["FifteenJumbo"]
    let ThirtyVA = RatesList["ThirtyVA"]
    let ThirtyFHA = RatesList["ThirtyFHA"]

    let HOA = 0.0
    if (hometype === "Condo") {
        HOA = 300.0
    }

    //Assumes 20% Down
    let MinDownPct = 20.0

    /* ======================================================================
    CALCULATE THE INTEREST RATE

        15% Down: +0.125%
        10% Down: +0.25%
        *Adjustment made up to 25% down
        *Reconfigure in Valuation based on Amt Down/Value/Offer
        Townhouse: 0
        Condo: +0.125%
        Coop: 0
        Mobile, et al: 0
        Vacation: 0
        Investment: minimum 20% down and +0.5%

    Need to add spread for Jumbo loans, and set min down payment % to 20
    and max mortgage at 417,000 (conforming limit...)
    ======================================================================= */

    let Yrs
    let Rate
    let annualpremium
    let upfront
    let min
    let PMI

    if (mortgagetype === "30 Year Fixed") {
        Yrs = 30
        Rate = ThirtyFred
        if (hometype === "Single Family Home") {
            Rate = ThirtyFred
        } else {
            if (MinDownPct < 25.0) {
                Rate = ThirtyFred + 0.125
            }
        }
    }

    if (mortgagetype === "15 Year Fixed") {
        Yrs = 15
        Rate = FifteenFred

        if (hometype === "Single Family Home") {
            Rate = FifteenFred
        } else {
            if (MinDownPct < 25.0) {
                Rate = FifteenFred + 0.125
            }
        }
    }

    if (mortgagetype === "5/1 Adjustable Rate") {
        Yrs = 30
        Rate = ARMFred

        if (hometype === "Single Family Home") {
            Rate = ARMFred
        } else {
            if (MinDownPct < 25.0) {
                Rate = ARMFred + 0.125
            }
        }
    }

    if (mortgagetype === "30 Year VA") {
        Yrs = 30

        if (hometype === "Single Family Home") {
            Rate = ThirtyVA
        } else {
            Rate = ThirtyVA + 0.125
        }

        MinDownPct = 0.0
    }

    //Change the rate by adding the FHA Rate
    if (mortgagetype === "30 Year FHA Loan") {
        Yrs = 30

        if (hometype === "Single Family Home") {
            Rate = ThirtyFHA
        } else {
            if (MinDownPct < 25.0) {
                Rate = ThirtyFHA
            } else {
                Rate = ThirtyFHA
            }
        }

        //change down payment percentage to 3.5%
        MinDownPct = 3.5
    }

    if (mortgagetype === "30 Year FHA Loan") {
        annualpremium = 0.6
        upfront = 0.0175
    }

    if (mortgagetype === "30 Year VA") {
        upfront = 2.15
        annualpremium = 0.0
    } else {
        annualpremium = 0.0
        upfront = 0.0
    }

    if (creditscore === 1 || creditscore === 2) {
        Rate = Rate
    }

    if (creditscore === 3) {
        Rate = Rate + 0.125
    }

    if (creditscore === 4) {
        Rate = Rate + 0.25
    }

    if (creditscore >= 5) {
        Rate = Rate + 0.5
    }

    if (homeuse === "Investment Property") {
        Rate = Rate + 0.5
    }

    if (MinDownPct < 20.0 && MinDownPct >= 15.0) {
        Rate = Rate + 0.125
    }

    if (MinDownPct < 15.0 && MinDownPct >= 10.0) {
        Rate = Rate + 0.25
    }

    if (MinDownPct < 10.0 && mortgagetype != "30 Year FHA Loan") {
        Rate = Rate + 0.375
    }

    let I = Rate

    let TDI = 0.36 * MonthlyGross - debts

    let PITI = 0.28 * MonthlyGross

    if (mortgagetype === "30 Year FHA Loan") {
        TDI = 0.43 * MonthlyGross - debts
        PITI = 0.31 * MonthlyGross
    }

    let MaxPITI = Math.min(TDI, PITI)

    let DP = MinDownPct / 100 //assumes 20% Down as base case

    let Tax = 0.02 //assumes taxes are 2% of home value per year
    let Ins = 0.0017 //change this to min(Home Price/1000 * 3.50,1000)/12

    if (DP < 0.2) {
        //assumes PMI as 0.25% of home value per year
        PMI = 0.0025

        if (mortgagetype === "30 Year FHA Loan") {
            PMI = annualpremium / 100
        }
    } else {
        PMI = 0
    }

    let N = 12 //assumes monthly payments
    let M = Yrs //30 || 15 years based on mortgage product
    I = I / 100 //Interest Rate calculated above

    let MaxAffordOut = {}
    let Divisor = 12 * (1 - DP)
    let Taxes = Tax / Divisor
    let Insurance = Ins / Divisor
    let MortIns = PMI / Divisor
    let TIM = Taxes + Insurance + MortIns
    let PI = I / 12 / (1 - (1 + I / 12) ** -(N * M))
    let TIMPI = TIM + PI
    let R = 3 //assumes 3 months living expenses
    let Mortgage = MaxPITI / TIMPI
    let MaxHome = Mortgage / (1 - DP)
    let MonthlyTaxes = Tax * MaxHome / 12
    let MonthlyInsurance = Ins * MaxHome / 12
    let MonthlyPMI = PMI * MaxHome / 12
    //calculate PMT as separate calculation
    let x = Math.pow(1 + I / 12, N * M)
    let PnI = I / 12 * (0 + x * Mortgage) / (-1 + x)

    let EstPITI = PnI + MonthlyTaxes + MonthlyInsurance + MonthlyPMI
    let ClosingCosts = 0.02 * MaxHome + MaxHome * upfront
    let DownPayment = DP * MaxHome
    let CashToClose = DownPayment + ClosingCosts
    let RDF =
        R *
        (MonthlyTaxes +
            MonthlyInsurance +
            MonthlyPMI +
            PnI +
            expenses +
            debts +
            HOA)
    let PITIToIncome = EstPITI / MonthlyGross * 100
    let DebtToIncome = (EstPITI + debts) / MonthlyGross * 100
    let GoalPercent = totalcash / (ClosingCosts + DownPayment + RDF) * 100

    MaxAffordOut["Mortgage"] = Mortgage
    MaxAffordOut["MaxHome"] = MaxHome
    MaxAffordOut["MonthlyTaxes"] = MonthlyTaxes
    MaxAffordOut["MonthlyInsurance"] = MonthlyInsurance
    MaxAffordOut["PMI"] = MonthlyPMI
    MaxAffordOut["PnI"] = PnI
    MaxAffordOut["PITI"] = MonthlyTaxes + MonthlyInsurance + MonthlyPMI + PnI
    MaxAffordOut["ClosingCosts"] = ClosingCosts
    MaxAffordOut["DownPayment"] = DownPayment
    MaxAffordOut["CashToClose"] = CashToClose
    MaxAffordOut["RainyDayFund"] = RDF
    MaxAffordOut["TotalCash"] = ClosingCosts + DownPayment + RDF
    MaxAffordOut["PITI2Income"] = PITIToIncome
    MaxAffordOut["Debt2Income"] = DebtToIncome
    MaxAffordOut["Rate"] = parseFloat(Rate)
    MaxAffordOut["Years"] = Yrs
    MaxAffordOut["MaxPITI"] = MaxPITI
    MaxAffordOut["HOA"] = HOA
    MaxAffordOut["GoalPercent"] = GoalPercent
    MaxAffordOut["MortgageType"] = mortgagetype
    MaxAffordOut["CreditScore"] = creditscore
    MaxAffordOut["HomeType"] = hometype
    MaxAffordOut["HomeUse"] = homeuse

    return MaxAffordOut
}
