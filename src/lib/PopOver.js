import React, { Component } from "react"
import { PropTypes } from "prop-types"
import Dialog from "material-ui/Dialog"
import FlatButton from "material-ui/FlatButton"

class PopOver extends Component {
  state = {
    open: false
  }

  handleOpen = isopen => {
    this.setState({ open: isopen })
  }

  handleClose = () => {
    this.setState({ open: false })
  }

  render() {
    const actions = [
      <FlatButton
        label="Close"
        primary={true}
        onTouchTap={this.props.onHandleClose}
      />
    ]

    return (
      <div>
        <Popover
          open={this.props.open}
          anchorEl={this.props.anchorEl}
          anchorOrigin={{this.props.anchorOrigin}
          targetOrigin={this.props.targetOrigin}
          onRequestClose={this.handleClose}
          className={this.props.className}
        >
          {this.props.children}
        </Popover>
      </div>
    )
  }
}

PopOver.propTypes = {

  open: PropTypes.bool.isRequired,
  onHandleClose: PropTypes.func.isRequired,
  className: PropTypes.object.isRequired,
  anchorEl:PropTypes.object.isRequired,
  anchorOrigin: PropTypes.object.isRequired,
}

export default PopOver
