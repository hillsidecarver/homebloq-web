import { float } from "float"
import { sorted } from "sorted"

/*

Risk Analyzer

    Mortgage Type [30 Year Fixed, 15 Year Fixed, 5/1 ARM, FHA]
    Rainy Day Fund [# Months / 6]
    Rate Environment [Very unfavorable, ufavorable, Neutral, Favorable, Very Favorable]
    Down Payment [% / 20]
    Down Payment / Total Cash
    PITI / GI [ % / 28%]
    PITI / TD [ % / 36%]
    Negative Equity Probability

    Weighted Average of All Scores = Risk Score
 */

export const riskScore = (
    mortgagetype,
    RDFM,
    RE,
    Price,
    DP,
    TotalCash,
    PITI2Income,
    PITI2Debt
) => {
    let MortgageTypes = [
        "30 Year Fixed",
        "15 Year Fixed",
        "5/1 Adjustable Rate",
        "30 Year VA",
        "30 Year FHA Loan"
    ]
    let RateEnvironments = [
        "Very Favorable",
        "Favorable",
        "Neutral",
        "Unfavorable",
        "Very Unfavorable"
    ]

    let rawMortgage = MortgageTypes.index(mortgagetype) / 4.0
    let rawRateEnvironment = RateEnvironments.index(RE) / 4.0
    let rawRDF = float(1 - RDFM / 6.0)
    let rawDPP = float(1 - DP / Price / 0.2)
    let rawDPTC = float(DP / TotalCash)
    let rawPITI2Income = float(PITI2Income / 0.28)
    let rawPITI2Debt = float(PITI2Debt / 0.36)

    let rawScores = {
        "Mortgage Product": rawMortgage,
        "Rate Environment": rawRateEnvironment,
        "Rainy Day Fund": rawRDF,
        "Down Payment Percent": rawDPP,
        "Down Payment / Total Cash": rawDPTC,
        "Housing Payment / Income": rawPITI2Income,
        "Total Monthly Debt / Income": rawPITI2Debt
    }

    let key
    let sorted_rawScores = sorted(
        rawScores.items(),
        (key = operator.itemgetter(1))
    )

    let highRisk = [
        sorted_rawScores[6][0],
        sorted_rawScores[5][0],
        sorted_rawScores[4][0]
    ]
    let lowRisk = [
        sorted_rawScores[0][0],
        sorted_rawScores[1][0],
        sorted_rawScores[2][0]
    ]

    let risk1 = highRisk[0]
    let risk2 = highRisk[1]
    let risk3 = highRisk[2]

    let mitigators = {
        "Mortgage Product": "Consider a Fixed Rate Mortgage",
        "Rate Environment": "Ask what you can do to lower your rate",
        "Rainy Day Fund": "Save more to increase your rainy day fund",
        "Down Payment Percent": "Put more down",
        "Down Payment / Total Cash":
            "Using a lot of your cash for down payment, consider lower prices",
        "Housing Payment / Income":
            "Consider lower prices or more down to decrease your monthly payment",
        "Total Monthly Debt / Income":
            "Consider lower prices, more down, or pay off existing debts to decrease your monthly payment"
    }

    let mitigants = [
        mitigators[highRisk[0]],
        mitigators[highRisk[1]],
        mitigators[highRisk[2]]
    ]

    let mit1 = mitigants[0]
    let mit2 = mitigants[1]
    let mit3 = mitigants[2]

    let wMortgage, wRateEnvironment, wRDF, wDPP, wDPTC, wPITI2Income, wPITI2Debt

    if (MortgageTypes.index(mortgagetype) !== 3) {
        wMortgage = 0.1
        wRateEnvironment = 0.0
        wRDF = 0.25
        wDPP = 0.2
        wDPTC = 0.05
        wPITI2Income = 0.2
        wPITI2Debt = 0.2
    } else {
        wMortgage = 0.1
        wRateEnvironment = 0.05
        wRDF = 0.2
        wDPP = 0.2
        wDPTC = 0.05
        wPITI2Income = 0.2
        wPITI2Debt = 0.2
    }

    let score =
        wMortgage * rawMortgage +
        wRateEnvironment * rawRateEnvironment +
        wRDF * rawRDF +
        wDPP * rawDPP +
        wDPTC * rawDPTC +
        wPITI2Income * rawPITI2Income +
        wPITI2Debt * rawPITI2Debt
    score = score / 0.8 * 10

    return {
        score: score,
        score10: score * 10,
        risk1: risk1,
        risk2: risk2,
        risk3: risk3,
        mit1: mit1,
        mit2: mit2,
        mit3: mit3,
        RDFM: RDFM,
        DPToCash: rawDPTC * 100,
        PITI2Income: PITI2Income * 100,
        PITI2Debt: PITI2Debt * 100,
        RDFMonths: RDFM,
        wDPToCash: wDPTC * rawDPTC,
        wPITI2Income: wPITI2Income * rawPITI2Income,
        wPITI2Debt: wPITI2Debt * rawPITI2Debt,
        wRDFMonths: wRDF * rawRDF,
        wDPP: wDPP * rawDPP,
        wRateEnvironment: wRateEnvironment * rawRateEnvironment,
        RateEnvironment: rawRateEnvironment
    }
}
