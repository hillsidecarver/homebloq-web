import { FredRates } from "./mortgagerates"
export const affordabilityNow = (
    grossincome,
    expenses,
    debts,
    totalcash,
    hometype,
    homeuse,
    creditscore,
    mortgagetype,
    houseprice,
    maxHome
) => {
    var MonthlyGross = parseFloat(grossincome.replace(/,/g, ""))
    var expenses = parseFloat(expenses.replace(/,/g, ""))
    var debts = parseFloat(debts.replace(/,/g, ""))
    var totalcash = parseFloat(totalcash.replace(/,/g, ""))

    //convert creditscores to numericals
    if (creditscore == "760+" || creditscore == "740-759") {
        creditscore = 1
    }

    if (creditscore == "720-739" || creditscore == "700-719") {
        creditscore = 2
    }

    if (creditscore == "680-99" || creditscore == "660-679") {
        creditscore = 3
    }

    if (creditscore == "640-659") {
        creditscore = 4
    }

    if (creditscore == "<640") {
        creditscore = 5
    }

    let RatesList = FredRates()

    let HOA
    if (hometype == "Condo") {
        HOA = 300.0
    } else {
        HOA = 0.0
    }

    //Assumes 20% Down
    let MinDownPct = 20.0

    if (mortgagetype == "30 Year Fixed") {
        Yrs = 30
        if (hometype == "Single Family Home") {
            Rate = ThirtyFred
        } else {
            if (MinDownPct < 25.0) {
                Rate = ThirtyFred + 0.125
            } else {
                Rate = ThirtyFred
            }
        }
    }

    if (mortgagetype == "15 Year Fixed") {
        Yrs = 15

        if (hometype == "Single Family Home") {
            Rate = FifteenFred
        } else {
            if (MinDownPct < 25.0) {
                Rate = FifteenFred + 0.125
            } else {
                Rate = FifteenFred
            }
        }
    }

    if (mortgagetype == "5/1 Adjustable Rate") {
        Yrs = 30

        if (hometype == "Single Family Home") {
            Rate = ARMFred
        } else {
            if (MinDownPct < 25.0) {
                Rate = ARMFred + 0.125
            } else {
                Rate = ARMFred
            }
        }
    }

    if (mortgagetype == "30 Year VA") {
        Yrs = 30

        if (hometype == "Single Family Home") {
            Rate = ThirtyVA
        } else {
            Rate = ThirtyVA + 0.125
        }

        MinDownPct = 0.0
    }

    //Change the rate by adding the FHA Rate
    if (mortgagetype == "30 Year FHA Loan") {
        Yrs = 30

        if (hometype == "Single Family Home") {
            Rate = ThirtyFHA
        } else {
            if (MinDownPct < 25.0) {
                Rate = ThirtyFHA
            } else {
                Rate = ThirtyFHA
            }
        }

        //change down payment percentage to 3.5%
        let MinDownPct = 3.5
    }

    if (mortgagetype == "30 Year FHA Loan") {
        annualpremium = 0.85
        upfront = 0.0175
    }

    if (mortgagetype == "30 Year VA") {
        annualpremium = 0.0
        upfront = 2.15
    } else {
        annualpremium = 0.0
        upfront = 0.0
    }

    if (creditscore == 1 || creditscore == 2) {
        Rate = Rate
    }

    if (creditscore == 3) {
        Rate = Rate + 0.125
    }

    if (creditscore == 4) {
        Rate = Rate + 0.25
    }

    if (creditscore >= 5) {
        Rate = Rate + 0.5
    }

    if (homeuse == "Investment Property") {
        Rate = Rate + 0.5
    }

    if (MinDownPct < 20.0 && MinDownPct >= 15.0) {
        Rate = Rate + 0.125
    }

    if (MinDownPct < 15.0 && MinDownPct >= 10.0) {
        Rate = Rate + 0.25
    }

    if (MinDownPct < 10.0 && mortgagetype != "30 Year FHA Loan") {
        Rate = Rate + 0.375
    }

    let I = Rate / 100

    /* ======================================================================
  Max Affordability based on Income and Expenses

  Use Max Monthly PITI and assumptions about Insurance, Taxes, PMI, HOA
  and solve for maximum mortgage and home price as well as total cash
  to close and 6 (or 3 or other) living expenses = rainy day fund

  Assumptions:
      Rainy Day Fund = 6 Months Living Expenses
      20% Down Payment
      Condo HOA of $300 (average of 200-400/month)

  Should show the following scenarios:
      20%, 15%, 10%, 5% Down (subject to conventional mortgage max)
      6, 5, 4, 3 Month Rainy Day Fund

      Then let user choose which they prefer?
  ====================================================================== */
    //    PITI = (0.28 * MonthlyGross)
    let MaxPITI = maxPITI

    let DP = MinDownPct / 100 //assumes 20% Down as base case;
    let M = Yrs //30 || 15 years based on mortgage product;
    let N = 12 //assumes monthly payments;
    let Tax = 0.02 //assumes taxes are 2% of home value per year;
    let Ins = 0.0017 //change this to min(Home Price/1000 * 3.50,1000)/12;
    let CC = 0.02 //assumes closing costs of 2% of home value;

    if (DP < 0.2) {
        //assumes PMI as 0.25% of home value per year
        PMI = 0.0025

        if (mortgagetype == "30 Year FHA Loan") {
            PMI = annualpremium / 100
        }
    } else {
        PMI = 0
    }

    let R = 3 //assumes 3 months living expenses

    /*
  Methodology:
  Rainy Day Fund = 6 * (PnI + Taxes + Ins + TotalExpenses + TotalDebts + HOA)
      PnI = (i/n / (1-(1+i/n)^-m*n)) * Mortgage/(1-DP)
      Taxes = 0.02 * Mortgage/(1-DP) / 12
      Ins = 0.0017 * Mortgage/(1-DP) / 12
      Total Expenses = Known
      Total Debts = Known
      HOA = Known

      Mortgage / (1-DP) = Home Value (i.e. Mortgage / (1-0.20) if 20% is down payment percentage)

  Total Cash Req'd = 6*RainyDayFund + Closing Cost + DownPayment
      Total Cash Req'd = Total Cash on Hand
      Closing Costs = 0.02 * Mortgage / (1 - DP)
      DownPayment = 0.20 * Mortgage / (1 - DP)

  Solve for Mortgage and divide by 1 - Down Payment Percent = Home
  DP = Down Payment Percent = 20% for base case
   */

    let Divisor = 12 * (1 - DP)
    let ClosingCosts = CC / (1 - DP)
    let DownPayment = 0.2 / (1 - DP)

    let Taxes = Tax / Divisor
    let Insurance = Ins / Divisor
    let PnI = Math.pow(I / 12 / (1 - (1 + I / 12)), -(M * N))
    let MIns = PMI / Divisor
    HOA = HOA
    let Denominator =
        R * Taxes +
        R * Insurance +
        R * MIns +
        R * PnI +
        ClosingCosts +
        DownPayment
    let Mortgage =
        (totalcash - R * HOA - R * expenses - R * debts) / Denominator
    let HomeValueNow = Mortgage / (1 - DP)

    //calculate PMT as separate calculation
    let x = Math.pow(1 + I / 12, N * M)
    let MonthlyPnI = I / 12 * (0 + x * Mortgage) / (-1 + x)

    let MonthlyTaxes = Tax * HomeValueNow / 12
    let MonthlyInsurance = Ins * HomeValueNow / 12
    let MonthlyPMI = PMI * HomeValueNow / 12
    let MonthlyPITI = MonthlyPnI + MonthlyTaxes + MonthlyInsurance + MonthlyPMI
    ClosingCosts = HomeValueNow * 0.02 + HomeValueNow * upfront
    DownPayment = HomeValueNow - Mortgage

    let RainyDayFund =
        R *
        (MonthlyPnI +
            MonthlyTaxes +
            MonthlyInsurance +
            MonthlyPMI +
            HOA +
            expenses +
            debts)
    let TotalCashNeeded = RainyDayFund + ClosingCosts + DownPayment
    let Difference = totalcash - TotalCashNeeded
    let CashToClose = DownPayment + ClosingCosts

    let PITIToIncome = MonthlyPITI / MonthlyGross * 100
    let DebtToIncome = (MonthlyPITI + debts) / MonthlyGross * 100

    /*=====================================================================
Need to also calculate Max based on Income. This is because the amount of cash you have may indicate a higher mortgage amount than calculated based on income, but a bank won't allow that, so it's the minimum of the two just to be safe.
=====================================================================*/

    let TDI = 0.36 * MonthlyGross - debts
    let PITI = 0.28 * MonthlyGross

    if (mortgagetype == "30 Year FHA Loan") {
        TDI = 0.43 * MonthlyGross - debts
        PITI = 0.31 * MonthlyGross
    }

    MaxPITI = Math.min(TDI, PITI)

    /*=====================================================================
  Okay, so we've got affordability MAX and NOW. Let's find out the cost of      this specific house at 20%      down and compare to your affordability. If      you can afford it, use these components, if you cannot, still display the   components but make it clear it's beyond the users ability to buy?

  Calculate: Mortgage, Taxes, Insurance, PMI, PnI, PITI, ClosingCosts,
  DownPayment, CashToClose, RainyDayFund, PITIToIncome, DebtToIncome, HOA,
  and a flag (can you afford or not? Y/N).
        =====================================================================*/

    MortgageSH = houseprice * (1 - MinDownPct / 100)
    DownPaymentSH = houseprice - MortgageSH
    TaxesSH = houseprice * Tax / 12
    InsuranceSH = houseprice * Ins / 12
    PMISH = PMI * houseprice / 12
    x = Math.pow(1 + I / 12, N * M)
    let PnISH = I / 12 * (0 + x * MortgageSH) / (-1 + x)
    PITISH = PnISH + PMISH + InsuranceSH + TaxesSH
    ClosingCostsSH = CC * houseprice
    CashToCloseSH = ClosingCostsSH + DownPaymentSH
    RainyDayFundSH = R * (expenses + debts + PITISH + HOA)
    PITIToIncomeSH = Number(PITISH / MonthlyGross * 100)
    DebtToIncomeSH = (PITISH + debts) / MonthlyGross * 100
    let Flag
    if (PITISH < MaxPITI && PITIToIncomeSH < 28 && DebtToIncomeSH < 36) {
        Flag =
            "You can afford this home based on your maximum affordability estimate!"
    } else {
        Flag = "This price exceeds your maximum affordability estimate!"
    }

    var specHome = {
        MortgageSH: MortgageSH,
        DownPaymentSH: DownPaymentSH,
        TaxesSH: TaxesSH,
        InsuranceSH: InsuranceSH,
        PMISH: PMISH,
        PnISH: PnISH,
        PITISH: PITISH,
        ClosingCostsSH: ClosingCostsSH,
        CashToCloseSH: CashToCloseSH,
        RainyDayFundSH: RainyDayFundSH,
        PITIToIncomeSH: PITIToIncomeSH,
        DebtToIncomeSH: DebtToIncomeSH,
        Flag: Flag,
        maxHome: maxHome,
        thisHome: houseprice,
        difference: maxHome - houseprice
    }

    return specHome
}
