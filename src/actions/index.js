import { tracking } from "../lib/fakedata"
import { FredRates } from "../lib/mortgagerates"

const BASE_URL = "http://localhost:3001/api/"

export const REQUEST_POSTS = "REQUEST_POSTS"
export const RECEIVE_POSTS = "RECEIVE_POSTS"

export const LOGIN_REQUEST = "LOGIN_REQUEST"
export const LOGIN_SUCCESS = "LOGIN_SUCCESS"
export const LOGIN_FAILURE = "LOGIN_FAILURE"
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS"
export const LOGOUT_REQUEST = "LOGOUT_REQUEST"

export const SAVE_PROFILE_REQUEST = "SAVE_PROFILE_REQUEST"
export const RECEIVE_PROFILE_REQUEST = "RECEIVE_PROFILE_REQUEST"
export const RECEIVE_PROFILE_FAILURE = "RECEIVE_PROFILE_FAILURE"

export const SAVE_LINKEDIN_PROFILE_REQUEST = "SAVE_LINKEDIN_PROFILE_REQUEST"
export const RECEIVE_LINKEDIN_PROFILE_REQUEST =
  "RECEIVE_LINKEDIN_PROFILE_REQUEST"
export const RECEIVE_LINKEDIN_PROFILE_FAILURE =
  "RECEIVE_LINKEDIN_PROFILE_FAILURE"

export const REQUEST_TRACKING = "REQUEST_TRACKING"
export const RECEIVE_TRACKING = "RECEIVE_TRACKING"
export const SORT_TRACKING = "SORT_TRACKING"
export const UPDATE_TRACKING_REQUEST = "UPDATE_TRACKING_REQUEST"
export const UPDATE_TRACKING_RECEIVE = "UPDATE_TRACKING_RECEIVE"

export const REQUEST_CUSTOMER_PROPERTY_TRACKING =
  "REQUEST_CUSTOMER_PROPERTY_TRACKING"
export const RECEIVE_CUSTOMER_PROPERTY_TRACKING =
  "RECEIVE_CUSTOMER_PROPERTY_TRACKING"

export const FRED_REQUEST = "FRED_REQUEST"
export const FRED_FAILURE = "FRED_FAILURE"

export const requestFred = data => ({
  type: FRED_REQUEST,
  isFetching: false,
  data
})

export const fredError = error => ({
  type: FRED_REQUEST,
  isFetching: false,
  error
})

export const requestPosts = payload => ({
  type: REQUEST_POSTS,
  payload
})

export const receivePosts = (payload, json) => ({
  type: RECEIVE_POSTS,
  payload,
  posts: json,
  receivedAt: Date.now()
})

export const requestCustomersProperties = professionalId => ({
  type: REQUEST_CUSTOMER_PROPERTY_TRACKING,
  professionalId
})

export const receiveCustomersProperties = list => ({
  type: RECEIVE_CUSTOMER_PROPERTY_TRACKING,
  list,
  receivedAt: Date.now()
})

export const requestTracking = customerId => ({
  type: REQUEST_TRACKING,
  customerId
})

export const receiveTracking = tracking => ({
  type: RECEIVE_TRACKING,
  tracking,
  receivedAt: Date.now()
})

export const requestSaveProfile = profile => ({
  type: SAVE_PROFILE_REQUEST,
  isFetching: true,
  profile
})

export const receiveSaveProfile = profile => ({
  type: RECEIVE_PROFILE_REQUEST,
  isFetching: true,
  profile
})

export const requestUpdateState = item => ({
  type: UPDATE_TRACKING_REQUEST,
  isFetching: false,
  item
})

export const receiveUpdateState = tracking => ({
  type: UPDATE_TRACKING_RECEIVE,
  isFetching: false,
  tracking
})

export const requestLogin = creds => ({
  type: LOGIN_REQUEST,
  isFetching: true,
  isAuthenticated: false,
  creds
})

export const receiveLogin = profile => ({
  type: LOGIN_SUCCESS,
  isFetching: false,
  isAuthenticated: true,
  id_token: profile.id_token,
  profile
})

export const loginError = message => ({
  type: LOGIN_FAILURE,
  isFetching: false,
  isAuthenticated: false,
  message
})

export const requestLogout = () => ({
  type: LOGOUT_REQUEST,
  isFetching: true,
  isAuthenticated: true
})

export const receiveLogout = () => ({
  type: LOGOUT_SUCCESS,
  isFetching: false,
  isAuthenticated: false
})

const config = {
  method: "POST",
  headers: { "Content-Type": "application/json" }
}

export const updateTrackState = (item, tracking) => dispatch => {
  dispatch(requestUpdateState(item))
  dispatch(receiveUpdateState(tracking))
}

export const loadFred = () => dispatch => {
  try {
    dispatch(requestFred(FredRates()))
  } catch (e) {
    dispatch(fredError(e))
  }
}

export const loginUser = creds => dispatch => {
  config.body = creds

  let user = {
    id_token: creds.username
  }

  dispatch(requestLogin(creds))

  // Dispatch the success action
  dispatch(receiveLogin({ user }))

  let profile = {
    lastname: "Consumer",
    firstname: "Professional",
    homebloqtype: "CONSUMER"
  }

  dispatch(requestSaveProfile(profile))

  dispatch(receiveSaveProfile(profile))

  // dispatch(receiveSaveProfile(profile));

  /*
    // We dispatch requestLogin to kickoff the call to the API
    dispatch(requestLogin(creds))
    return fetch(BASE_URL + 'sessions/create', config)
      .then(response =>
        response.json()
        .then(user => ({ user, response }))
      ).then(({ user, response }) =>  {
        if (!response.ok) {
          // If there was a problem, we want to
          // dispatch the error condition
          dispatch(loginError(user.message))
          return Promise.reject(user)
        }
        else {
          // If login was successful, set the token in local storage
          localStorage.setItem('id_token', user.id_token)

          // Dispatch the success action
          dispatch(receiveLogin(user))
        }
      }).catch(err => console.log("Error: ", err))

*/
}

export const retrieveCustomersProperties = professionalId => dispatch => {
  dispatch(requestCustomersProperties(professionalId))
  //dispatch(receiveCustomersProperties(customerProperties()))
}

export const retrieveTracking = customerId => dispatch => {
  dispatch(requestTracking(customerId))
  dispatch(receiveTracking(tracking()))
}

export const saveProfile = profile => dispatch => {
  config.body = profile

  dispatch(requestSaveProfile(profile))

  //localStorage.setItem("profile", profile)
  dispatch(receiveSaveProfile(profile))
}

export const logoutUser = dispatch => {
  return dispatch => {
    dispatch(requestLogout())
    localStorage.removeItem("id_token")
    localStorage.removeItem("user")
    dispatch(receiveLogout())
  }
}

function callApiLinkedin(endpoint, authenticated) {
  const config = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "x-li-format": "json",
      Authorization:
        "Bearer AQWcHPOChvPUFnf9duejdlK04Wlpepiu2ucs5AHaCY3do0MMy9vAW3lo0vCImrm3Lf1JnV9JrGGp-4r1yk5EKuetP3wop1FQkpDsyIH3k3FHWmyPeIob_xcB6LxogcyDpRyY4byNb65Mi2g3IEz24UyL3dYjkDcWexLisnF4Uxv-EPLwwFk"
    }
  }

  /*
  https://homebloq.com/auth/callback?
AQSRSFy4xn28cFTvHX_eOnsXKc5O9uKutbjs1-2_FwepLZJiP7VIcRbKSyZaXu6wciGffo20i0ymW23SXzCruGKUHOMEmw1ryurvX2QvGOByeSFZBG8
  &state=4%243232

  https://homebloq.com/auth/callback?
  code=AQR3cO60RgM3cy2OS8xjUsN6_uEjtreXNizF-uvCdgkBE9Fa_dWY9c_xrHXgfJK3-e_EWTylVbdZ1cYWSmg-qXfTMriaYtxbmhwrE-WSOCq1SmiZLhc
  &state=4%243232

  {"access_token":
  "AQWcHPOChvPUFnf9duejdlK04Wlpepiu2ucs5AHaCY3do0MMy9vAW3lo0vCImrm3Lf1JnV9JrGGp-4r1yk5EKuetP3wop1FQkpDsyIH3k3FHWmyPeIob_xcB6LxogcyDpRyY4byNb65Mi2g3IEz24UyL3dYjkDcWexLisnF4Uxv-EPLwwFk",
  "expires_in":5183999}
*/

  return fetch(BASE_URL + endpoint, config)
    .then(response => response.text().then(text => ({ text, response })))
    .then(({ text, response }) => {
      if (!response.ok) {
        return Promise.reject(text)
      }

      return text
    })
    .catch(err => console.log(err))
}

function callApi(endpoint, authenticated) {
  let token = localStorage.getItem("id_token") || null
  let config = {}

  if (authenticated) {
    if (token) {
      config = {
        headers: { Authorization: `Bearer ${token}` }
      }
    } else {
      throw new { error: "No token saved!" }()
    }
  }

  return fetch(BASE_URL + endpoint, config)
    .then(response => response.text().then(text => ({ text, response })))
    .then(({ text, response }) => {
      if (!response.ok) {
        return Promise.reject(text)
      }

      return text
    })
    .catch(err => console.log(err))
}
