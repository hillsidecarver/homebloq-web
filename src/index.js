import React from "react"
import { render } from "react-dom"
import { Provider } from "react-redux"
import injectTapEventPlugin from "react-tap-event-plugin"
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider"
import getMuiTheme from "material-ui/styles/getMuiTheme"
import darkBaseTheme from "material-ui/styles/baseThemes/homebloqBaseTheme"

import { persistStore } from "redux-persist"
import localForage from "localforage"

import "./index.css"
import App from "./App"
import store from "./reducers/combine"

injectTapEventPlugin()
persistStore(store, { storage: localForage })

let rootElement = document.getElementById("root")

render(
  <Provider store={store}>
    <MuiThemeProvider muiTheme={getMuiTheme(darkBaseTheme)}>
      <App />
    </MuiThemeProvider>
  </Provider>,
  rootElement
)
